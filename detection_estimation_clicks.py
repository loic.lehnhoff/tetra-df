#%%## Importations #####
import os
import json
import argparse
import numpy as np
from tqdm import tqdm
from datetime import datetime

from utils.utils import float_range, path_exists, find_sequences, BColors
from utils.sound import ClickDataFetcher
from utils.geometry import PositionsFromTDOAs, cartesian2spherical
from utils.plots import HandleResults

import warnings
warnings.filterwarnings(action="ignore")


#%%## Main #####
parent_output_folder = "Clicks_kit/detection_estimation"
channel_for_click_detection = 0
nrg_threshold = 1e-4
filter_manual = False
filter_herve = False
filter_auto = True
use_filtered_clicks = True
angles_mode = "fast"
hdbscan_min_cluster_size = 25
hdbscan_min_samples = 25
hdbscan_with_umap = False
hdbscan_method = "eom"
hdbscan_cluster_features = ["azimuth", "elevation"]

override_parameters = {
    "2022-07-24_09h08":{
        "nrg_threshold": 1e-5,
        "channel_for_click_detection": -1,
    }
}

wavefile_sources = [
    os.path.join("Audio_data", file) 
    for file in os.listdir("Audio_data")
    ]

# save parameters
with open(os.path.join(parent_output_folder, "parameters.json"), "w") as f:
    json.dump({
        "parent_output_folder" : parent_output_folder,
        "channel_for_click_detection" : channel_for_click_detection,
        "nrg_threshold" : nrg_threshold,
        "filter_manual" : filter_manual,
        "filter_herve" : filter_herve,
        "filter_auto" : filter_auto,
        "use_filtered_clicks"  : use_filtered_clicks,
        "angles_mode" : angles_mode,
        "hdbscan_min_cluster_size" : hdbscan_min_cluster_size,
        "hdbscan_min_samples" : hdbscan_min_samples,
        "hdbscan_with_umap" : hdbscan_with_umap,
        "hdbscan_method" : hdbscan_method,
        "hdbscan_cluster_features" : hdbscan_cluster_features,
        "wavefile_sources" : wavefile_sources,
        "override_parameters" : override_parameters,
        },
        f, indent=4)


#%% Run in loops
for wavefile_source in tqdm(wavefile_sources, leave=False, desc="Audio folder", position=0, total=len(wavefile_sources)):
    source_date = datetime.strptime(os.path.basename(wavefile_source), '%Y%m%d')
    sequences_in_source = find_sequences(directory=wavefile_source)
    current_output_folder = os.path.join(parent_output_folder, source_date.strftime("%d%m%Y")+"_records")
    os.makedirs(current_output_folder, exist_ok=True)

    with open(os.path.join(current_output_folder, "sequences.json"), "w") as f:
        json.dump(sequences_in_source, f, indent=4)
    
    for datetime_key in tqdm(sequences_in_source.keys(), leave=False, desc="Sequence", position=1):
        if datetime_key in override_parameters.keys():
            ClickData = ClickDataFetcher(
                sequence = sequences_in_source[datetime_key],
                output_f = current_output_folder,
                audio_dir = wavefile_source,
                threshold_peaks = override_parameters[datetime_key]["nrg_threshold"]
                )
            tqdm.write("Fetching clicks...")
            try:
                ClickData.validate_positions(
                    filter_manual=filter_manual, 
                    detect_channel=override_parameters[datetime_key]["channel_for_click_detection"])
            except NotImplementedError:
                tqdm.write(f"\t{BColors.WARNING}Skipping {datetime_key}: empty audio file(s).{BColors.ENDC}\n")
                continue

        else:
            ClickData = ClickDataFetcher(
                sequence = sequences_in_source[datetime_key],
                output_f = current_output_folder,
                audio_dir = wavefile_source,
                threshold_peaks = nrg_threshold
                )
            tqdm.write("Fetching clicks...")
            try:
                ClickData.validate_positions(filter_manual=filter_manual)
            except NotImplementedError:
                tqdm.write(f"\t{BColors.WARNING}Skipping {datetime_key}: empty audio file(s).{BColors.ENDC}\n")
                continue
        tqdm.write("Fetching delays...")
        ClickData.get_delays(filter_herve=filter_herve, filter_auto=filter_auto, tqdm_pos=2)
        tqdm.write("Saving results...")
        ClickData.save_concat_data(denoized=use_filtered_clicks, tqdm_pos=2)

        Data3D = PositionsFromTDOAs(
            sequence = sequences_in_source[datetime_key],
            output_f = current_output_folder
            )
        tqdm.write("Computing angles...")
        Data3D.compute_angles(
            path_coordinates=os.path.join(
                os.path.dirname(parent_output_folder),
                f"coords-tetra-{source_date.year}.csv"
            ), 
            mode=angles_mode)
        tqdm.write("HDBSCAN...")
        Data3D.cluster_tracks(
            mode="manual", 
            min_cluster_size=hdbscan_min_cluster_size, 
            min_samples=hdbscan_min_samples, 
            with_umap=hdbscan_with_umap,
            cluster_with=hdbscan_cluster_features, 
            cluster_selection_method=hdbscan_method)
        tqdm.write(f"{BColors.OKBLUE}\tSequence done.\n{BColors.ENDC}")

        del Data3D, ClickData

    tqdm.write(f"{BColors.OKCYAN}Audio folder done.\n{BColors.ENDC}")
tqdm.write(f"{BColors.OKGREEN}Done.\n{BColors.ENDC}")