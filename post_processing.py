#%%## IMPORTATIONS #####
import os
import json
import numpy as np
import pandas as pd
from tqdm import tqdm

from utils.misc import (load_sequences, get_dangle_segments, 
    add_whistles_2_df)

#%%## PARAMETERS #####
folder_annot = "Whistles_kit/whistle_annotations_harmonics"
output_dir = "Whistles_kit/features"

use_sequences = [
    os.path.join("exploration_results", folder)
    for folder in os.listdir("exploration_results") 
    if folder.startswith("Seq")]
sequences_df_list = load_sequences(use_sequences)

min_dur = 0.1
min_SNR = 10

#%%## MAIN #####
# fetch whistle features
whistle_features = pd.DataFrame()
for sequence_df, sequence in tqdm(zip(sequences_df_list, use_sequences), desc="Sequence", position=0, total=len(use_sequences)):   
    whistle_annotations = [
        os.path.join(folder_annot, file)
        for file in os.listdir(sequence)
        if file.endswith(".json")
    ]
    if len(whistle_annotations) > 0:
        segmented_sequence = get_dangle_segments(sequence_df)
        segmented_sequence_dangle = add_whistles_2_df(
            segmented_sequence, whistle_annotations)
        if len(segmented_sequence_dangle) > 0:
                whistle_features = pd.concat([
                    whistle_features, segmented_sequence_dangle
                ], ignore_index=True)

# add column for unique ID
whistle_features["ID"] = (
    whistle_features["contour_name"] + "_" +
    whistle_features["datetime"].dt.year.astype(str) +
    whistle_features["datetime"].dt.month.astype(str) +
    whistle_features["datetime"].dt.day.astype(str) +
    whistle_features["datetime"].dt.hour.astype(str) +
    whistle_features["datetime"].dt.minute.astype(str))

# add column for number of harmonics :
whistle_features["n_harmonics"] = 0
for loc, row in whistle_features.iterrows():
    if row.ID.startswith("h"):
        # harmonics of the following contour :
        fundamental_name = row.ID[3:]

        if (row.SNR > min_SNR) and (row.duration > min_dur):
            # increase harmonics number of contour by 1
            whistle_features.loc[(whistle_features["ID"] == fundamental_name).argmax(), "n_harmonics"] += 1

whistle_features.drop(
    whistle_features[whistle_features.contour_name.str.startswith("h")].index,
    inplace=True)
del whistle_features["contour_name"] 

# save result
whistle_features.to_csv(
    os.path.join(output_dir, "whistle_features.csv"),
    index=False
)

# hard limit on SNR and durations : 
whistle_features = whistle_features[whistle_features.SNR > min_SNR]
whistle_features = whistle_features[whistle_features.duration > min_dur]
whistle_features.to_csv(
    os.path.join(output_dir, "whistle_features_filtered.csv"),
    index=False
)