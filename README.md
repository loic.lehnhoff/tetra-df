# TETRA-DF
This repository contains the files and results associated with the analysis of the data obtained from the use of TETRA, a compact array of hydrophones, during experiments at sea with short-beaked common dolphins.
The goal was to determine the direction of arrival (DoAs) of clicks. Whistles were also investigated in order to verify the variation of some of their features depending on dolphins orientation.

<img 
    src="utils/Antenne-TETRA-no-bg.png" 
    alt="TETRA antenna: compact array of hydrophones" style="width:50%;display:block;margin:auto;"
/>

## Description of repository
```bash
.
│   # Files
├── README.md        
├── requirements.txt
├── detection_estimation_clicks.py      # to get clicks TDoAs & DoAs (1)
├── manual_processing.py                # click clustering and whistle annotations (2)
├── post_processing.py                  # Whistles features extraction (3)
├── click_directivity_analysis.ipynb    # Stats of clicks (one particular sequence)
├── whistles_analyses.ipynb             # Stats of whistles (all sequences)
│   # Folders
├── utils                               # Functions and classes for scripts execution
├── Audio_data                          
│   ├── wavefiles                       # Hidden (~10 GB of data)
│   └── data_availability_statement.md
├── Observation_data                    # DOLPHINFREE observations
├── Validation_data                     # DOLPHINFREE sequence (checking DoAs)
├── Clicks_kit                          # Results of (1) and (2)
│   ├── coords-tetra-2021.csv           # Positions of hydrophones in 2021
│   ├── coords-tetra-2022.csv           # Positions of hydrophones in 2022
│   ├── parameters.json                 # Parameters of (1)
│   ├── detection_estimation            # Results of (1) [TDoAs, DoAs, clustering]
│   └── orientation                     # Results of (1) [angles derivs]
└── Whistles_kit                        # Results of (2) and (3)
    ├── features                        # Features extracted from whistles
    ├── whistle_annotations             # Annotation results from (3)
    └── whistle_annotations_harmonics   # Additionnal annotations from (3)

```
## Setup
### Requirements
- Python >=3.9.18
- Packages in requirements.txt
- Developped with Ubuntu 20.04.6 LTS 
    - *should work on any OS though*

### Recommended
It is preferable to work in a virtual environment :
```bash
$ python -m venv DYOC-ENV
$ source DYOC-ENV/bin/activate
$ pip install -r requirement.txt
```

## Get started
### Data exploration
See *.ipynb files for results of ou data exploration on data collected amidst DOLPHINFREE experiments by TETRA antenna. 

### Framework for AoA estimation
Using functions and classes defined in the `utils` module, one can apply these methods to custom datasets. Here is an example:


```python
### Importations
import os

from utils.utils import find_sequences
from utils.sound import ClickDataFetcher
from utils.geometry import PositionsFromTDOAs
from utils.plots import HandleResults

### Parameters
audio_folder="/folder_containing_wavefiles_to_analyse"
output_folder="/folder_that_will_contain_output_data"

station_coordinates="/coords.csv" 
# if none available, can be generated using utils.geometry.TETRACoordsFinder


### Main
# find files that were recorded one after the other
# sequences is a dict : one key = one starting file
sequences = find_sequences(directory=audio_folder)

for datetime_key in sequences:
    # Fetch clicks and TDoAs
    ClickData = ClickDataFetcher(
        sequence = sequences[datetime_key],
        output_f = output_folder,
        audio_dir = audio_folder
        )
    ClickData.validate_positions(filter_manual=False)
    ClickData.get_delays()
    ClickData.save_concat_data()

    # Compute AoAs and clustering
    Data3D = PositionsFromTDOAs(
        sequence = sequences[datetime_key],
        output_f = output_folder
        )
    Data3D.compute_angles(path_coordinates = station_coordinates)
    Data3D.cluster_tracks(mode = "auto")


    # Read, display and plot results
    Results = HandleResults(
        csv_path = os.path.join(
            output_folder, 
            f"Seq_{datetime_key}", 
            "concat_delays_with_angles_and_clusters.csv"
            ), 
        station_path = station_coordinates,
        media_folder = audio_folder
        )
    Results.plot_column(columns="azimuth")
```

See the documentation associated with each class for more details.


## Contact
Please contact [Loïc Lehnhoff](mailto:loic.lehnhoff@gmail.com), [Bastien Mérigot](mailto:bastien.merigot@umontpellier.fr) or [Hervé Glotin](mailto:herve.glotin@lis-lab.fr) for any question related to this repository.


## Related to
Experiment and results are presented in details in: Lehnhoff, L., *et al* (**in prep**).

