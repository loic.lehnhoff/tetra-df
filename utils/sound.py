#%%## Importations #####
import os
import sys
import json
import wave
import librosa
import argparse
import numpy as np
import pandas as pd
from tqdm import tqdm
from datetime import datetime
import matplotlib.pyplot as plt
from scipy.signal import filtfilt, butter, find_peaks

from utils.utils import (concatenate_delays_csv, execute_code_delays_quota, get_csv, BColors)


#%%## Definitions #####
def get_SR(wavepath):
    """
    Get the sampling rate of an audio, without having to 
    import the whole waveform.

    Parameters
    ----------
    wavepath : string
        Path to the audio recording from which the sampling rate
        will be extracted
    Returns
    -------
    sr : int
        THe sampling rate of the audio file
    """
    with wave.open(wavepath, "rb") as wave_file:
        sr = wave_file.getframerate()
    return sr

def map_pinger_clicks(map_peaks, SR, distance=0.101, tolerance=0.001, clicks=10):
    """
    A refined way of finding clicks from DOLPHINFREE pinger
    without selecting echoes or other similar peaks.
    This method is based on the regularity of emission of these clicks.

    Parameters
    ----------
    map_peaks : NUMPY ARRAY
        Audio map of a waveform where each 1 is a click
        (and the rest is undefined).
    sr: INT
        Sampling rate of the waveform
    distance : FLOAT
        Distance between 2 clicks to consider them regularly distributed,
        in seconds.
    tolerance : FLOAT
        Tolerance on distance, in seconds.
    clicks : INT
        Number of clicks by train of emission from the pinger
    """
    dist = int(SR*distance)
    tol = int(SR*tolerance)

    peaks = np.where(map_peaks == 1)[0]

    ref_distances = [(((distance-tolerance)*(i+1))*SR, ((distance+tolerance)*(i+1))*SR) 
        for i in range(clicks-1)]

    # for each peak, search in the next 1.1 sec if there are 10 peaks at 
    # correct distances from each others
    correct_peaks = np.array([], dtype=int)
    total = 0
    for peak in peaks:
        map_extract = np.copy(map_peaks[peak:int(peak+(SR*1.1))])

        keep = True
        keep_peaks = np.array([], dtype=int)
        for ref in ref_distances:
            if 1 in map_extract[int(ref[0]):int(ref[1])]:
                keep_peaks = np.append(keep_peaks,
                    np.where(map_extract[int(ref[0]):int(ref[1])])[0] + int(ref[0]))
            else:
                keep = False

        if keep:
            correct_peaks = np.append(correct_peaks, peak)
            correct_peaks = np.append(correct_peaks, keep_peaks+peak)

    map_correct = np.zeros(map_peaks.shape)
    map_correct[correct_peaks] = 1      

    return map_correct

def butter_pass_filter(data, fs, cutoff=50_000, order=1, mode='high'):
    """
    Parameters
    ----------
    data : NUMPY ARRAY
        Audio signal (1D array)
    cutoff : INT or FLOAT
        Frequency limit for the filter.
    fs : INT
        Sample rate of the audio given as 'data'
    order : INT, optional
        Order of the highpass filter. The default is 1.
    mode : STRING, optional
        Mode of the filter (higpass or low pass). Must be 'high' or 'low'
    Returns
    -------
    y : NUMPY ARRAY
        Filtered signal.
    """
    
    normal_cutoff = cutoff / (fs/2)
    # Get the filter coefficients 
    b, a = butter(order, normal_cutoff, btype=mode, analog=False)
    y = filtfilt(b, a, data)
    return y

def TeagerKaiser_operator(audio):
    """
    Parameters
    ----------
    audio : NUMPY ARRAY
        Audio signal (1D array).

    Returns
    -------
    tk_signal : NUMPY ARRAY
        Signal energy computed with teager kaiser operator (1D array).
    """
    # Define Teager-Kaiser operator
    tk_signal = np.empty(audio.shape)
    # Formula : TK = x(t)**2 - x(t-1)*x(t+1)
    tk_signal[1:(audio.shape[0]-1)] = (audio[1:(audio.shape[0]-1)]**2) - \
        (audio[0:(audio.shape[0]-2)]*audio[2:(audio.shape[0])]) 
    tk_signal[0] = 0  # set first and last value to 0 (neutral)
    tk_signal[-1] = 0
    
    return tk_signal

def exclude_regular_clicks(map_peaks, SR, distance=0.101, tolerance=0.001):
    """
    Parameters
    ----------
    map_clean : NUMPY ARRAY
        Audio map of a waveform where each 1 is a click
        (and the rest is undefined).
    sr: INT
        Sampling rate of the waveform
    dist: FLOAT
        Distance between 2 clicks to consider them regularly distributed 
        (in sec).
    tol: FLOAT
        Tolerance on distance (in sec).

    Returns
    -------
    tk_signal : NUMPY ARRAY
        Signal energy computed with teager kaiser operator (1D array).
    """
    dist = int(SR*distance)
    tol = int(SR*tolerance)

    map_clean = np.copy(map_peaks)
    peaks = np.where(map_clean)[0]
    for peak in peaks:
        coord1_low = peak-dist-tol
        coord1_up = peak-dist+tol
        coord2_low = peak+dist-tol
        coord2_up = peak+dist+tol
        if ( (1 in map_peaks[coord1_low:coord1_up]) or 
            (1 in map_peaks[coord2_low:coord2_up]) ):
            map_clean[peak] = 0
    return map_clean

def group_clicks(data, space=1):
    """
    Regroups data by groups of clics that are less than 'space' apart and
    have the same label (pinger or not pinger).

    Parameters
    ----------
    data : pandas dataframe
        Must contain columns ['time', 'clusters', 'pinger'].
    space : float
        Duration in seconds between 2 clicks 
        to consider that they belong to the same group.

    Returns
    -------
    data : pandas dataframe
        Same dataframe with an added columns : 'clicks_group',
        assigning a groupe number to each click.
    """
    group = 0
    for label, subdf in data.groupby('clusters'):
        group += 1
        last = 0
        groups = []
        for i in np.where(subdf.time.diff() > space)[0]:
            groups += [group]*(i-last)
            last = i
            group += 1
        groups += [group]*(len(subdf)-last)
        data.loc[subdf.index, 'click_group'] = groups

    return data

def tdoa_denoizer_herve(df, tolerance=1e-5, n_stations=4):
    """
        Parameters
        ----------
        df : pd.DataFrame
            dataframe with columns 'h_tXY'. 
            XY must be the index of a station, with indexes in [0, n_stations-1].
        tolerance : float
            Tolerance between the sum of delays and zero. 
            Every sum farther than 'tolerance' from zero will be considered as noise.
        n_stations : int
            The number of stations (receivers/hydrophones) described by tdoas in df.
        
        Returns
        -------

    """
    # the path around all stations should have delays adding up to 0
    couples = [[i, ((i+1) if ((i+1)<n_stations) else 0)] for i in range(n_stations)]

    df_processed = df.copy()
    df_processed[f"h_t{couples[-1][0]}{couples[-1][1]}"] = -df_processed[f"h_t{couples[-1][1]}{couples[-1][0]}"]
    couples_names = [f"h_t{couple[0]}{couple[1]}" for couple in couples]

    # criteria : 01+12+23+30 = 0
    df_processed["sum_of_delays"] = df_processed[couples_names].sum(axis=1).abs()
    del df_processed[f"h_t{couples[-1][0]}{couples[-1][1]}"]

    # classify
    df_processed["noise"] = (df_processed["sum_of_delays"]>tolerance)

    return df_processed

def auto_filter(df, tolerance_delta=1e-5, check_around=1, n_stations=4, mode="tdoa", col_time="# pos", progress_bar=False):
    """
        A function that makes a classification between noisy and not noisy tdoas
        based solely on the distance with nearby tdoas.
        
        Parameters
        ----------
        df : pd.DataFrame
            Dataframe containing the columns 'h_tXY' for each combination of stations.
            And angles "azimuth" and "elevation" computed from them depending on the mode used.
        tolerance_delta : float
            When computing the minimum difference between a tdoa (or angle) and its 
            surrounding tdoas (or angles), threshold past which the difference is
            considered too correspond to the one of a noise/error.
        check_around : float
            Intervall (in sec) around each tdoa to check for correlations
        n_stations : int
            Number of stations (receivers or hydrophones) described in df.
        mode : str
            Decides if algorithms compares tdoas or angles.
            Should be "tdoa" or "angle".
        col_time : str
            name of the column where time in second is indicated.

        Returns
        -------
        The initial dataframe with an added column "noise" containing booleans
    """
    df.sort_values(by=col_time, inplace=True)
    df.reset_index(inplace=True, drop=True)
    noise = np.zeros(len(df))
    score = np.zeros(len(df))

    for i in tqdm(range(len(df)), leave=True, disable=(not progress_bar), desc="Filtering tdoas"):
        subdf = df.where(
            (df[col_time] > df[col_time][i]-check_around) &
            (df[col_time] < df[col_time][i]+check_around)
        ).dropna(how="all").copy()
        subdf = subdf.where(
            subdf[col_time] != df[col_time][i]).dropna(how="all").copy()

        if mode=="tdoa":
            for couple in [f"0{station}" for station in range(1,n_stations)]:
                if np.min(np.abs(subdf[f"h_t{couple}"] - df[f"h_t{couple}"][i])) > tolerance_delta:
                    noise[i] = 1
                    break
            score[i] = np.min(np.abs(subdf[f"h_t{couple}"] - df[f"h_t{couple}"][i]))
        elif mode == "angle":
            for angle in ["azimuth", "elevation"]:
                if np.min(np.abs(subdf[angle] - df[angle][i])) > tolerance_delta:
                    noise[i] = 1
                    break
        else:
            raise ValueError(f"mode {mode} is not in ['tdoa', 'angle']")

    
    df_with_noise = df.copy()
    df_with_noise["noise"] = noise.astype(bool)
    df_with_noise["score"] = score

    return df_with_noise

class AutoClicksDetector:
    """
        A class to detect clicks automatically in an audio recording.

        Parameters
        ----------
        wavefile_path : string
            Path to the wavefile in which clicks will be detected.
        obs_folder : string
            Path to csv files containing data from experiments 
            (mainly absence/presence of a pinger).
        name_column : string
            Name of the column containing the names of the wavefiles.
            Default is "Fichier Audio".
        outputs_path : string
            Path to the folder that will contain the output file.
            Default is "./Outputs".
        verbose : boolean
            Wether to show prints about the process or not.
            Default is False.
        sound_thresh : float
            Threshold for the detection of peaks in the audio signal
            filtered (high-pass + teager-kaiser).
            Default is 0.001.
        chan : integer
            In case the audio data is not mono-channel,
            index of the channel on which the detection of clicks will be made.
            Default is 0.
        click_size_sec : float
            Duration of a click in seconds.
            Default is 0.001 sec.

        Methods
        -------
        create_peaks_map():
            A function to find peaks (supposedly clicks) in an audio recording.
            Generates map_peaks, of the same shape that the audio recording
            with 0 as a base and 1's showing the positions of detections.
        pinger_apriori():
            A function to fetch the experimental context of the recording.
        handling_pinger_cliks():
            A function to find and exclude peaks (supposedly clicks)
            which were emitted from a pinger. 
            Based on the regularity of pinger clicks.
    """
    def __init__(
    self,
    wavefile_path=None,
    obs_folder="",
    name_column="Fichier Audio",
    outputs_path="./Outputs",
    verbose=False,
    sound_thresh=0.001,
    chan=0,
    click_size_sec=0.001):

        # fetching arguments
        if (isinstance(wavefile_path, str) and
                isinstance(obs_folder, str) and
                isinstance(name_column, str) and
                isinstance(outputs_path, str) and
                isinstance(verbose, bool) and
                isinstance(sound_thresh, float)):
            self.wavefile_path = wavefile_path
            self.obs_folder = obs_folder
            self.name_column = name_column
            self.outputs_path = outputs_path
            self.verbose = verbose
            self.sound_thresh = sound_thresh

        else:
            if self.verbose:
                print(f"{BColors.WARNING}Not enough/no attributes given to class."
                    f"\t|-> Trying to fecth arguments from terminal.{BColors.ENDC}")
            self._fetch_inputs()
        
        self.pinger_apriori()

        self.chan = chan
        self.click_size_sec = click_size_sec
        self.now = datetime.now()
        if self.verbose:
            print(f"Session start: {self.now.strftime('%Y/%m/%d %Hh%Mm%Ss')}")
        os.makedirs(outputs_path, exist_ok = True)

        # actual click detection
        self.audio_data, self.sr = librosa.load(
            self.wavefile_path, sr=None, mono=False)

        # security, if length <0.1 sec, then consider that there is no data inside
        if (self.audio_data.shape[1]/self.sr) < 0.1:
            raise NotImplementedError(
                f"Waveform duration is {(self.audio_data.shape[1]/self.sr)}s,"
                " minimum is 0.1 sec.")

        self.create_peaks_map()
        self.handling_pinger_cliks()

        # saving results
        np.save(os.path.join(
            self.outputs_path, 
            (self.wavefile_path.split("/")[-1][:-4]+
                "_peaks")),
        np.nonzero(self.map_clean)[0])

        del self.audio_data, self.map_clean


        if self.verbose:
            print(f"Session ended successfully after "
                f"{round((datetime.now()-self.now).total_seconds(),2)} seconds.\n")

    def create_peaks_map(self):
        # assign parameters
        max_length = int(self.sr*self.click_size_sec)
        mini_space = int(self.sr*self.click_size_sec*2)

        # check if mono or stereo+
        if len(self.audio_data.shape) == 1:
            signal = np.copy(self.audio_data)
        else:
            signal = np.copy(self.audio_data[self.chan])

        # detect clicks
        signal_high = butter_pass_filter(signal, self.sr, mode='high')
        tk_signal = TeagerKaiser_operator(signal_high)
        signal_peaks = find_peaks(tk_signal, 
                                distance=mini_space,
                                width=[0,max_length],
                                prominence=self.sound_thresh)[0]
        map_peaks = np.zeros(len(tk_signal), dtype=int)
        map_peaks[signal_peaks] = 1

        self.map_peaks = map_peaks

    def pinger_apriori(self):
        # find if a pinger was emitting during the recording
        if os.path.exists(self.obs_folder):
            df_obs, _ =  get_csv(self.obs_folder, slash="/")

            try:
                df_obs[self.name_column]
            except KeyError:
                print(f"\n{BColors.FAIL}KeyError{BColors.ENDC}: {self.name_column} "
                    "does not exist in dataframe\n"
                    "|-> Try to provide/change '--name_col' argument.\n")
                exit()

            idxA = np.where( 
                ("/".join(self.wavefile_path.split("/")[-2:])) ==
                list(df_obs[self.name_column]) )
            idxB = np.where(
                self.wavefile_path.split("/")[-1] == df_obs[self.name_column] )
            idx = max(list(idxA[0]), list(idxB[0]))

            if len(idx) == 1:
                signals_data = df_obs.iloc[idx[0],:]
                cat_acoustic = ["AV","AV+D","D","D+AP","AP"][
                    np.argmax(signals_data[["AV","AV+D","D","D+AP","AP"]])
                    ]
            else:
                if self.verbose:
                    print(f"\n{BColors.WARNING}WARNING: "
                        "Did not find wavefile name in .csv data.\n "
                        f"|-> Script will proceed without pinger exclusion.{BColors.ENDC}")
                cat_acoustic = "T"
        else:
            if self.verbose:
                print(f"{BColors.WARNING}WARNING: "
                    "Folder with observation data was not provided or does not exist\n"
                    f"|-> Script will assume there was no pinger recorded.{BColors.ENDC}")
            cat_acoustic = "T"

        self.cat_acoustic = cat_acoustic

    def handling_pinger_cliks(self):
        # are we supposed to have anthropogenic clicks ?
        if ('D' in self.cat_acoustic):
            if self.verbose:
                print(f"{BColors.WARNING}Pinger recorded in wavefile "
                    f"(category: {self.cat_acoustic}), excluded.{BColors.ENDC}")
            # exclude them
            map_clean = exclude_regular_clicks(self.map_peaks, self.sr)

        else:
            map_clean=np.copy(self.map_peaks)
        self.map_clean = map_clean

    def _fetch_inputs(self):
        """
        If called from a terminal command,
        will fetch arguments given after $python script.py _______
        """
        # setting up arg parser
        parser = argparse.ArgumentParser(
            formatter_class=argparse.RawTextHelpFormatter,
            description=
            ("This script requires Numpy."
            "\nAnd it does things, I think.")
        )

        parser.add_argument(
            '--wavefile', 
            type=str, 
            required=True,
            help=("Path to .wav file.")
        )

        parser.add_argument(
            '--observations', 
            type=str, 
            required=False,
            help=("Path to data folder, containing visual observation in .csv form, "
                "associated to each .wav file.")
        )

        parser.add_argument(
            '--name_col', 
            type=str, 
            required=False,
            nargs='?',
            const="Fichier Audio",
            default="Fichier Audio",
            help=("Name of the column of the .csv files containing file names.\n"
                "Default name if 'Fichier Audio'")
        )

        parser.add_argument(
            '--output', 
            type=str,
            nargs='?', 
            const="./Outputs", 
            default="./Outputs",
            required=False,
            help=("Path where the outputs will be saved. "
            "(It better be an empty folder). "
            "\nDefault value is './Outputs'")
        )

        parser.add_argument(
            '--threshold', 
            type=float,
            nargs='?', 
            const=0.001, 
            default=0.001,
            required=False,
            help=("Threshold of energy to detect clicks in waveform."
                "\nDefault value is '0.001'")
        )

        parser.add_argument(
            '--verbose', 
            action=argparse.BooleanOptionalAction,
            required=False,
            help=("Activates prints.")
            )

        # fetching arguments
        args = parser.parse_args()
        self.wavefile_path = args.wavefile
        self.obs_folder = args.observations if args.observations else False
        self.name_column = args.name_col
        self.outputs_path = args.output
        self.verbose = args.verbose
        self.sound_thresh = args.threshold


        # raising errors & warnings
        try:
            assert (os.path.exists(self.wavefile_path)), (
                "\nInputError: Wavefile path given does not exist"
                f": '{self.wavefile_path}'.\n")
            assert (self.wavefile_path.endswith(".wav")), (
                "\nInputError: Wavefile path given is not a "
                f"'.wav' file: '{self.wavefile_path}'.\n")
            assert (os.path.exists(self.outputs_path)), (
                "\nInputError: Path given for outputs does not exist: "
                f"'{self.outputs_path}'.\n")
            if self.obs_folder:
                assert os.path.exists(self.obs_folder), (
                    f"\nInputError: '{self.obs_folder}' does not exist.\n")
        except Exception as e:
            print(e)
            sys.exit()

class ClickDataFetcher:
    """
        Parameters
        ----------
        sequence : list of strings
            List of datetimes for which a recording was made.
            Strings must have the following format : "%Y%m%d_%H%M%S".
        output_f : string
            Path to the folder were thestringlist of strings
            Path to audio_folder containing wavefiles.

        Methods
        -------
        validate_positions():
            Displays a sample of the results to the user,
            sound_threshold can then be changed to obtain better results.
        get_delays():
            Using the positions of clicks computed before,
            runs a cross-correlation algorithm to find TDOAS for each recording.
        save_concat_data():
            concatenates single file of each recording in a single dataframe.
            Also adds several columns :
                - sampling rate
        _get_positions():
            Fetches clicks from audio files using AutoClicksDetector
    """
    def __init__(
            self, 
            sequence,
            output_f,
            audio_dir,
            threshold_peaks=0.0001,
            verbose=False
            ):

        # Default values
        self.threshold_peaks = threshold_peaks
        self.verbose = verbose
        
        # Initiate other variables
        self.file_dates = [datetime.strptime(date, "%Y%m%d_%H%M%S") 
            for date in sequence]
        self.output_dir = os.path.join(
            output_f, 
            min(self.file_dates).strftime("Seq_%Y-%m-%d_%Hh%M")
            )
        add_2_path = os.path.join(
            min(self.file_dates).strftime("%d%m%Y"),"wavs")
        self.audio_dir = audio_dir

        # Create folder / Fetch existing data
        if not os.path.exists(os.path.join(self.output_dir, "peaks")):
            os.makedirs(os.path.join(self.output_dir, "peaks"))

        else:
            if self.verbose:
                print(f"{BColors.WARNING}Folder already exists, "
                    f"any function executed will replace old data{BColors.ENDC}")

    def validate_positions(self, filter_manual=False, detect_channel=0):
        """
            A method to validate the positions of clicks automatically
            detected by the "._get_positions()" method.

            Parameters
            ----------
            filter_manual : bool
                If False, runs self._get_positions()
                If True, runs self._get_positions() and then prompts the
                user with plots of the clicks detected. Then the user can
                change the threshold for click detection. 
                WORKS ONLY IF TERMINAL CAN DISPLAY PYPLOT.
                Default is False.
            detect_channel : int
                Channel of the audio file on which the detection of clicks will be performed
                Default is 0.

            Returns
            -------
            None
        """
        self._get_positions(detect_channel=detect_channel)

        if filter_manual:
            loop = True
            while loop:
                loop = False

                # find highest and lowest number of detections
                min_clicks = [np.inf, -1]
                max_clicks = [0, -1]
                for num, date in enumerate(self.file_dates):
                    data = np.loadtxt(os.path.join(
                        self.output_dir, 
                        "peaks",
                        date.strftime("%Y%m%d_%H%M%S")+'_positions.csv'
                        ))
                    if isinstance(data, np.ndarray):
                        if data.shape == ():
                            n_clicks = 1
                        else:
                            n_clicks = len(data)
                        if n_clicks > max_clicks[0]:
                            max_clicks[0] = n_clicks
                            max_clicks[1] = num
                        if n_clicks < min_clicks[0]:
                            min_clicks[0] = n_clicks
                            min_clicks[1] = num
                
                # Display min and max detections separately (cpu otpimisation)
                ## Minimal detection display
                min_waveform, min_sr = librosa.load(
                    os.path.join(
                        self.audio_dir, 
                        (self.file_dates[min_clicks[1]].strftime("%Y%m%d_%H%M%S")
                            + "UTC_V12.wav")), 
                    sr=None,
                    mono=False)
                if len(min_waveform.shape)>1:
                    min_waveform = min_waveform[0]
                min_waveform = butter_pass_filter(min_waveform, min_sr, cutoff=60_000)
                min_tk = TeagerKaiser_operator(min_waveform)
                del min_waveform

                min_detect = np.loadtxt(
                    os.path.join(
                        self.output_dir, 
                        "peaks",
                        (self.file_dates[min_clicks[1]].strftime("%Y%m%d_%H%M%S")
                            + '_positions.csv')
                        )
                    )

                fig, ax = plt.subplots(1, 1)
                ax.plot(np.abs(min_tk), label='Waveform', zorder=0)
                ax.scatter(
                    min_detect*min_sr, 
                    np.abs(min_tk[np.round(min_detect*min_sr).astype(int)]),
                    label='Detections', color="tab:orange", s=5, zorder=1,)
                ax.legend(loc="upper right")
                ax.set_title(f"Waveform ({min_clicks[1]}) with less detections")
                plt.show(block=True)
                del min_detect, min_tk

                ## Maximal detection display
                max_waveform, max_sr = librosa.load(
                    os.path.join(
                        self.audio_dir, 
                        (self.file_dates[max_clicks[1]].strftime("%Y%m%d_%H%M%S")
                            + "UTC_V12.wav")), 
                    sr=None,
                    mono=False)
                if len(max_waveform.shape)>1:
                    max_waveform = max_waveform[0]
                max_waveform = butter_pass_filter(max_waveform, max_sr, cutoff=60_000)
                max_tk = TeagerKaiser_operator(max_waveform)
                del max_waveform

                # load detections
                max_detect = np.loadtxt(
                    os.path.join(
                        self.output_dir, 
                        "peaks",
                        (self.file_dates[max_clicks[1]].strftime("%Y%m%d_%H%M%S")
                            + '_positions.csv')
                        )
                    )

                fig, ax = plt.subplots(1, 1)
                ax.plot(np.abs(max_tk), label='Waveform', zorder=0)
                ax.scatter(
                    max_detect*max_sr, 
                    np.abs(max_tk[np.round(max_detect*min_sr).astype(int)]),
                    label='Detections', color="tab:orange", s=5, zorder=1)
                ax.legend(loc="upper right")
                ax.set_title(f"Waveform ({max_clicks[1]}) with most detections")
                plt.show(block=True)
                del max_detect, max_tk
                plt.close('all')


                ## validation by user ?
                new_threshold = input("Change detection threshold? "
                    f"(current={self.threshold_peaks}) [new/pass] ")
                try:
                    self.threshold_peaks = float(new_threshold)
                    print("Looking for clicks with "
                        f"new threshold={self.threshold_peaks}")
                    self._get_positions(detect_channel=detect_channel)
                    loop = True

                except:
                    print(f"\t{BColors.OKCYAN}Passed/Wrong input given{BColors.ENDC} : "
                        f"keeping threshold of {self.threshold_peaks}\n")

        else:
            pass

    def get_delays(self, filter_herve=False, filter_auto=True, select_channels=[], script_folder="utils/gsrp-tdoa", tqdm_pos=-1):
        """
            A methiod to extract TDOAs of detected clicks.

            Parameters
            ----------
            filter_herve : bool
                Whether a to apply a filter that removes all tdoas for which
                the sum of delays around all couples of stations is >1e-12 (can be modified in the function).
                Default is False, 'filter_herve' as priority over 'filter_auto'.
            filter_auto : bool
                Whether to apply a filter that only keeps tdoas which seem to be time-correlated.
                (Harsh filter but has the advantage of eliminating most of False positives.)
                Default is True
            script_folder : string
                Path to the folder were the files for 'gsrp-tdoa' algorithms are stored.
                Default is "utils/gsrp-tdoa".
            tqdm_pos : int
                Whether to show a tqdm progress bar or not.
                If negative, progress bar is hidden
                If positive, shows a progress bar with given indentation.
            select_channels : list of ints
                List of int corresponding to the channels which will be kept
                to computed tdoas. (Allows to exclude gps pulse tracks for example...)
                Default is [] (selects all channels)
            
            Returns
            -------
            None : Results are saved in the output folder in:
                - "peaks/{basename}_gsrp_outputs.csv" if no filter was applied
                - "peaks/{basename}_gsrp_outputs_denoized.csv" if a filter was applied
        """
        if filter_herve and filter_auto:
            raise NotImplementedError("Filters herve and auto cannot be set together.")

        if self.verbose:
            print("Fetching TDoAs in all audio files...")
            
        disable = True
        if tqdm_pos >= 0:
            disable = False

        for num, date in tqdm(enumerate(self.file_dates), position=tqdm_pos, total=len(self.file_dates), leave=False, desc="File treated", disable=disable):
            if self.verbose:
                print(f"Treating file {num+1}/{len(self.file_dates)}")
            basename = date.strftime("%Y%m%d_%H%M%S")
            wavefile_path = os.path.join(
                    self.audio_dir, basename+"UTC_V12.wav")
            if not os.path.exists(wavefile_path):
                wavefile_path = wavefile_path[:-3] + "WAV"

            execute_code_delays_quota(
                audio=wavefile_path, 
                outfile=os.path.join(
                    self.output_dir, "peaks", basename+'_gsrp_outputs.csv'), 
                pos_file=os.path.join(
                    self.output_dir, "peaks", basename+'_positions.csv'),
                script_folder=script_folder,
                channels=select_channels
                )  
            if self.verbose:
                print("")
            # No output from this execution, only a file saved.     

        if filter_herve or filter_auto:
            if self.verbose:
                print("De-noising delays...")
            delay_files = [
                f for f in os.listdir(os.path.join(self.output_dir, "peaks"))
                if f.endswith("_gsrp_outputs.csv")]
            for file in tqdm(delay_files, position=tqdm_pos, total=len(delay_files), leave=False, desc="Filter noise", disable=disable):
                df_delays = pd.read_csv(os.path.join(self.output_dir, "peaks", file))

                # save new df
                if filter_herve:
                    new_df_delays = tdoa_denoizer_herve(df_delays, tolerance=1e-12, n_stations=4)
                else:
                    new_df_delays = auto_filter(df_delays, mode="tdoa", check_around=2)
                new_df_delays.to_csv(
                    os.path.join(self.output_dir, "peaks", file[:-4]+"_denoized.csv"),
                    index=False)
                
    def save_concat_data(self, denoized=False, tqdm_pos=-1):
        """
            A method that uses output files from get_delays() to summarize
            them into one .CSV file. A column for the sampling rate of each
            corresponding audio file is also added.

            Parameters
            ----------
            Denoized : bool
                If True, uses files ending with "_gsrp_outputs_denoized.csv".
                Those files are generated if get_delays was executed with a filter.
                If False, uses files ending with "_gsrp_outputs.csv", which is the default output of get_delays()
                Default is False.
            tqdm_pos : int
                Whether to show a tqdm progress bar or not.
                If negative, progress bar is hidden
                If positive, shows a progress bar with given indentation.
            
            Returns
            -------
            None : The file is saved as "concat_delays.csv" in the output folder.
        """
        if denoized:
            self.tdoas = concatenate_delays_csv(
                os.path.join(self.output_dir, "peaks"),
                ending="_gsrp_outputs_denoized.csv")
        else:
            self.tdoas = concatenate_delays_csv(
                os.path.join(self.output_dir, "peaks"),
                ending="_gsrp_outputs.csv")  

        # column initialisation
        self.tdoas["sampling_rate"] = -1

        disable = True
        if tqdm_pos >= 0:
            disable = False

        for date in tqdm(pd.unique(self.tdoas.file_date), desc="Data cleaning", disable=disable, position=tqdm_pos, leave=False):
            # isolate useful data
            basename = pd.to_datetime(date).strftime("%Y%m%d_%H%M%S")
            audio_path = os.path.join(
                self.audio_dir, basename+"UTC_V12.wav")    
            if not os.path.exists(audio_path):
                audio_path = audio_path[:-3] + "WAV"
            elements = self.tdoas.where(
                self.tdoas.file_date == date
                ).sampling_rate.dropna().index 

            # add sampling rate column
            SR = get_SR(audio_path)
            self.tdoas.loc[elements, "sampling_rate"] = SR

        # Update column names
        self.tdoas["time_in_file"] = self.tdoas["time"].copy()
        self.tdoas["datetime"] = self.tdoas["file_date"] + pd.to_timedelta(self.tdoas["time"], unit="s")
        del self.tdoas["time"]

        # save concatenated data
        self.tdoas.to_csv(
            os.path.join(self.output_dir, "concat_delays.csv"),
            index=False)

        # save class parameter's to file
        save_parameters = {"threshold" : self.threshold_peaks} 

        with open(os.path.join(self.output_dir, "process_parameters.json"), "w") as f:
            json.dump(save_parameters, f, indent=4)

    def _get_positions(self, detect_channel=0):
        """
            A method that reads the given sequence and run through all 
            associated wavefiles in order to detect clicks.

            Parameters
            ----------
            detect_channel : int
                Channel of the audio file on which the detection of clicks will be performed
                Default is 0.

            Returns
            -------
            None, detections are saved in the output folder, in
            "peaks" subfolders as "{basename}_positions.csv" files
        """
        if self.verbose:
            print("Fetching clicks in all audio files...")
        for num, date in enumerate(self.file_dates):
            if self.verbose:
                print(f"Treating file {num+1}/{len(self.file_dates)}")

            basename = date.strftime("%Y%m%d_%H%M%S")

            try:
                audio_path = os.path.join(
                    self.audio_dir, basename+"UTC_V12.wav")

                AutoClicksDetector(
                    audio_path,
                    outputs_path=os.path.join(self.output_dir, "peaks"),
                    sound_thresh=self.threshold_peaks,
                    verbose=self.verbose,
                    chan=detect_channel)
            except FileNotFoundError:
                audio_path = os.path.join(
                    self.audio_dir, basename+"UTC_V12.WAV")

                AutoClicksDetector(
                    audio_path,
                    outputs_path=os.path.join(self.output_dir, "peaks"),
                    sound_thresh=self.threshold_peaks,
                    verbose=self.verbose,
                    chan=detect_channel)

            peaks_path = os.path.join(
                self.output_dir, 
                "peaks", 
                basename+"UTC_V12_peaks.npy"
                )
            peaks = np.load(peaks_path)
            sr = get_SR(audio_path)

            with open(os.path.join(self.output_dir, "peaks", basename+'_positions.csv'), "w") as f:
                np.savetxt(f, peaks/sr, fmt="%.18f")
            os.remove(peaks_path)
            del peaks

        if self.verbose:
            print("")
