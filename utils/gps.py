#%%## Importations #####
import os
import gpxpy
import numpy as np
import pandas as pd

import pyproj
geodesic = pyproj.Geod(ellps='WGS84')


#%%## Definitions ####""
class GPSConvert:
    """
    Class to 

    Parameters
    ----------
    general_path : string
        Path to the folder containing gpx files to use.
        Should look like '[folder]/{year}/GPS_tracks/{gps_loc}
    years : list of strings
        List of the years for which gpx files are available.
    gps_from : list of strings
        List of the gps location to compare

    Methods
    -------
    make_relative_to(ref):
        A function to turn absolute gps positionning into relative
        to one of the gps locations.
    save_data(table, save_to, by_day)
        A function to save computed data easily.
            - table must be "absolute" or "relative"
            - save_to must be the path to the output folder
    """
    def __init__(
        self,
        general_path, 
        years=["2021","2022"], 
        gps_from=["Boat", "Tetra"]):
        if not isinstance(years, list):
            print(years)
            raise TypeError("'years' must be a list of years.")
        elif not isinstance(years[0], str):
            raise TypeError("'years' in list must be strings.")

        if not isinstance(gps_from, list):
            raise TypeError("'gps_from' must be a list.")
        elif len(gps_from) != 2:
            raise AttributeError("'gps_from' must contain 2 elements.")

        self.years = years
        self.gps_from = gps_from
        self.general_path = general_path

        self._load_data()


    def _load_data(self):
        """
        A function to read and load gps data from indicated years
        into a single dataframe.
        """
        self.GPS_data = pd.DataFrame()
        # loop on files (organized by year and boat type here)
        for year in self.years:
            for gps_loc in self.gps_from:
                GPX_file = [
                    file for file in 
                    os.listdir(self.general_path.format(
                        year=year, gps_type=gps_loc)
                    )
                    if file.endswith(".gpx")
                ]

                # read and add gps data to DataFrame
                for file in GPX_file:
                    new_data = read_gpx(
                        os.path.join(
                            self.general_path.format(year=year, gps_type=gps_loc), 
                            file
                            )
                        )
                    new_data["rec_from"] = gps_loc

                    self.GPS_data = pd.concat(
                        [self.GPS_data, new_data],
                        ignore_index=True)

        # order data
        self.GPS_data = self.GPS_data.sort_values(by='time')
        self.GPS_data = self.GPS_data.sort_values(by='rec_from')

    def make_relative_to(self, ref):
        """
        A function to turn absolute gps positionning into relative.

        Parameters
        ----------
        ref : string
            Name of the reference gps (his relative coordinates will be 0).
            Should be one of 'gps_from'

        Returns
        -------
        None, creates a self.relative_GPS_data dataframe.
        """
        if not (ref in self.gps_from):
            raise AttributeError(f"{ref} is not in 'gps_from': {self.gps_from}")

        self.relative_GPS_data = pd.DataFrame()
        ref_data = self.GPS_data.where(
            self.GPS_data.rec_from == ref).dropna().copy()
        mov_data = self.GPS_data.where(
            self.GPS_data.rec_from != ref).dropna().copy()

        # treat each day separately
        days = pd.unique(ref_data.time.dt.strftime("%Y-%m-%d"))

        for day in days:
            gps_ref = ref_data.where(
                ref_data.time.dt.strftime("%Y-%m-%d") == day).dropna().copy()
            gps_moving = mov_data.where(
                mov_data.time.dt.strftime("%Y-%m-%d") == day).dropna().copy()

            # trim moving data to keep intersection with reference data
            trim_moving = gps_moving.where(
                gps_moving['time'] >= gps_ref['time'].min()).dropna().copy()
            trim_moving = trim_moving.where(
                trim_moving['time'] <= gps_ref['time'].max()).dropna().copy()

            gps_ref.sort_values(by='time', inplace=True)
            gps_moving.sort_values(by='time', inplace=True)
            trim_moving.sort_values(by='time', inplace=True)

            if len(trim_moving) == 0:
                warnings.warn(
                    f"Skipping '{day}': tracks have no time point in common", 
                    UserWarning)
            else:
                # convert geodesic to seconds for usability
                trim_moving['total_sec'] = (trim_moving['time'] - 
                    gps_ref['time'].min()).dt.total_seconds()
                gps_ref['total_sec'] = (gps_ref['time'] - 
                    gps_ref['time'].min()).dt.total_seconds()

                ### Now, we change the reference frame
                # First, we interpolate the positions of the reference frame at the times of the moving object
                ref_rel = trim_moving.copy()
                ref_rel['latitude'] = np.interp(
                    trim_moving['total_sec'], 
                    gps_ref['total_sec'], 
                    gps_ref['latitude'])
                ref_rel['longitude'] = np.interp(
                    trim_moving['total_sec'], 
                    gps_ref['total_sec'], 
                    gps_ref['longitude'])

                # Now we can compute relative coordinates (result in spherical)
                results = np.array([
                    geodesic.inv(ref_long, ref_lat, move_long, move_lat)
                        for ref_long, ref_lat, move_long, move_lat
                        in zip(ref_rel['longitude'], ref_rel['latitude'], trim_moving['longitude'], trim_moving['latitude'])
                    ])

                # keep following data
                moving_rel = trim_moving.copy()
                moving_rel["fwd_azimuth"] = results[:,0]
                moving_rel["back_azimuth"] = results[:,1]
                moving_rel["distance"] = results[:,2]
                moving_rel["ref_latitude"] = ref_rel["latitude"].copy()
                moving_rel["ref_longitude"] = ref_rel["longitude"].copy()     

                self.relative_GPS_data = pd.concat([
                    self.relative_GPS_data, moving_rel.copy()])

    def save_data(self, table, save_to, by_day=False):
        """
        A function to save computed data easily.
            - table must be "absolute" or "relative"
            - save_to must be the path to the output folder

        Parameters
        ----------
        table : string
            The type of table to save : 'absolute' or 'relative'
        save_to : string
            Path to the folder where the dataframe will be saved
        by_day : boolean
            Whether to save the dataframe by day or as one file.
            Default is False (one file).
        """
        if table == 'relative':
            try:
                self.relative_GPS_data
            except:
                raise NameError("Table of relative positions does not exist."
                    "\n-> Did you ran make_relative_to() before trying this?")
            use_data = self.relative_GPS_data.copy()
        elif table == 'absolute':
            use_data = self.GPS_data.copy()
        else:
            raise AttributeError(f"Unknown table '{table}'."
                "table should be one of 'absolute', 'relative'.")

        if by_day:
            days = pd.unique(use_data['time'].dt.strftime("%Y-%m-%d"))
            for day in days:
                for gps_loc in self.gps_from:
                    use_data.where(
                        use_data['time'].dt.strftime("%Y-%m-%d")==day
                        ).dropna().to_csv(
                            os.path.join(
                                save_to, 
                                gps_loc+"_gps_"+day+"_"+table+".csv"
                                ), 
                            index=False
                        )

        else:
            use_data.to_csv(
                os.path.join(save_to, "all_"+table+".csv"),
            index=False)

def read_gpx(path_to_gpx):
    """
    Reads .gpx files and extracts useful data.

    Parameters
    ----------
    path_to_gps : string
        pretty self explenatory...
    Returns
    -------
    Dataframe containing useful data : 
        latitude, longitutde, elevation, time
    """
    with open(path_to_gpx, 'r') as gpx_file:
        gpx_data = gpxpy.parse(gpx_file)
        
    gps_data = []
    for idx, track in enumerate(gpx_data.tracks):
        for segment in track.segments:
            for point in segment.points:
                gps_data.append({
                    'latitude': point.latitude,
                    'longitude': point.longitude,
                    'elevation': point.elevation,
                    'time': point.time
                })
    return pd.DataFrame(gps_data)

def add_GPS_info(df, gps_path, time_shift=0, angle_shift=0):
    """
    A function to fetch gps data corresponding to a sequence in df
    time shift is in seconds, angle shift is in radians,
    they are to be used if there is a delay between acoustic and gps points.

    Parameters
    ----------
    gps_path : string
        path to a csv file containing all infos about relative positionning
        of the boat and the antenna TETRA.
    time_shift : integer
        Number of seconds that the acoustic data must be shifted by.
        Default is 0.
    angle_shift :
        Angle that the acoustic data must be shifted by.
        Must be in radians.
        Default is 0.

    Returns
    -------
    df : pandas dataframe
        Table containing given data (shifted) and additional columns:
            - distance
            - fwd_azimuth
            - sequence_time
    """
    from utils.geometry import angle_modulo
    # transform file-relative time to sequence-relative time
    sr = int(pd.unique(df["sampling_rate"]))
    df = df.sort_values(by="sequence_time")

    # get gps data (already preprocessed)
    gps_data = pd.read_csv(gps_path, parse_dates=["time"])
    gps_data['time'] = gps_data['time'].dt.tz_localize(None)
    gps_data["time"] += pd.to_timedelta(2, unit='h')        # UTC+2 in france

    # keep only gps corresponding to this sequence
    gps_data = gps_data.where(
        gps_data["time"] >= (
            df["sequence_time"].min() 
            - pd.to_timedelta(300, unit='s'))
        ).dropna()
    gps_data = gps_data.where(
        gps_data["time"] <= (
            df["sequence_time"].max() 
            + pd.to_timedelta(300, unit='s'))
        ).dropna()

    if len(gps_data)>0:
        # add GPS data to dataframe
        df["distance"] = np.interp(
            df["sequence_time"]+pd.to_timedelta(time_shift, unit='s'),
            gps_data["time"],
            gps_data["distance"])

        # before interpolation, check for potential modulo errors on angles
        # + convert to radians
        gps_data["fwd_azimuth"] = np.radians(angle_modulo(
            gps_data["fwd_azimuth"],
            mode="deg"))
        gps_data["back_azimuth"] = np.radians(angle_modulo(
            gps_data["back_azimuth"],
            mode="deg"))

        # now we can do linear interpolation
        df["fwd_azimuth"] = np.interp(
            df["sequence_time"]+pd.to_timedelta(time_shift, unit='s'),
            gps_data["time"],
            gps_data["fwd_azimuth"])

    if (time_shift != 0) and (angle_shift != 0):
        df["sequence_time"] = (df["sequence_time"]
            + pd.to_timedelta(time_shift, unit='s'))
        # raw_azimuth should already be in radians
        df["raw_azimuth"] = angle_modulo(
            df["raw_azimuth"]+angle_shift, 
            mode="deg")
        
    return df
