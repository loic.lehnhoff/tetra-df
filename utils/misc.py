#%%## IMPORTATIONS #####
import os
import wave
import json
import numpy as np
import pandas as pd
import pickle as pkl
from tqdm import tqdm
from datetime import datetime
from itertools import groupby
from operator import itemgetter 
from sklearn.cluster import HDBSCAN
from scipy.signal import butter, filtfilt

from scipy import stats
import statsmodels.api as sm

import seaborn as sns
from matplotlib import colors
from matplotlib import rcParams
import matplotlib.pyplot as plt
from matplotlib.path import Path
from matplotlib.patches import Ellipse
from matplotlib.patches import Polygon
import matplotlib.transforms as transforms
from matplotlib.widgets import LassoSelector

import sys
sys.path.insert(0, '/home/loic/Data/DOLPHINFREE/Public folders/PyAVA')
from functions import load_waveform, wave_to_spectrogram
sys.path.insert(0, '/home/loic/Data/DOLPHINFREE/Public folders/PyAVA/line_clicker')
from lite_line_clicker import clicker, to_curve

#%%## DEFINITIONS #####
def find_longest_sequence(arr):
    current_sequence = 0
    max_sequence = 0

    for num in arr:
        if num == 1:
            current_sequence += 1
            max_sequence = max(max_sequence, current_sequence)
        else:
            current_sequence = 0

    return max_sequence

def moving_average(a, n=3):
    ret = np.cumsum(a, dtype=float)
    ret[n:] = ret[n:] - ret[:-n]
    return ret[n - 1:] / n

def right_derivatives(x, y, fill_last=True):
    deriv_y = []

    if len(x) < 2:
        raise ValueError("Need 2 values minimum for computation of derivative.")

    # right
    for i in range(0, len(x)-1):
        prev_x = x[i]
        next_x = x[i+1]
        prev_y = y[i]
        next_y = y[i+1]

        deriv_y += [(next_y-prev_y)/(next_x-prev_x)]
        
    # index -1 
    if fill_last:
        deriv_y = deriv_y + [
            ((y[-1]-y[-1])/(x[-1]-x[-2]))
            ] 

    return np.array(deriv_y)

def central_difference(xs, ys, dt, float_precision=10):
    # WARNING : xs and ys must be ordered
    if not isinstance(xs, np.ndarray):
        xs = np.array(xs)
    if not isinstance(ys, np.ndarray):
        ys = np.array(ys)

    if (xs != np.sort(xs)).all():
        raise ValueError("Given data is not sorted.")

    dx = round(xs[1]-xs[0], float_precision)

    if len(xs) < (round(dt/dx)*2):
        raise ValueError(
            "Given 'xs' is too small for given 'dt'. "
            f"'xs' as steps of {dx}, with dt={dt} minimal length should be "
            f"{(round(dt/dx)*2) +1} but len(xs)={len(xs)}")

    new_x = []
    deriv_y = []
    for i in range(round(dt/dx), len(xs)-round(dt/dx)):
        prev_x = xs[i-round(dt/dx)]
        next_x = xs[i+round(dt/dx)]
        prev_y = ys[i-round(dt/dx)]
        next_y = ys[i+round(dt/dx)]

        new_x += [np.mean([prev_x, next_x])]
        deriv_y += [(next_y-prev_y)/(next_x-prev_x)]
    
    return new_x, deriv_y

def difference(xs, ys, dt, float_precision=10):
    # WARNING : xs and ys must be ordered
    if (xs != np.sort(xs)).all():
        raise ValueError("Given data is not sorted.")

    dx = round(xs[1]-xs[0], float_precision)

    if len(xs) < (round(dt/dx)*2):
        raise ValueError(
            "Given 'xs' is too small for given 'dt'. "
            f"'xs' as steps of {dx}, with dt={dt} minimal length should be "
            f"{(round(dt/dx)*2) +1} but len(xs)={len(xs)}")

    new_x = []
    deriv_y = []
    for i in range(round(dt/dx), len(xs)-round(dt/dx)):
        prev_x = xs[i-round(dt/dx)]
        next_x = xs[i+round(dt/dx)]
        prev_y = ys[i-round(dt/dx)]
        next_y = ys[i+round(dt/dx)]

        new_x += [np.mean([prev_x, next_x])]
        deriv_y += [(next_y-prev_y)]
    
    return new_x, deriv_y

def modulo360(angles, bias=0):
    """
    A function to make sure that angles remain in the interval [-180°, 180°].

    Parameters
    ----------
    angles : numpy array
        Angles in degrees
    bias : float
        A value to add to all angles

    Returns
    -------
    angles : numpy array
        angles with bias added, with value circling in [-180°, 180°] interval.
    """
    angles = angles + bias
    angles = (angles+180)%360   # add 180 because
    angles -= 180               # next line centers on 0

    return angles

class NumpyEncoder(json.JSONEncoder):
    """ Special json encoder for numpy types """
    def default(self, obj):
        if isinstance(obj, (np.int_, np.intc, np.intp, np.int8,
                            np.int16, np.int32, np.int64, np.uint8,
                            np.uint16, np.uint32, np.uint64)):
            return int(obj)
        elif isinstance(obj, (np.float_, np.float16, np.float32,
                              np.float64)):
            return float(obj)
        elif isinstance(obj, (np.ndarray,)):
            return obj.tolist()
        return json.JSONEncoder.default(self, obj)
   
        
#%%## DATA HANDLERS #####
def import_tdoa_data(folder_tdoas, folder_outputs, folder_csvs=None, list_conditions=None):
    """
        A function to merge all tdoas and angle estimations into
        one CSV file. Conditions can be given to keep/remove
        sections of data.

        Parameters
        ----------
        folder_tdoas : str
            folder containing tdoas and angle estimation CSVs.
        folder_outputs : str
            Path to the output folder.
        folder_csvs : str
            Path to the folder containing CSVs of visual observations.
            CSVs names should follow the pattern 'Tetra_Data_{year}.csv'
            and contain columns ['Date', 'Heure (UTC) [corrigé]', 'Fichier Audio'].
            Default is None.
        list_conditions : list
            Conditions that must match an entry to be integrated
            into the output dataframe. Columns that are used as 
            conditions are the ones in CSVs from folders_csvs.
            A list must be made with sublists : conditions inside
            a sublists are 'or' operators, we use the 'and' operator
            between sublists.
            e. g.   conditions = [["A"], ["B", "C"]]
                    then an entry must correspond to 
                    (("A") and ("B" or "C")))

        Returns
        -------
        pd.DataFrame
            DataFrame containing rows corresponding to given 
            parameters. Columns are ['file_date', 'time_in_file', 
            'datetime', 'elevation', 'azimuth', 'sequence_name', 
            'time_in_sequence'].
    """
    # saving parameters and verbose
    if isinstance(list_conditions, list):
        print("Prepocessing data...")
        print_conditions = " and\n\t\t".join(
                [" or ".join(conditions) 
                for conditions in list_conditions]
                )
        print(f"\tConditions to keep a sequence:\n\t\t{print_conditions}")
        with open(os.path.join(folder_outputs, "conditions.pkl"), "wb") as f:
            pkl.dump(list_conditions, f)

    # importation
    all_data = None  # initialization

    # loop on predictions (parent folder > day > sequence)
    days_folders = [
        folder for folder in os.listdir(folder_tdoas)
        if folder.endswith("_records")]
    for day in tqdm(days_folders, desc="day", position=0, leave=False):
        sequence_folders = [
            folder for folder in
            os.listdir(os.path.join(folder_tdoas, day))
            if folder.startswith("Seq_")]

        # load corresponding obs data
        if isinstance(folder_csvs, str):
            obs_data = pd.read_csv(
                os.path.join(folder_csvs, f"Tetra_Data_{day[4:8]}.csv"),
                parse_dates=["Date"], dayfirst=True)
            obs_data["datetime"] = obs_data["Date"] + pd.to_timedelta(obs_data["Heure (UTC) [corrigé]"])
            obs_data["file_date"] = pd.to_datetime(obs_data["Fichier Audio"], format="%Y%m%d_%H%M%SUTC_V12.wav")

        for sequence in tqdm(sequence_folders, desc="sequence", position=1, leave=False):
            temp_df = pd.read_csv(
                os.path.join(folder_tdoas, day, sequence, 
                    "concat_delays_with_angles.csv"),
                parse_dates=["file_date", "datetime", "time_in_file"]
            )
            temp_df = temp_df.where(temp_df["noise"] == False).dropna(how="any").copy()

            # suppress useless columns
            del temp_df["db_norm"], temp_df["db"], temp_df["h_db_norm"], temp_df["h_db"]
            del temp_df["t01"], temp_df["t02"], temp_df["t03"], temp_df["t12"], temp_df["t13"], temp_df["t23"]
            del temp_df["h_t01"], temp_df["h_t02"], temp_df["h_t03"], temp_df["h_t12"], temp_df["h_t13"], temp_df["h_t23"]
            del temp_df["sampling_rate"], temp_df["score"], temp_df["noise"]

            # add attributes
            temp_df["sequence_name"] = sequence
            temp_df["time_in_sequence"] = (temp_df.datetime - temp_df.datetime.min()).dt.total_seconds()
            temp_df.sort_values(by="time_in_sequence", inplace=True)
            temp_df.reset_index(drop=True, inplace=True)
            for condition_i in range(len(list_conditions)):
                temp_df[f"condition{condition_i}"] = "unknown"

            # loop on files in sequence
            file_dates = temp_df["file_date"].unique().copy()
            for file in file_dates:
                if isinstance(list_conditions, list):
                    # find corresponding file in obs data to check conditions
                    obs_row = obs_data.where(obs_data["file_date"] == file).dropna(how="all").copy()

                    if np.all([
                        np.any([obs_row[condition].all()==1 for condition in conditions])
                        for conditions in list_conditions
                        ]):
                        pass # keep file
                        
                        rows = np.where(temp_df["file_date"] == file)[0]
                        for condition_i in range(len(list_conditions)):
                            temp_df.loc[rows, f"condition{condition_i}"] = obs_row[list_conditions[condition_i]].idxmax(axis=1).values[0]

                    else:
                        temp_df = temp_df.where(temp_df["file_date"] != file).dropna().copy()
                        temp_df.reset_index(drop=True, inplace=True)

            
            # add to full-dataframe
            if isinstance(all_data, pd.DataFrame):
                # append
                all_data = pd.concat([all_data, temp_df], ignore_index=True)
            else:
                # create
                all_data = temp_df.copy()
    
    return all_data

def cluster_and_clean_angles(points, cluster_mode="auto", clean=True):
    """
        A function that asks to shift angles before clustering
        them together (taking time into account).

        Parameters
        ----------
        points : pd.DataFrame
            A dataframe containing points to cluster
            Must contain columns ['azimuth', 'time_in_sequence']
            azimuths must be in DEGREES.
        cluster_mode : str, optional
            If 'auto', uses UMAP to cluster angles in time.
            If 'manual', displays a plot to cluster manually by lasso selection.
            Default is 'auto'.
        clean : bool, optional
            Whether to clean data after clusterisation or not.
            Cleaning by discarding clusters with 
                - mean difference between azimuths > 1 deg.
                - number of points in cluster <= 5.
            Default is True.

        Returns
        -------
        tuple
            DataFrame given cleaned of detected noise.
            Labels of the clusters associated to each point.

        Raises
        ------
        ValueError
            When cluster_mode given is not available.
    """
    points = points.copy()

    tqdm.write("Shifting angles...")
    plt.close("all")
    shift_angle = TransformData(points[["time_in_sequence", "azimuth"]].values).angle_bias
    points["azimuth"] = modulo360(points["azimuth"], shift_angle)
        
    if cluster_mode == "auto":
        tqdm.write("Autoclustering...")
        labels = AutoClusters(points[["time_in_sequence", "azimuth"]].values) 

    elif cluster_mode == "manual":
        tqdm.write("Manual clustering...")
        labels = ManualClusters(points[["time_in_sequence", "azimuth"]].values)
    
    else:
        raise ValueError("Unknown mode given.")

    tqdm.write("Cleaning clusters...")
    if clean:
        # clear clusters
        new_labels = np.copy(labels)
        new_points = points.copy()

        # STEP 1 : remove low count data
        unique_elements, counts = np.unique(labels, return_counts=True)
        for label, count in zip(unique_elements, counts):
            temp_points = new_points.iloc[new_labels == label].copy()
            density = count/(np.max(temp_points["time_in_sequence"])-np.min(temp_points["time_in_sequence"]))
            if (count <= 5):
                new_points = new_points.iloc[np.where(new_labels != label)[0]].copy()
                new_labels = np.copy(new_labels[new_labels != label])
            elif (density < 1):
                new_points = new_points.iloc[np.where(new_labels != label)[0]].copy()
                new_labels = np.copy(new_labels[new_labels != label])

        return new_points.reset_index(drop=True, inplace=False), new_labels

    else:
        return points.iloc[labels != -1].reset_index(drop=True, inplace=False), labels[labels != -1]

def derivatives_per_cluster(df, labels, deriv_threshold=30):
    """
        Computes derivatives in each cluster

        Parameters
        ----------
        df : pd.DataFrame
            Table with ["time_in_sequence", "azimuth"] columns
        labels : array-like
            A list of labels corresponding to the rows of df
        deriv_threshold : float, optional
            Maximal derivative that should be considered as True.
            Values over this threshold will be considered errors and
            the user will be asked to handle them. 
            Default is 30 (in deg).
    """
    # initialization
    derivatives = np.zeros(len(labels))
    df = df.reset_index(inplace=False, drop=True).copy()

    # compute derivative by cluster
    unique_labels = np.unique(labels)
    for label in unique_labels:
        df_subset = df.loc[
                np.where(labels == label)
            ].dropna(how="all").copy()
        
        if len(df_subset) > 1:
            local_derivatives = right_derivatives(
                x=df_subset["time_in_sequence"].values, 
                y=df_subset["azimuth"].values, 
                fill_last=True)

            exclude = np.copy((np.abs(local_derivatives) > 30))
        else:
            local_derivatives = np.array([np.nan])
            exclude = np.array([False])

        if find_longest_sequence(exclude) > 5:
            local_clusters = ManualClusters(
                df_subset[["time_in_sequence", "azimuth"]].values)

            # 2 cases :
            # no selection -> throw cluster away
            if (len(np.unique(local_clusters)) == 1) and (local_clusters[0] == 0):
                local_derivatives = np.full(len(local_derivatives), np.nan)
                derivatives[labels == label] = local_derivatives

            # n selections -> remap cluster
            else:
                local_derivatives = np.full(len(local_derivatives), np.isnan)
                new_labels = np.copy(labels)
                for local_cluster in np.unique(local_clusters):
                    # remap labels
                    new_label = max(new_labels)+1

                    to_change = np.where(labels==label)[0][np.where(local_clusters==local_cluster)[0]]
                    new_labels[to_change] = new_label

                    if (local_cluster != 0) and (len(np.where(local_clusters == local_cluster)[0])>5):
                        # recompute derivatives
                        subsubset = df.loc[
                            np.where(new_labels == new_label)
                        ].dropna(how="all").copy()
                    
                        local_derivatives[local_clusters == local_cluster] = right_derivatives(
                            x=subsubset["time_in_sequence"].values, 
                            y=subsubset["azimuth"].values, 
                            fill_last=True)
                    else:
                        local_derivatives[local_clusters == local_cluster] = np.full(
                            len(np.where(local_clusters == local_cluster)[0]),
                            np.nan
                        )
                
                derivatives[labels == label] = local_derivatives
                labels = np.copy(new_labels)

        else:
            # exclude designated values
            local_derivatives[exclude] = np.nan
            derivatives[labels == label] = local_derivatives

    # data to_return
    to_return = np.invert(np.isnan(derivatives))

    if (np.sum(to_return) != len(derivatives)):
        return derivatives_per_cluster(
            df.loc[to_return].copy(), 
            np.copy(labels[to_return]), 
            deriv_threshold=30)

    else:
        return (
            df.loc[to_return].copy(),
            np.copy(labels[to_return]),
            np.copy(derivatives[to_return]))

def derivatives_by_label(points, labels, min_time=0.5, step=0.025, size_ma=4):
    """
        A function that computes the derivatives for each cluster
        of points.

        Parameters
        ----------
        points : pd.DataFrame
            DataFrame containing the columns ['azimuth', 'time_in_sequence']
            'time_in_sequence' must be in SECONDS.
        labels : array-like
            List of the labels corresponding to each point in points
        min_time : float
            Minimal number of seconds that a cluster must last
            to be considered enough for further computation.
            Default is 0.5 seconds.
        step : float
            Time between two points to compute derivative
            (interpolated from points['time_in_sequence']).
            Default is 25 ms.
        size_ma : int
            _description_

        Returns
        -------
        pd.DataFrame
            A DataFrame containing the columns 
            ['deriv_x', 'deriv_y', 'label']
    """
    uniques, counts = np.unique(labels, return_counts=True)

    all_derivs = pd.DataFrame(columns=["deriv_x", "deriv_y", "label"])
    for label, count in zip(uniques, counts):
        subset = points.iloc[labels==label].copy()       
        # subset[:,1] = subset[:,1]-np.mean(subset[:,1])

        if ((subset["time_in_sequence"].max()-subset["time_in_sequence"].min()) >= min_time):
            
            x = np.arange(
                subset["time_in_sequence"].min(), 
                subset["time_in_sequence"].max()+step, 
                step)
            y = np.interp(
                x, subset["time_in_sequence"], 
                subset["azimuth"])

            avg_x = moving_average(x, n=size_ma)
            avg_y = moving_average(y, n=size_ma)

            deriv_x, deriv_y = central_difference(avg_x, avg_y, dt=step)

            to_add = pd.DataFrame(
                columns=["deriv_x", "deriv_y", "label"],
                data=np.array([
                    deriv_x, 
                    np.abs(deriv_y), 
                    np.repeat(label, len(deriv_x))]).T
            )

            all_derivs = pd.concat([all_derivs, to_add])

    all_derivs.label = all_derivs.label.astype(int)
    return all_derivs.sort_values(by="deriv_x")

def segmentation_OnOffUnknown(points_df, deriv_points_df, interp_step=0.025, eval_step=0.5, dangle_threshold=3):
    """
        A function to categorize derivatives per segment.

        Parameters
        ----------
        points_df : pd.DataFrame
            Must contain column ['time_in_sequence']
        deriv_points_df : pd.DataFrame
            Must contain columns ['deriv_x', 'deriv_y', 'label']
        step : float, optional
            Time step for determination of average derivatives.
            Default is 0.025 seconds.
        dangle_threshold: float, optionnal
            A threshold for derivatives to be considered ON/OFF axis.
            Default is 3 degrees.

        Returns
        -------
        pd.DataFrame
            A table with columns ["xmin", "xmax", "median_dangle", "category"]
            That contain one of ['ON-AXIS', 'OFF-AXIS', 'UNKNOWN'] 
            category for each x-interval.
        float
            Threshold used for the categorization of angles.
    """
    segmentation_df = pd.DataFrame(columns=["xmin", "xmax", "median_dangle", "category"])
    
    x_range = np.arange(
                deriv_points_df["deriv_x"].min(), 
                deriv_points_df["deriv_x"].max(),
                interp_step)
    smooth_x = moving_average(x_range, n=round(1/interp_step))
    smooth_y = moving_average(
            np.interp(
                x_range,
                deriv_points_df["deriv_x"],
                deriv_points_df["deriv_y"]), 
                n=round(1/interp_step))
    
    for xinterval in tqdm(np.arange(
        deriv_points_df["deriv_x"].min(), 
        deriv_points_df["deriv_x"].max(), 
        eval_step), desc="segmentation", leave=False):

        m_dangle = np.median(smooth_y[np.where(
            np.logical_and(
                (smooth_x >= xinterval),
                (smooth_x <= xinterval+eval_step))
            )[0]])
        
        points_in_interval = np.logical_and(
                (points_df["time_in_sequence"] >= xinterval),
                (points_df["time_in_sequence"] <= xinterval+eval_step)).sum()
        
        if (np.abs(m_dangle) < dangle_threshold) and (points_in_interval > 0):
            segmentation_df.loc[len(segmentation_df)] = [xinterval, xinterval+eval_step, m_dangle, "ON-AXIS"]
        elif (np.abs(m_dangle) >=  dangle_threshold) and (points_in_interval > 0): 
            segmentation_df.loc[len(segmentation_df)] = [xinterval, xinterval+eval_step, m_dangle, "OFF-AXIS"]
        else:   
            segmentation_df.loc[len(segmentation_df)] = [xinterval, xinterval+eval_step, np.nan, "UNKNOWN"]

    return segmentation_df, dangle_threshold

def butter_pass_filter(data, cutoff, fs, order=1, mode='high'):
    """
    Parameters
    ----------
    data : NUMPY ARRAY
        Audio signal (1D array)
    cutoff : INT or FLOAT
        Frequency limit for the filter.
    fs : INT
        Sample rate of the audio given as 'data'
    order : INT, optional
        Order of the highpass filter. The default is 1.
    mode : STRING, optional
        Mode of the filter (higpass or low pass). Must be 'high' or 'low'
    
    Returns
    -------
    y : NUMPY ARRAY
        Filtered signal.
    """
    
    normal_cutoff = cutoff / (fs/2)
    # Get the filter coefficients 
    b, a = butter(order, normal_cutoff, btype=mode, analog=False)
    y = filtfilt(b, a, data)
    return y

def load_spectrogram(path2WAV, new_sr, for_display=False):
    if for_display:
        spectrogram, duration = wave_to_spectrogram(
            waveform=butter_pass_filter(
                load_waveform(path2WAV, sr_resample=new_sr, channel=0), 
                cutoff=250, fs=new_sr, mode='high'), 
            SR=new_sr, clip=-160, 
            n_fft=2048, w_size=512, 
            as_pcen=False, top_db=160)
    
        spectrogram = np.copy(spectrogram[::-1])
        spectrogram[spectrogram < -80] = -80

    else:
        spectrogram, duration = wave_to_spectrogram(
            waveform=load_waveform(path2WAV, sr_resample=new_sr, channel=0), 
            SR=new_sr, clip=-160, 
            n_fft=2048, w_size=512, 
            as_pcen=False, top_db=160)
        spectrogram = np.copy(spectrogram[::-1])

    return spectrogram, duration

def load_sequences(sequence_folders):
    if not isinstance(sequence_folders, list):
        raise ValueError("sequence_folders must be a list of folders paths.")
    
    # load 
    sequences_dfs = []

    for use_sequence in tqdm(sequence_folders, "sequence", position=0, leave=False):
        sequence_files = sorted(os.listdir(use_sequence))
        if len(sequence_files) == 0:
            sequences_dfs += [[]]
            continue # skip loop    
        
        df_load = pd.read_csv(
            os.path.join(use_sequence, "concat_doa_cluster_deriv.csv"))
        
        old_clusters = df_load["clusters"].values
        new_clusters = np.zeros(len(old_clusters))
        for i, cluster in enumerate(np.unique(old_clusters)):
            new_clusters[old_clusters == cluster] = i
        
        df_load["clusters"] = new_clusters

        sequences_dfs += [df_load.copy()]

    return sequences_dfs

def get_dangle_segments(df):
    # now, get segments
    clusters_starts_stops = pd.DataFrame(columns=[
        "time_min_in_sequence", "time_max_in_sequence", "file_date", "clusters"])
    for cluster in np.unique(df["clusters"]):
        clusters_starts_stops.loc[len(clusters_starts_stops)] = [
            df.where(df["clusters"] == cluster).dropna(how="all").copy()["time_in_sequence"].min(),
            df.where(df["clusters"] == cluster).dropna(how="all").copy()["time_in_sequence"].max(),
            df.where(df["clusters"] == cluster).dropna(how="all").copy()["file_date"].unique()[0],
            cluster
        ]

    segments = pd.DataFrame(columns=[
        "time_min_in_sequence", "time_max_in_sequence", 
        "time_min_in_file", "time_max_in_file",
        "file_date", "angular_speed"])
    df.sort_values(by="time_in_sequence", inplace=True)
    
    for i in tqdm(range(len(df)-1), position=1, leave=False, desc="segment"):
        lower_t = df.iloc[i].time_in_sequence.copy()
        upper_t = df.iloc[i+1].time_in_sequence.copy()

        if df.iloc[i].file_date != df.iloc[i+1].file_date:
            continue

        local_clusters = clusters_starts_stops.where(
            (clusters_starts_stops["time_min_in_sequence"] <= upper_t) &
            (clusters_starts_stops["time_max_in_sequence"] >= lower_t) &
            (clusters_starts_stops["file_date"] == df.iloc[i].file_date)
        ).dropna(how="all").copy().clusters.unique()

        # if only one cluster, ez : store results
        if len(local_clusters) == 1:
            segments.loc[len(segments)] = [
                df.iloc[i].time_in_sequence,
                df.iloc[i+1].time_in_sequence,
                df.iloc[i].time_in_file,
                df.iloc[i+1].time_in_file,
                df.iloc[i].file_date,
                df.iloc[i].angular_speed
            ]

        # if several clusters....
        else:
            min_cluster = df.iloc[i].clusters
            min_cluster_end = df.where(
                df["clusters"] == min_cluster
                ).dropna(how="all").copy()["time_in_sequence"].max()
            max_cluster = df.iloc[i+1].clusters
            max_cluster_start = df.where(
                df["clusters"] == max_cluster
                ).dropna(how="all").copy()["time_in_sequence"].min()

            if ((lower_t == min_cluster_end) & (upper_t == max_cluster_start)):
                # no saving needed
                pass 


            else:
                # overlapping clusters, take the mean angular speed
                angular_speeds = []
                for cluster in local_clusters:
                    
                    pick_df_from = df.where(
                        df["clusters"] == cluster
                        ).dropna(how="all").copy()
                    
                    # find closest value
                    closest_i = np.argmin(np.abs(pick_df_from["time_in_sequence"] - lower_t))
                    closest_i_value = np.min(np.abs(pick_df_from["time_in_sequence"] - lower_t))

                    if (np.min(np.abs(pick_df_from["time_in_sequence"] - upper_t)) < closest_i_value):
                        closest_i = np.argmin(np.abs(pick_df_from["time_in_sequence"] - upper_t))
                
                    angular_speeds += [pick_df_from["angular_speed"].iloc[closest_i]]

                segments.loc[len(segments)] = [
                    df.iloc[i].time_in_sequence,
                    df.iloc[i+1].time_in_sequence,
                    df.iloc[i].time_in_file,
                    df.iloc[i+1].time_in_file,
                    df.iloc[i].file_date,
                    np.mean(angular_speeds)
                ]
        
    return segments

def add_whistles_2_df(df, annotation_files, re_sr=64_000, min_dur=0, min_SNR=0):
    # fetch contours infos
    all_data_summarized = pd.DataFrame()
    for file in tqdm(annotation_files, "file", position=1, leave=False):
        file_datetime = datetime.strptime(os.path.basename(file)[:-5], '%Y%m%d_%H%M%S')
        
        tqdm.write(f"\n\tfile: {file_datetime.strftime('%Y%m%d_%H%M%S')}")

        # load contours
        original_contours = import_contours_to_curves(file)

        audio_path = ("/media/loic/Extreme SSD/Acoustique/" +
                f"{file_datetime.strftime('%Y')}/Antenne/" +
                f"{file_datetime.strftime('%d%m%Y')}/wavs/" +
                f"{file_datetime.strftime('%Y%m%d_%H%M%S')}UTC_V12.wav") 
        tqdm.write(f"\t\t{len(original_contours)} annotations loaded.")
        spec, duration = load_spectrogram(
            audio_path, new_sr=re_sr, for_display=False)
        tqdm.write(f"\t\tSpectrogram loaded.")
       
        file_stats = pd.DataFrame(
            columns=["datetime", "contour_name"],
            data=np.array([
                [file_datetime for i in range(len(original_contours))],
                list(original_contours.keys())]).T)

        pixel_contours = refine_contours2pixels(
            original_contours, 
            new_sr=re_sr,
            wavepath=audio_path,
            interval=7,
            better=True)

        file_stats["duration"] = [
            (len(pixel_contours[contour])/spec.shape[1])*duration 
            for contour in pixel_contours
        ]
        tqdm.write(f"\t\t{(file_stats['duration'] > min_dur).sum()} annotations with length >= {min_dur} sec.")

        # compute SNR of each contour  
        # (antenna audios are quite noisy instead of the median taken as ref, 
        # we use the 5% background noise)
        SNRs = []
        signals = []
        for key in list(pixel_contours.keys()):
            try :
                noise_per_x = np.quantile(
                spec[
                    min(pixel_contours[key][:,1]):(max(pixel_contours[key][:,1])+1),
                    min(pixel_contours[key][:,0]):(max(pixel_contours[key][:,0])+1),
                ], 
                q=0.25,
                axis=0)
                signal_db_per_x = spec[pixel_contours[key][:,1], pixel_contours[key][:,0]]

                SNRs += [np.mean(signal_db_per_x-noise_per_x)]
                signals += [np.mean(signal_db_per_x)]
            except ValueError:
                SNRs += [np.nan]
                signals += [np.mean(signal_db_per_x)]
            except IndexError:
                SNRs += [np.nan]
                signals += [np.mean(signal_db_per_x)]

        file_stats["SNR"] = SNRs
        file_stats["signal_dB"] = signals
        tqdm.write(f"\t\t{(file_stats['SNR']>min_SNR).sum()} annotations with SNR >= {min_SNR}.")
        tqdm.write(f"\t\t{((file_stats['SNR']>min_SNR) & (file_stats['duration']>min_dur)).sum()} annotations with duration >= {min_dur} and SNR >= {min_SNR}.")

        # find and save mean angular speeds
        angular_speeds = []
        for key in list(pixel_contours.keys()):
            whistle_start = original_contours[key][:,0].min()
            whistle_end = original_contours[key][:,0].max()

            df_selection  = df.where(
                (df["time_min_in_file"] <= whistle_end) &
                (df["time_max_in_file"] >= whistle_start) &
                (df["file_date"] == file_datetime.strftime('%Y-%m-%d %H:%M:%S'))
            ).dropna(how="all").copy()
            angular_speeds += [df_selection["angular_speed"].mean()]
        file_stats["angular_speeds"] = angular_speeds

        # save last infos
        file_stats["max_f"] = [
            (re_sr//2)-((min(pixel_contours[key][:,1])/spec.shape[0])*(re_sr//2)) 
            for key in list(pixel_contours.keys())
        ]
        file_stats["min_f"] = [
            (re_sr//2)-((max(pixel_contours[key][:,1])/spec.shape[0])*(re_sr//2))
            for key in list(pixel_contours.keys())
        ]
        file_stats["mean_f"] = [
            (re_sr//2)-((np.mean(pixel_contours[key][:,1])/spec.shape[0])*(re_sr//2))
            for key in list(pixel_contours.keys())
        ]

        all_data_summarized = pd.concat([all_data_summarized, file_stats.copy()])

    return all_data_summarized

#%%## STATS #####
def import_contours_to_curves(path):
    with open(path, "r") as f:
        annotations = json.load(f)
    
    curves = {}
    for key in list(annotations.keys()):
        coords = np.copy(np.array(annotations[key]))
        if coords.shape[0] > 2:
            curvex, curvey = to_curve(coords[:,0], coords[:,1])
            curves[key] = np.array([curvex, curvey]).T
        else:
            curves[key] = np.array([coords[:,0], coords[:,1]]).T

    return curves

def refine_contours2pixels(coord_dict, wavepath, new_sr, interval, smoothing=3, better=False):
    if smoothing%2 != 1:
        raise ValueError("'smoothing' must be an odd number.")

    spectrogram, duration = load_spectrogram(
        wavepath, 
        new_sr=new_sr,
        for_display=True)

    new_contours = {}
    for key in list(coord_dict.keys()):
        curve_coords = np.copy(np.array(coord_dict[key]))

        # scale it to spectrogram length/height
        contourx = curve_coords[:,0]*(spectrogram.shape[1]/duration)
        contoury = spectrogram.shape[0]-(curve_coords[:,1]*(spectrogram.shape[0]/(new_sr//2)))

        # Match contour actual with pixel values
        contourx_int = np.arange(round(min(contourx)), round(max(contourx))+1, 1)
        contoury_int = np.round(np.interp(
                contourx_int,
                contourx,
                contoury
            )).astype(int)

        # except if x outside of range we have to clip the contour
        contoury_int = contoury_int[contourx_int < spectrogram.shape[1]]
        contourx_int = contourx_int[contourx_int < spectrogram.shape[1]]

        if better and (key[0] != "h"):
            # find local_maxima y at each timestamp x
            new_contoury_int = np.copy(contoury_int) 

            for i, x in enumerate(contourx_int):
                border_up = contoury_int[i]+interval
                border_down = contoury_int[i]-interval

                yinterval = spectrogram[
                    max(0, border_down):min(border_up, spectrogram.shape[0]-1)+1, 
                    x]
                
                # ez way
                new_y = border_down+np.argmax(yinterval)
                new_contoury_int[i] = round(new_y)

            # add smoothing (moving average)
            smooth_contourx = moving_average(contourx_int, n=smoothing).astype(int)
            smooth_contoury = np.round(moving_average(new_contoury_int, n=smoothing)).astype(int)

            # add back missing values
            n_missing = (smoothing-1)//2
            smooth_contourx = np.append(np.append(contourx_int[:n_missing], smooth_contourx), contourx_int[-n_missing:]).astype(int)
            smooth_contoury = np.append(np.append(new_contoury_int[:n_missing], smooth_contoury), new_contoury_int[-n_missing:]).astype(int)

            new_contours[key] = np.array([smooth_contourx, smooth_contoury]).T
        else:
            new_contours[key] = np.array([contourx_int, contoury_int]).T
    return new_contours

def qqplot_perso(x):
    fig = sm.qqplot(x, line='45', fit=True)
    s, p = stats.shapiro(x)
    if p < 0.05:
        concl = "H0 rejected (distrib not normal)"
        color="red"
    else:
        concl = "H0 cannot be rejected (normal distrib)"
        color="green"
    fig.axes[0].text(
        0.95, 0.05,
        f'shapiro-wilk: s={s:.2f}, p={p:.2g}\n'+concl,
        transform = fig.axes[0].transAxes,
        ha='right', va='bottom',
        color=color
    )
    fig.suptitle(f"QQ plot.")
    return fig

def confidence_ellipse(x, y, ax, n_std=3.0, facecolor='none', **kwargs):
    """
    Create a plot of the covariance confidence ellipse of *x* and *y*.

    Parameters
    ----------
    x, y : array-like, shape (n, )
        Input data.

    ax : matplotlib.axes.Axes
        The axes object to draw the ellipse into.

    n_std : float
        The number of standard deviations to determine the ellipse's radiuses.

    **kwargs
        Forwarded to `~matplotlib.patches.Ellipse`

    Returns
    -------
    matplotlib.patches.Ellipse
    """
    if x.size != y.size:
        raise ValueError("x and y must be the same size")

    cov = np.cov(x, y)
    pearson = cov[0, 1]/np.sqrt(cov[0, 0] * cov[1, 1])
    # Using a special case to obtain the eigenvalues of this
    # two-dimensional dataset.
    ell_radius_x = np.sqrt(1 + pearson)
    ell_radius_y = np.sqrt(1 - pearson)
    ellipse = Ellipse((0, 0), width=ell_radius_x * 2, height=ell_radius_y * 2,
                      facecolor=facecolor, **kwargs)

    # Calculating the standard deviation of x from
    # the squareroot of the variance and multiplying
    # with the given number of standard deviations.
    scale_x = np.sqrt(cov[0, 0]) * n_std
    mean_x = np.mean(x)

    # calculating the standard deviation of y ...
    scale_y = np.sqrt(cov[1, 1]) * n_std
    mean_y = np.mean(y)

    transf = transforms.Affine2D() \
        .rotate_deg(45) \
        .scale(scale_x, scale_y) \
        .translate(mean_x, mean_y)

    ellipse.set_transform(transf + ax.transData)
    return ax.add_patch(ellipse)

def custom_draftmans_plot(data, old_names=[], new_names=[]):
    """
        Spearman.
    """
    lowess = sm.nonparametric.lowess
    fig, axs = plt.subplots(
        len(data.columns), len(data.columns),
        sharex="col", sharey="row",
        figsize=(13,8))

    # remove histograms from sharing y-axis
    axs[0,0].set_yticks([])
    for i in range(len(data.columns)):
        try:
            axs[i,i].get_shared_y_axes().remove(axs[i,i])
        except:
            raise ValueError("Cannot remove shared y axis without matplotlib==3.5.3")

        if (len(old_names) + len(new_names)) != 0:
            new_name = new_names[np.where(np.array(old_names) == data.columns[i])[0][0]]
        else:
            new_name = data.columns[i]
        axs[i, 0].set_ylabel(new_name)
        axs[-1, i].set_xlabel(new_name)

    for i in range(len(data.columns)):
        for j in range(i+1):
            if i==j:
                sns.histplot(
                    data[data.columns[j]],
                    stat="density",
                    ax=axs[i, j],
                    facecolor="#00AFBB")
                sns.kdeplot(
                    data[data.columns[j]],
                    color="black",
                    lw=.5, alpha=1,
                    ax=axs[i, j])
                
            else:
                data_no_nan = data.dropna(subset = [data.columns[j], data.columns[i]]).copy()

                # below diagonal is scatterplot
                axs[i, j].scatter(                
                    data[data.columns[j]],
                    data[data.columns[i]],
                    color="black", s=1)
                # with lowess
                z = lowess( 
                    data_no_nan[data.columns[i]].values,
                    data_no_nan[data.columns[j]].values,
                    frac=1./5)
                axs[i, j].plot(
                    z[:,0], z[:,1],
                    color="red"
                )

                # with confidence ellipse
                confidence_ellipse(
                    x=data_no_nan[data.columns[j]].values, 
                    y=data_no_nan[data.columns[i]].values, 
                    ax=axs[i, j], 
                    n_std=1,
                    edgecolor='black')
                axs[i, j].scatter(
                    np.mean(data_no_nan[data.columns[j]].values),
                    np.mean(data_no_nan[data.columns[i]].values),
                    s=25, color="red"
                )

                # above diagonal is Pearson correlation value
                r = stats.spearmanr(
                    data_no_nan[data.columns[j]], 
                    data_no_nan[data.columns[i]])[0]
                axs[j, i].text(
                    s = f"{r:.2f}",
                    x = 0.5, y = 0.5,
                    va = 'center', ha = 'center',
                    transform = axs[j, i].transAxes,
                    fontsize = 32
                )
        xmin = np.nanmin(data[data.columns[i]])
        xmax = np.nanmax(data[data.columns[i]])
        axs[0, i].set_xlim(
            xmin-0.05*(xmax-xmin), 
            xmax+0.05*(xmax-xmin))

    plt.subplots_adjust(
        left  = 0.05, right = 0.95,
        bottom = 0.05, top = 0.95,
        hspace=0.1, wspace=0.1)
    return fig, axs


#%%## PLOT WIDGETS #####
def plot_contours_on_spectrogram(spec_img, coord_dict, max_t=None, max_f=None, block=True, etiquettes=[]):
    if not isinstance(etiquettes, list):
        raise ValueError("'etiquettes' must be a list.")

    fig, axs = plt.subplots(1, 1, sharex=True, sharey=True)
    if (isinstance(max_t, int) or isinstance(max_t, float)) and ((isinstance(max_f, int) or isinstance(max_f, float))):
        axs.imshow(
            spec_img,
            cmap="jet", 
            extent=(0, max_t, 0, max_f),
            aspect="auto",
            interpolation='none')
    else:
        axs.imshow(
            spec_img,
            cmap="jet", 
            aspect="auto",
            interpolation='none')

    keys = list(coord_dict.keys())
    for k, key in enumerate(keys):
        if len(etiquettes) > 0:
            if etiquettes[k] == "":
                axs.plot(
                    np.array(coord_dict[key])[:,0],
                    np.array(coord_dict[key])[:,1])
            else:
                axs.plot(
                    np.array(coord_dict[key])[:,0],
                    np.array(coord_dict[key])[:,1],
                    color="white")
                axs.annotate(
                    etiquettes[k], 
                    (
                        np.mean(np.array(coord_dict[key])[:,0]), 
                        np.mean(np.array(coord_dict[key])[:,1])
                    ),
                    color="white")
        else:
            axs.plot(
                np.array(coord_dict[key])[:,0],
                np.array(coord_dict[key])[:,1])

    fig.tight_layout()
    plt.show(block=block)
    plt.close(fig)

def scatter_with_etiquettes(points, labels):
    fig, ax = plt.subplots()
    ax.scatter(points[:,0], points[:,1],
        c=labels, s=5, cmap="prism") 
    for label in np.unique(labels):
        if label==-1:
            continue
        ax.annotate(
            label, 
            (
                np.mean(points[np.where(labels == label)[0],0]), 
                np.mean(points[np.where(labels == label)[0],1])
            )
        )
    ax.set_title("Data with etiquettes")
    ax.set_ylabel("Azimuths (in deg)")
    ax.set_xlabel("Time (in sec)")
    plt.show(block=True)

def plot_data_segmentation(points_df, deriv_points_df, interval_categories, block=True, threshold=None):
    """
        A function to plot a figure with two axis : data points
        and their derivates with segmentation categories in
        background.

        Parameters
        ----------
        points_df : pd.DataFrame
            Must contain columns ["time_in_sequence", "azimuths"]
        deriv_points_df : pd.DataFrame
            Must contain columns ["deriv_x", "deriv_y"]
        interval_categories : pd.DataFrame
            Must contain columns ["xmin", "xmax", "category"]
        block : bool, optional
            Whether to block or not the plot on screen.
            Default is True.
        threshold : float, optionnal
            The threshold for the categorization of derivatives (ON/OFF-axis)
            Default is None (no threshold displayed)

        Raises
        ------
        ValueError
            In case a category in interval_categories is not in
            ['ON-AXIS', 'OFF-AXIS', 'UNKNOWN']
    """
    plt.close("all")
    fig, axs = plt.subplots(1, 2, figsize=(12,7.5), sharex=True)

    azimuths = axs[0].scatter(
        points_df["time_in_sequence"], 
        points_df["azimuth"], 
        s=5, color="black", label="azimuths")
    dazimuths = axs[1].scatter(
        deriv_points_df["deriv_x"], 
        deriv_points_df["deriv_y"], 
        s=5, color="black", label="d(azimuth)/dt")
   
    for _, row in interval_categories.iterrows():
        for i in range(2):
            if row["category"] == "ON-AXIS":
                axs[i].axvspan(
                    row["xmin"], row["xmax"], 
                    alpha=0.15, fc="green", ec=None)
            elif row["category"] == "OFF-AXIS":
                axs[i].axvspan(
                    row["xmin"], row["xmax"], 
                    alpha=0.15, fc="red", ec=None)       
            elif row["category"] == "UNKNOWN": 
                axs[i].axvspan(
                    row["xmin"], row["xmax"], 
                    alpha=0.5, fc="gray", ec=None)    
            else:
                raise ValueError(f"'{row['category']}' category not in ['ON-AXIS', 'OFF-AXIS', 'UNKNOWN']")
    
    if isinstance(threshold, int) or isinstance(threshold, float):
        hline = axs[1].axhline(y=threshold, color="red")
    else:
        hline = None

    axs[0].set_title("Segmentation in time (Angles).")
    axs[0].legend(
        handles=[
            Polygon(np.zeros((4,2)), facecolor="green", alpha=0.15, label="ON-AXIS"),
            Polygon(np.zeros((4,2)), facecolor="red", alpha=0.15, label="OFF-AXIS"),
            Polygon(np.zeros((4,2)), facecolor="gray", alpha=0.5, label="UNKNOWN"),
            azimuths
            ],
        loc="upper right"
        )
    axs[1].set_title("Segmentation in time (d(angles)).")
    axs[1].legend(
        handles=[
            Polygon(np.zeros((4,2)), facecolor="green", alpha=0.15, label="ON-AXIS"),
            Polygon(np.zeros((4,2)), facecolor="red", alpha=0.15, label="OFF-AXIS"),
            Polygon(np.zeros((4,2)), facecolor="gray", alpha=0.5, label="UNKNOWN"),
            dazimuths, hline
            ],
        loc="upper right"
        )
    plt.show(block=block)  

def educated_whistle_annotation(wavefile_path, segments_df, sequence_data, date, title="", re_sr=96_000):
    """
        A function that plots 2-axis figure :
            - top axis contain the segments (ON/OFF-axis)
            - bottom axis contain the spectrgram image of the audio to annotate
            it is interactive to extract whistles easily.

        Parameters
        ----------
        wavefile_path : str
            Path to a wavefile.
        segments_df : pd.DataFrame 
            Must contain columns ["xmin", "xmax", "category"]
        sequence_data : pd.DataFrame
            Must contain columns ["file_date", "time_in_file", "azimuth"]
        date : datetime object
            Datetime of the wavefile to use.
        re_sr : int, optional
            Sampling rate of the wavefile to show as spectrogram.
            Default is 64_000.
        title : str, optionnal
            Main title for the figure
            Default is "".

        Returns
        -------
        json dict
            Coordinates of the whistles drawn (seconds, frequency)
    """
    # data preparation
    with wave.open(wavefile_path, "rb") as f:
        duration = f.getnframes()/f.getframerate()

    sequence_data_date = sequence_data.where(
        (sequence_data["file_date"] == date)
    ).dropna().copy()
    start_time = sequence_data_date["time_in_sequence"].min()
    end_time = sequence_data_date["time_in_sequence"].max()
    delay = sequence_data_date["time_in_file"].min()

    subsegments_df = segments_df.where(
        (segments_df["xmax"] >= start_time-delay) &
        (segments_df["xmin"] <= end_time+delay)
    )[["xmin", "xmax", "category", "median_dangle"]].dropna().copy()

    # plot
    plt.close("all")
    fig, axs = plt.subplots(2, 1, sharex=True, height_ratios=[0.15, 0.85])

    local_segements = pd.DataFrame(columns=["xmin", "xmax", "category", "median_dangle"])
    for _, subrow in subsegments_df.iterrows():
        if (subrow["category"] == "ON-AXIS"):
            axs[0].axvspan(
                subrow["xmin"] + delay - start_time, 
                subrow["xmax"] + delay - start_time, 
                alpha=0.15, fc="green", ec=None)
        elif (subrow["category"] == "OFF-AXIS"):
            axs[0].axvspan(
                subrow["xmin"] + delay - start_time, 
                subrow["xmax"] + delay - start_time, 
                alpha=0.15, fc="red", ec=None)    
        elif (subrow["category"] == "UNKNOWN"):
            axs[0].axvspan(
                subrow["xmin"] + delay - start_time, 
                subrow["xmax"] + delay - start_time, 
                alpha=0.5, fc="gray", ec=None)   
        else:
            raise ValueError(f"'{subrow['category']}' category is not in ['ON-AXIS', 'OFF-AXIS', 'UNKNOWN']")
        local_segements.loc[len(local_segements)] = [
            subrow["xmin"] + delay - start_time,
            subrow["xmax"] + delay - start_time,
            subrow["category"],
            subrow["median_dangle"]
        ]

    if len(local_segements)==0:
        axs[0].axvspan(
            0, duration, 
            alpha=0.5, fc="gray", ec=None)   

    axs[0].scatter(
        sequence_data_date["time_in_file"], 
        sequence_data_date["azimuth"], s=5, color="black")

    axs[0].set_title("Segmentation in time.")
    axs[0].legend(
        handles=[
            Polygon(np.zeros((4,2)), facecolor="green", alpha=0.15, label="ON-AXIS"),
            Polygon(np.zeros((4,2)), facecolor="red", alpha=0.15, label="OFF-AXIS"),
            Polygon(np.zeros((4,2)), facecolor="gray", alpha=0.5, label="UNKNOWN"),
            ],
        loc="upper right"
        )

    # audio transform
    # added a high_pass filter at 250 Hz
    img, _ = load_spectrogram(
        wavefile_path,
        re_sr,
        for_display=True
    )
    
    axs[1].imshow(
        img, 
        cmap="jet", 
        extent=(0, duration, 0, re_sr//2),
        aspect="auto",
        interpolation='none')
    base = clicker(axis=axs[1], bspline="quadratic", maxlines=9999)

    axs[0].set_yticklabels([])
    fig.suptitle(title, fontsize=15)
    plt.subplots_adjust(wspace=0, hspace=0)

    # PyAVA
    plt.show(block=True)
    plt.close("all")

    return base.coords, local_segements

def blind_whistle_annotation(wavefile_path, sequence_data, date, title="", re_sr=96_000):
    # data preparation
    with wave.open(wavefile_path, "rb") as f:
        duration = f.getnframes()/f.getframerate()

    sequence_data_date = sequence_data.where(
        (sequence_data["file_date"] == date)
    ).dropna().copy()
    start_time = sequence_data_date["time_in_sequence"].min()
    end_time = sequence_data_date["time_in_sequence"].max()
    delay = sequence_data_date["time_in_file"].min()

    # plot
    plt.close("all")
    fig, ax = plt.subplots(1, 1, sharex=True)

    # audio transform
    # added a high_pass filter at 250 Hz
    img, _ = load_spectrogram(
        wavefile_path,
        re_sr,
        for_display=True
    )
    
    ax.imshow(
        img, 
        cmap="jet", 
        extent=(0, duration, 0, re_sr//2),
        aspect="auto",
        interpolation='none')
    base = clicker(axis=ax, bspline="quadratic", maxlines=9999)

    fig.suptitle(title, fontsize=15)
    plt.subplots_adjust(wspace=0, hspace=0)

    # PyAVA
    plt.show(block=True)
    plt.close("all")

    return base.coords
 
class TransformData:
    """
        A very complicated object for a simple action:
            By pressing the arrows on the keyboard while plot is in focus
            angles in given dataframe will be shifted in [-180, 180]° space.

        After closing, angle bias can be accessed with attribute:
        TransformData.angle_bias. 

        Example of use:
            transformation = TransformData(
                np.array([time, angles]).T, 
                mode="deg")
            shift = transformation.angle_bias
            
        Parameters
        ----------
        data : array-like
            Data to be plotted, must be of shape (N, 2).
            data[:,0] will be the timestamps
            data[:,1] will be the angles
            angles must be in degrees.
    """
    def __init__(self, data):
        # define variables
        self.data = data
        self.angle_bias = 0

        # compute angles
        self.azimuth_tdoa = modulo360(
            np.copy(self.data[:,1]), self.angle_bias)
        self.timings_tdoa = np.copy(self.data[:,0])
        
        # make plot
        self.fig, self.ax = plt.subplots(1,1, figsize=(12,7.5))

        self.tdoa_plot, = self.ax.plot(
            self.timings_tdoa, self.azimuth_tdoa, 
            '.', markersize=5, label="TDOA", color="tab:blue")

        self.ax.legend(loc="lower left")
        self.ax.set_title(f"Panning by {self.angle_bias}°")
        self.ax.set_ylim(-180-10,180+10)
        self.ax.set_xlim(
            self.timings_tdoa.min()-((self.timings_tdoa.max()-self.timings_tdoa.min())*0.05), 
            self.timings_tdoa.max()+((self.timings_tdoa.max()-self.timings_tdoa.min())*0.05))
        
        # add events
        self.fig.canvas.mpl_connect("key_press_event", self.key_pressed)

        # initialise updates 
        self.update_plot()
        plt.show()

    def key_pressed(self, event):
        change = False
        # control bias by moving delay part 
        if "up" in event.key:
                change = True
                if event.key == "ctrl+up":
                    self.angle_bias += 10
                elif event.key == "up":
                    self.angle_bias += 1
                elif event.key == "shift+up":
                    self.angle_bias += 0.1
        elif "down" in event.key:
                change = True
                if event.key == "ctrl+down":
                    self.angle_bias -= 10
                elif event.key == "down":
                    self.angle_bias -= 1
                elif event.key == "shift+down":
                    self.angle_bias -= 0.1
        
        # general key
        elif "X" in event.key:
            plt.close()

        if change:
            self.update_plot()

    def update_plot(self):
        # update starting point
        self.azimuth_tdoa = modulo360(
            np.copy(self.data[:,1]), self.angle_bias)

        # update plot
        self.tdoa_plot.set_data(self.timings_tdoa, self.azimuth_tdoa)
        self.ax.set_title(f"Panning by {self.angle_bias}°")
        self.fig.canvas.draw()

def AutoClusters(points, time_dilatation=15):
    """
        A function that clusterize points into groups
        Based on their proximity, using HDBSCAN method.

        Parameters
        ----------
        points : numpy.array
            Array of shape (N, 2) with N the number of points.
            points[:, 0] refers to the time column in seconds.
            points[:, 1] refers to the angle column in degrees.
        time_dilatation : int, optional
            Multiplicative factor for time column.
            If too low : 
            If too high : 
            Default factore is 15.

        Returns
        -------
        labels : numpy.array
            The labels for each point corresponding to 
            their attributed cluster.

        Raises
        ------
        ValueError
            Security raised when 'points' is not of the right shape.
    """
    if (points.ndim != 2) or (points.shape[1] != 2):
        raise ValueError(
            "'points' shouuld be of general shape (N,2). "
            f"Got {points.shape}.")
    
    points = np.copy(points)
    points[:,0] = points[:,0]*time_dilatation

    cluster_method = HDBSCAN(
        min_cluster_size=3, cluster_selection_epsilon=2.5,
        cluster_selection_method="leaf")
    
    return cluster_method.fit(points).labels_
    
class SelectFromCollection:
    """
        A widget that can be implemented into a matplotlib figure.
        It allows for the manual selection of groups of points.

        Example usage:
            ax = plt.subplot()
            pts = ax.scatter(
                points[:,0], points[:,1], 
                s=5)
            selector = SelectFromCollection(ax, pts)
            plt.show()

        Parameters
        ----------
        ax : matplotlib.Axes object
            The parent Axes for the widget.
        collection : matplotlib.collections.PathCollection object
            The output of ax.scatter()
        
        Useful Attributes
        -----------------
        labels : numpy.array
            The labels associated to each point in collection.
    """
    def __init__(self, ax, collection):

        self.ax = ax
        self.canvas = self.ax.figure.canvas
        self.collection = collection
        self.default_color = np.array(colors.to_rgba('black'))
        self.list_colors = [
        np.array(colors.hex2color(col)) 
            for col in ['#1f77b4', '#ff7f0e', '#2ca02c', '#d62728', '#9467bd', '#8c564b', '#e377c2', '#7f7f7f', '#bcbd22', '#17becf']]
        self.xys = collection.get_offsets()
        self.Npts = len(self.xys)

        # Ensure that we have separate colors for each object
        self.fc = collection.get_facecolors()
        if len(self.fc) == 0:
            raise ValueError('Collection must have a facecolor')
        elif len(self.fc) == 1:
            self.fc = np.tile(self.fc, (self.Npts, 1))
        self.fc[:, :-1] = self.default_color[:-1]
        self.collection.set_facecolors(self.fc)
        self.last_fc = np.copy(self.fc)
        self.lasso = LassoSelector(self.ax, onselect=self.onselect)
        self.ind = []

        # Ensure we have labels
        self.labels = np.zeros(len(self.fc))
        self.prev_labels = np.zeros(len(self.fc))
        self.count_labels = 0
        self.canvas.mpl_connect("key_press_event", self.key_parser)
        self.ax.set_title(
            "Press 'Shift+R' to reset selection"
            "\nPress 'Z' to cancel last selection")
        self.authorize_cancel = False

        # Display useful informations
        self.textbox = self.ax.text(
            0.05, 0.95, 
            (f"{self.count_labels} groupes selected"
                f"\n{np.count_nonzero(self.labels==0)} points not selected"), 
            transform=self.ax.transAxes, 
            fontsize=12,
            verticalalignment='top', 
            bbox=dict(
                boxstyle='round', 
                facecolor='white', 
                alpha=0.5))
        self.canvas.draw_idle()

    def onselect(self, verts):
        self.prev_labels = np.copy(self.labels)
        self.last_fc = np.copy(self.fc)
        path = Path(verts)
        self.ind = np.nonzero(path.contains_points(self.xys))[0]
        self.count_labels += 1
        self.labels[self.ind] = self.count_labels
        self.textbox.set_text(f"{self.count_labels} groupes selected"
                f"\n{np.count_nonzero(self.labels==0)} points not selected")
        self.fc[self.ind, :-1] = self.list_colors[int(self.count_labels)%len(self.list_colors)]
        self.collection.set_facecolors(self.fc)
        self.canvas.draw_idle()
        self.authorize_cancel = True

    def key_parser(self, event):
        if event.key == "R":
            self.reset()
        elif event.key == "z":
            self.cancel_last()

    # new event to reset plot
    def reset(self):
        # reset all variables
        self.count_labels = 0
        self.labels = np.zeros(len(self.fc))
        self.textbox.set_text(f"{self.count_labels} groupes selected"
                f"\n{np.count_nonzero(self.labels==0)} points not selected")
        self.fc[:, :-1] = self.default_color[:-1]
        self.collection.set_facecolors(self.fc)
        self.canvas.draw_idle()
        self.authorize_cancel = False

    # cancel last action
    def cancel_last(self):
        if self.authorize_cancel:
            self.labels = np.copy(self.prev_labels)
            self.fc = np.copy(self.last_fc)
            self.count_labels -= 1
            self.textbox.set_text(f"{self.count_labels} groupes selected"
                f"\n{np.count_nonzero(self.labels==0)} points not selected")
            self.collection.set_facecolors(self.fc)
            self.canvas.draw_idle()
            self.authorize_cancel = False

def ManualClusters(points):
    """
        A function that allows the user to interact with
        a matplotlib plot to make clusters of points.

        Parameters
        ----------
        points : numpy.array
            Array of shape (N, 2) with N the number of points.
            points[:, 0] refers to the time column in seconds.
            points[:, 1] refers to the angle column in radians.

        Returns
        -------
        labels : numpy.array
            The labels for each point corresponding to 
            their attributed cluster.

        Raises
        ------
        ValueError
            Security raised when 'points' is not of the right shape.
    """
    # inpired by me (cluster_it_yourself function)
    if (points.ndim != 2) or (points.shape[1] != 2):
        raise ValueError(
            "'points' shouuld be of general shape (N,2). "
            f"Got {points.shape}.")
    
    rcParams["keymap.pan"] = ['p', 'w']
    plt.close("all")
    fig = plt.figure()
    ax = fig.add_subplot(111)
    pts = ax.scatter(
        points[:,0], points[:,1], 
        s=5)
    selector = SelectFromCollection(ax, pts)
    plt.show()
    rcParams["keymap.pan"] = ['p']

    return selector.labels

#%%## LEGACY #####
# def extract_bin_values(df, annotation_files, re_sr=96_000):
#     # fetch contours infos
#     bin_values_summarized = pd.DataFrame()
#     for file in tqdm(annotation_files, "file", position=1, leave=False):
#         file_datetime = datetime.strptime(os.path.basename(file)[:-5], '%Y%m%d_%H%M%S')
        
#         tqdm.write(f"\n\tfile: {file_datetime.strftime('%Y%m%d_%H%M%S')}")

#         # load contours
#         original_contours = import_contours_to_curves(file)

#         audio_path = ("/media/loic/Extreme SSD/Acoustique/" +
#                 f"{file_datetime.strftime('%Y')}/Antenne/" +
#                 f"{file_datetime.strftime('%d%m%Y')}/wavs/" +
#                 f"{file_datetime.strftime('%Y%m%d_%H%M%S')}UTC_V12.wav") 
#         tqdm.write(f"\t\t{len(original_contours)} annotations loaded.")
#         spec, duration = load_spectrogram(
#             audio_path, new_sr=re_sr, for_display=True)
#         tqdm.write(f"\t\tSpectrogram loaded.")
       
#         pixel_contours = refine_contours2pixels(
#             original_contours, 
#             new_sr=re_sr,
#             wavepath=audio_path,
#             interval=3,
#             better=True)

#         # compute SNR of each contour  
#         # using minimum not mean because too much noise
#         for key in list(pixel_contours.keys()):
#             noise_per_x = np.min(spec[
#                 min(pixel_contours[key][:,1]):(max(pixel_contours[key][:,1])+1),
#                 min(pixel_contours[key][:,0]):(max(pixel_contours[key][:,0])+1),
#             ], axis=0)
#             signal_db_per_x = spec[pixel_contours[key][:,1], pixel_contours[key][:,0]]

#             pixel_SNRs = signal_db_per_x-noise_per_x

#             whistle_start = original_contours[key][:,0].min()
#             whistle_end = original_contours[key][:,0].max()

#             pixel_angular_speeds = []
#             for t in range(min(pixel_contours[key][:,0]), (max(pixel_contours[key][:,0])+1)):
#                 t_in_secs = ((t)/spec.shape[1])*duration
#                 df_selection  = df.where(
#                     (df["time_min_in_file"] <= (whistle_start+t_in_secs)) &
#                     (df["time_max_in_file"] >= (whistle_start+t_in_secs)) &
#                     (df["file_date"] == file_datetime.strftime('%Y-%m-%d %H:%M:%S'))
#                 ).dropna(how="all").copy()

#                 print(df_selection["angular_speed"])
#                 pixel_angular_speeds += [df_selection["angular_speed"].mean()]
#             print(pixel_angular_speeds)

#             # add to file_values dataframe
#             local_df = pd.DataFrame(
#                 data={
#                     "datetime": [file_datetime]*len(pixel_SNRs),
#                     "contour_name": [key]*len(pixel_SNRs),
#                     "angular_speed": pixel_angular_speeds,
#                     "SNR": pixel_SNRs,
#                     "frequency": (re_sr//2)-((pixel_contours[key][:,1]/spec.shape[0])*(re_sr//2)),
#                 }
#             )

#             bin_values_summarized = pd.concat([bin_values_summarized, local_df.copy()])

#     return bin_values_summarized

