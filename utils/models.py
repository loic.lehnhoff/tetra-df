#%%## Importations #####
import os
import copy
import warnings
import numpy as np
import pandas as pd
from tqdm import tqdm

import matplotlib.pyplot as plt

import torch
from torch import nn, optim
from sklearn.preprocessing import StandardScaler

from utils.utils import BColors, find_device
from utils.geometry import tdoa_residuals_solve


#%%## Definitions #####
class DResNet(nn.Module):
    """
    Architecture and forward pass for a PyTorch Model.
    The model is a ResNet of 1D.

    Parameters
    ----------
    len_input (int):
        Length of the 1D tensor in input.
    len_output (int):
        Length of the 1D tensort in output.
    """
    def __init__(self, len_input, len_output):
        self.len_input = len_input
        self.len_output = len_output
        super(DResNet, self).__init__()
        
        self.flatten = nn.Flatten()

        self.fc1 = nn.Sequential(
            nn.Linear(self.len_input, 128),
            nn.ReLU())
        
        self.fc1b = nn.Sequential(
            nn.Linear(128, 128),
            nn.ReLU())

        self.fc2 = nn.Sequential(
            nn.Linear(128+self.len_input, 64),
            nn.ReLU())

        self.fc3 = nn.Sequential(
            nn.Linear(64+128+self.len_input, 16),
            nn.ReLU())
        
        self.fc4 = nn.Linear(16+64+128+self.len_input, self.len_output)

    def forward(self, x):
        """
        Forward pass in model

        Parameters
        ----------
        x : torch.tensor
            Data to use for prediction.
            Shape should be (N, len_input)

        Returns
        -------
        out : torch.tensor
            y data predicted
        """
        flat = self.flatten(x)
        x1 = self.fc1(flat)
        x1b = self.fc1b(x1)
        x2 = self.fc2(torch.cat([flat, x1b], dim=1))
        x3 = self.fc3(torch.cat([flat, x1b, x2], dim=1))
        out = self.fc4(torch.cat([flat, x1b, x2, x3], dim=1))

        return out

class NewMLP(nn.Module):
    """
    Architecture and forward pass for a PyTorch Model.
    The model is a MLP of 1D.

    Parameters
    ----------
    len_input (int):
        Length of the 1D tensor in input.
    len_output (int):
        Length of the 1D tensort in output.
    """
    def __init__(self, len_input, len_output):
        self.len_input = len_input
        self.len_output = len_output
        super(NewMLP, self).__init__()
        
        self.flatten = nn.Flatten()

        self.fc1 = nn.Sequential(
            nn.Linear(self.len_input, 16),
            nn.ReLU())
        
        self.fc1b = nn.Sequential(
            nn.Linear(16, 16),
            nn.ReLU())


        self.fc2 = nn.Sequential(
            nn.Linear(16, 8),
            nn.ReLU())

        self.fc3 = nn.Sequential(
            nn.Linear(8, 4),
            nn.ReLU())
        
        self.fc4 = nn.Linear(4, self.len_output)

    def forward(self, x):
        """
        Forward pass in model

        Parameters
        ----------
        x : torch.tensor
            Data to use for prediction.
            Shape should be (N, len_input)

        Returns
        -------
        out : torch.tensor
            y data predicted
        """
        flat = self.flatten(x)
        x1 = self.fc1(flat)
        x1b = self.fc1b(self.fc1b(self.fc1b(x1)))
        x2 = self.fc2(x1b)
        x3 = self.fc3(x2)
        out = self.fc4(x3)

        return out

class train_model:
    """
        Class to train a Pytorch model.

        Parameters
        ----------
        model : LeModel object
            Pytorch model to train
        X : Pytorch tensor
            Tensor that will be used as input for training.
        y : Pytorch tensort
            Tensor that will be used as reference output for training.
        X_val : Pytorch tensor
            Tensor that will be used as input for validation.
        y_val: Pytorch tensort
            Tensor that will be used as reference output for training.
        learning_rate : float
            Default is 1e-2
        max_epochs : integer
            Maximum number of epochs default is 10000
        print_every : integer
            Print loss at current epoch every 'print_every' epoch.
            Default is 0 (no print)
        end_lr_factor : float
            Factor to multiply learning rate by at the end of epochs.
            When given, learning rate will follow a linear increase/decrease
        plot : str
            If plot == "false", no plot is displayed nor saved.
            If plot == "true", displays plot.
            If plot is a path, saves plot to this path.
            Default is "true".
    """
    def __init__(
    self, 
    model, 
    X, 
    y, 
    X_val=[], 
    y_val=[], 
    learning_rate=1e-2, 
    max_epochs=10000, 
    print_every=0,  
    end_lr_factor=1,
    plot="true",
    labels=[],
    plot_every=-1,
    early_stopping_epochs=50,
    min_loss_diff=-1):
        self.optimizer = optim.Adam(model.parameters(), lr=learning_rate) 
        self.model = model
        self.X = X
        self.y = y
        self.X_val = X_val
        self.y_val = y_val
        self.learning_rate = learning_rate
        self.max_epochs = max_epochs
        self.print_every = print_every
        self.end_lr_factor = end_lr_factor
        self.plot = plot
        self.labels = labels
        self.plot_every = plot_every
        self.early_stopping_epochs = early_stopping_epochs
        self.min_loss_diff = min_loss_diff

        if isinstance(self.end_lr_factor, float):
            self.scheduler = optim.lr_scheduler.LinearLR(self.optimizer, 
                start_factor=1, end_factor=end_lr_factor, total_iters=max_epochs)


        self.training()

    def custom_loss(self, pred, truth):
        """
        Error in proportion of distance 
        (gives more weight to closest points)
        """
        loss_per_point = torch.abs(pred-truth)
        proportion_loss = loss_per_point/torch.abs(truth)
        return torch.mean(proportion_loss*100)

        # """
        # Mean Absolute Error (Same measure for all distances).
        # """
        # return torch.mean(torch.abs(pred-truth))

    def training(self):
        self.loss_train = []
        if len(self.y_val)>0:
            if isinstance(self.y_val, list):
                self.loss_val = [[] for i in range(len(self.y_val))]
            else:
                self.loss_val = []
        
        self.best_epoch = 0
        self.best_loss = np.inf

        for epoch in range(self.max_epochs):
            # training
            y_pred = self.model(self.X)  # forward pass
            loss = self.custom_loss(y_pred, self.y)  # compute loss

            if (epoch > 1) and (loss < np.min(self.loss_train)):
                # store best iteration
                self.best_model = copy.deepcopy(self.model)
                self.best_epoch = epoch
                self.best_loss = loss

            self.optimizer.zero_grad()  # reset gradients
            loss.backward()  # backpropagate gradients
            self.optimizer.step()  # update weights (gradient descent step)
            if isinstance(self.end_lr_factor, float):
                self.scheduler.step()           
            self.loss_train.append(loss.item())

            # validation
            if len(self.y_val)>0:
                if isinstance(self.y_val, list):
                    for idx, _ in enumerate(self.loss_val):
                        y_pred = self.model(self.X_val[idx])
                        loss = self.custom_loss(y_pred, self.y_val[idx])
                        self.loss_val[idx].append(loss.item())
                else:
                    y_pred = self.model(self.X_val)
                    loss = self.custom_loss(y_pred, self.y_val)
                    self.loss_val.append(loss.item())

            if self.print_every != 0:
                if epoch % self.print_every == 0:
                    message = f"[EPOCH]: {epoch:<10}[LOSS]: {self.loss_train[-1]:<10.3f}"
                    if len(self.y_val)>0:
                        if isinstance(self.y_val, list):
                            for i, _ in enumerate(self.loss_val):
                                message = message + f"[VAL{i}]: {self.loss_val[i][-1]:<10.3f}"
                        else:
                            message = message + f"[VAL]: {self.loss_val[-1]:<10.3f}"
                    print(message)
            
            if self.plot_every > 0:
                if (epoch > 1) and (epoch % self.plot_every == 0):
                    self.plot_training(current_epoch=epoch)
                    self.plot_training_confusion(current_epoch=epoch)

            # early stopping          
            if (((self.max_epochs-self.best_epoch)>self.early_stopping_epochs) and
                (torch.abs(self.best_loss-loss)<self.min_loss_diff)):
                if (self.print_every != 0):
                    message = f"[EARLY]: {epoch:<10}[LOSS]: {self.loss_train[-1]:<10.3f}"
                    if len(self.y_val)>0:
                        if isinstance(self.y_val, list):
                            for i, _ in enumerate(self.loss_val):
                                message = message + f"[VAL{i}]: {self.loss_val[i][-1]:<10.3f}"
                        else:
                            message = message + f"[VAL]: {self.loss_val[-1]:<10.3f}"
                    print(message)
                break

        if self.plot != "false":
            self.plot_training(current_epoch="max")

    def plot_training(self, current_epoch):
        self.fig = plt.figure(figsize=(9,9))
        self.ax = self.fig.add_subplot()

        self.train_line, = plt.plot(self.loss_train, label='loss')
        if len(self.y_val)>0:
            if isinstance(self.y_val, list):
                for i, _ in enumerate(self.loss_val):
                    if len(self.labels)>0:
                        self.val_line, = plt.plot(self.loss_val[i], label=self.labels[i]+"_loss")
                    else:
                        self.val_line, = plt.plot(self.loss_val[i], label='val_loss')
            else:
                self.val_line, = plt.plot(self.loss_val, label='val_loss')
        self.ax.set_xlabel("Epoch")
        self.ax.set_ylabel("Loss")
        self.ax.set_ylim(0, np.max(self.y.detach().cpu().numpy()))
        self.ax.legend()
        self.ax.grid(True)

        if self.plot == "true":
            self.fig.show()
        elif os.path.exists(os.path.split(self.plot)[0]):
            self.fig.savefig(self.plot[:-4]+f"_epochs{current_epoch}.svg", format="svg")
            plt.close()
        else:
            print(f"{BColors.WARNING}Warning: 'plot' is not 'true' nor an existing path."
                  f"Displaying plot in window by default.{BColors.ENDC}")
            self.fig.show()

class test_model:
    """
    Class to train a Pytorch model.

    Parameters
    ----------
    model : LeModel object
        Pytorch model to train
    X_test : Pytorch tensor
        Tensor that will be used as input for testing.
    y_test : Pytorch tensort
        Tensor that will be used as reference output for testing.
    verbose : bool
        If True, prints test loss. Default is False.
    """
    def __init__(self, model, X_test, y_test, verbose=False):
        self.model = model.eval()
        self.X_test = X_test
        self.y_test = y_test
        self.verbose = verbose

        self.test()

    def test(self):
        self.y_pred = self.model(self.X_test)
        # self.loss = nn.MSELoss(reduction='mean')(self.y_pred, self.y_test)
        self.loss = nn.L1Loss(reduction='mean')(self.y_pred, self.y_test)
        if self.verbose:
            print(f"[TEST LOSS]: {self.loss.item():.3f}")

def data_augment_old(tensor, variation=0.01):
    """
    Introduces random variations on each values of a data array.
    
    Parameters
    ----------
    tensor : array-like
        Preferrably a PyTorch tensor of any shape.
    variation: float
        The maximum amount of variation for each value in tensor.

    Returns
    -------
    tensor + variations
    """
    warnings.warn(
        "This augmentation method is not suited for datasets with relations between "
        "each others. 'intelligent_data_augment' is recommended.",
        stacklevel=2
    )
    random_variations = np.random.uniform(
        low=-variation, high=variation, size=tensor.shape).astype(np.float32)
    
    return tensor+random_variations

def intelligent_data_augment(df, max_delay_error, pos_hydros, time_delay, celerity, n_to_augment=1):
    """
        A function to create "augment" data based on random varations of TDoAs.
        These new TDoAs are used to compute corresponding new values 
        for angles and derivatives.

        Parameters
        ----------
        df : pd.DataFrame
            Must contain columns for tdoas, angles and derivatives for letters A and B.
        max_delay_error : float
            Maximum error on tdoa in seconds (random uniform variations).
        pos_hydros : array-like
            Coordinates of hydrophones in 3D space. 
            Must be of shape (N hydrophones, 3).
        time_delay : constant or array-like
            Time between each A and B.
        celerity : int
            Celerity of sound underwater, in m.s-1.
        n_to_augment : int
            Maximal number of TDOAs per point that will be affected 
            by a random variation.

        Returns
        -------
        new_df : pandas DataFrame
            df with augmented values.
    """
    pos_hydros = np.array(pos_hydros)
    n_stations = pos_hydros.shape[0]

    variations_on_columns = [f"t_0{i}_A" for i in range(1,n_stations)] + [f"t_0{i}_B" for i in range(1,n_stations)]

    # select how many tdoas will have random variations
    n_variations_A = np.random.choice(np.arange(n_to_augment+1), size=len(df))
    n_variations_B = np.random.choice(np.arange(n_to_augment+1), size=len(df))

    # create a table with True for each tdoa that will be randomly changed
    change_A = np.full((len(df), n_stations-1), False)
    change_B = np.full((len(df), n_stations-1), False)
    for line in range(len(df)):
        to_change = np.random.permutation(np.arange(n_stations-1))[:n_variations_A[line]]
        change_A[line, to_change] = True

        to_change = np.random.permutation(np.arange(n_stations-1))[:n_variations_B[line]]
        change_B[line, to_change] = True

    # then merge this tables into one
    to_change = np.append(change_A, change_B, axis=1)

    # apply changes (True = random variation, False = variation of 0)
    random_variations = np.zeros(to_change.shape, dtype=float)
    random_variations[np.where(to_change)] = np.random.uniform(
        low=-max_delay_error, high=max_delay_error, 
        size=to_change[np.where(to_change)].shape)

    new_df = df.copy()
    new_df[variations_on_columns] = df[variations_on_columns]+random_variations

    for letter in ["A", "B"]:
        elevations = []
        azimuths = []
        for _, row in tqdm(new_df.iterrows(), total=len(new_df), desc="augmentation", leave=False):
            temp = tdoa_residuals_solve(
                delays_to_stations=row[[f"t_0{n}_{letter}" for n in range(1, n_stations)]].to_numpy(), 
                stations_coordinates=pos_hydros, 
                celerity=celerity, 
                dist_max=125, 
                results="dea"
            )
            elevations += [temp[1]]
            azimuths += [temp[2]]
        new_df[f"elevation_{letter}"], new_df[f"azimuth_{letter}"] = elevations, azimuths

    # recompute derivatives from tdoas with errors
    for n in range(1, n_stations):
        new_df[f"d_0{n}"] = (new_df[f"t_0{n}_B"]-new_df[f"t_0{n}_A"])/time_delay

    return new_df

def split_to_tensor_XY_scale(
        data,
        feats, 
        preds,
        scaler=None):
    """ 
    A function to split train test and validation data into X, y.
    Also applies a standard scaling to X data based on X_train and 
    Transforms data from pandas dataframes to pytorch tensors

    Parameters
    ----------
    data : pd.DataFrame
        Table holding data, should contain columns in feats.
    feats : list
        List of columns names that will constitute X_data. 
    preds : list, optional
        List of columns that are the features to be affected to y data. 
        By default ["distance_A", "distance_B"].

    Returns
    -------
    List of tuples of tensors : 
        X, y data in the form 
        [(X_train, y_train), (X_val, y_val), (X_test, y_test)]
    """
    X_data = data[feats].copy().to_numpy()

    # use standard scaler
    if not isinstance(scaler, StandardScaler):
        scaler = StandardScaler()
        scaler.fit(X_data)

    X_data = scaler.transform(X_data)
    y_data = data[preds].copy().to_numpy().mean(axis=1)

    return (
        torch.tensor(X_data, dtype=torch.float32), 
        torch.tensor(y_data.astype("float32"), dtype=torch.float32)[:, None],
        scaler)

def train_val_test_steps(train, val, test, img_path, epochs, learning_rate, val_names=[], end_lr_factor=1, early_stopping=-1, print_every=0, model_instance=NewMLP):
    """ 
    A function to train, validate and test a DL model

    Parameters
    ----------
    train : list
        Training data [X_train, y_train] both must be tensors and 
        have the same length.
    val : list
        Validation data [X_vals, y_vals] both must be tensors
        or lists of tensors and have the same length.
    test : list
        Test data [X_tests, y_tests] both must be tensors or 
        lists of tensors and have the same length.
    img_path : str
        Path to save the training graph (in .svg).
    epochs : int
        Number of epochs for training.
    learning_rate : float
        Learning rate of the model.
    val_names : list, optional
        List of names for validation sets to show on training graph.
        By default [], no specific names.
    end_lr_factor : int, optional
        By how much the learning rate should be multiplied 
        by the last epoch, by default 1.

    Returns
    -------
    model_best : Pytorch model instance
        Trained model.
    loss_train : list
        List of the training losses after each epoch.
    loss_val : list
        List of the validation losses after each epoch.
    test_loss : list
        Test loss on data after training.
    y_true : numpy array
        Test set data to predict.
    y_pred : numpy array
        Test set predicted data.
    """
    
    model = model_instance(train[0].shape[1], train[1].shape[1])
    device = find_device()
    model.to(device)

    if isinstance(val[0], list) and torch.is_tensor(val[0][0]):
        X_val = [tensor.to(device) for tensor in val[0]]
        y_val = [tensor.to(device) for tensor in val[1]]   
    else:
        X_val = val[0].to(device)
        y_val = val[1].to(device)
    training_step = train_model(
        model, 
        train[0].to(device), train[1].to(device),
        X_val=X_val, y_val=y_val,
        learning_rate=learning_rate,
        max_epochs=epochs,
        print_every=print_every,
        plot=img_path,
        labels=val_names,
        end_lr_factor=end_lr_factor,
        plot_every=-1,
        early_stopping_epochs=early_stopping,
        min_loss_diff=-1)
    model_best = training_step.best_model

    if isinstance(test[0], list) and torch.is_tensor(test[0][0]):
        X_test = [tensor.to(device) for tensor in test[0]]
        y_test = [tensor.to(device) for tensor in test[1]]  
    else:
        X_test = [test[0].to(device)]
        y_test = [test[1].to(device)]   

    test_loss, y_true, y_pred = [], [], []
    for X_use, y_use in zip(X_test, y_test):
        test_step = test_model(model_best, X_use, y_use)
        test_loss = test_loss + [test_step.loss.item()]
        y_true = y_true + [test_step.y_test.detach().cpu().numpy()]
        y_pred = y_pred + [test_step.y_pred.detach().cpu().numpy()]
    
    return (model_best, 
            training_step.loss_train,
            training_step.loss_val,
            test_loss,
            y_true,
            y_pred)
