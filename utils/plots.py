#%%## Importations
import os
import math
import librosa
import numpy as np
import pandas as pd
from PIL import Image
from tqdm import tqdm
import seaborn as sns
from datetime import datetime
from scipy.stats import binned_statistic_2d

import matplotlib
from matplotlib import colors, cm
import matplotlib.pyplot as plt
from matplotlib.path import Path
import matplotlib.animation as animation
from matplotlib.widgets import LassoSelector
from matplotlib.collections import LineCollection

from utils.utils import BColors
from utils.geometry import cartesian2spherical, spherical2cartesian, tdoa_residuals_solve


#%%## Definitions #####
def plot_function(df, x_name, y_name, z_name, path, xlab="", ylab="", zlab="", title="", nbins=25):
    """
    A function to plot data in 2D with colors.
    The function saves 2 plots : a scatter plot and a plot containing 
    the means of this scatter plot per bin.

    Parameters
    ----------
    df : pandas.DataFrame
        Contains data to be plotted.
    x_name : str
        The name of a column in df that will be plotted along the x-axis.
    y_name : str
        The name of a column in df that will be plotted along the y-axis.
    z_name : str
        The name of a column in df that will be plotted along the z-axis.
    path : str
        Path to save the plot as .png.
    xlab : str, optional
        label for x-axis, by default "".
    ylab : str, optional
        label for y-axis, by default "".
    zlab : str, optional
        label for z-axis, by default "".
    title : str, optional
        Title of the plot, by default "".
    nbins : int or tuple , optional
        if int, number of bins for both axes.
        if tuple, number of bin for x and y axes separately.
        By default 25.
    """
    if isinstance(nbins, int):
        x_bins = np.linspace(df[x_name].min(), df[x_name].max(), nbins)
        y_bins = np.linspace(df[y_name].min(), df[y_name].max(), nbins)
    elif isinstance(nbins, tuple):
        x_bins = np.linspace(df[x_name].min(), df[x_name].max(), nbins[0])
        y_bins = np.linspace(df[y_name].min(), df[y_name].max(), nbins[1])     
    if z_name != "":
        zaxis = df[z_name]
    else:
        zaxis = np.zeros(len(df))

    ret = binned_statistic_2d(
        df[x_name].to_numpy(), df[y_name].to_numpy(), zaxis, 
        statistic=np.mean, 
        bins=[x_bins, y_bins])

    fig, axs = plt.subplots(1, 2, figsize=(6*2,4.8))
    sct = axs[0].scatter(
        df[x_name], df[y_name], 
        c=zaxis,
        cmap="viridis")
    axs[0].set_xlabel(xlab)
    axs[0].set_ylabel(ylab)
    if z_name != "":
        plt.colorbar(sct, label=zlab, ax=axs[0])
    img = axs[1].imshow(
        ret.statistic.T, 
        aspect='auto', origin='lower',
        extent=(df[x_name].min(), 
                df[x_name].max(), 
                df[y_name].min(), 
                df[y_name].max()),
        cmap="plasma")
    axs[1].set_xlabel(xlab)
    axs[1].set_ylabel(ylab)
    if z_name != "":
        plt.colorbar(img, label="Mean "+zlab.lower(), ax=axs[1])
    fig.suptitle(title)
    fig.tight_layout()
    fig.savefig(path, format="png", dpi=300)
    plt.close('all')


def plot_hist_function(df, x_name, y_name, z_name, path, xlab="", ylab="", zlab="", title="", nbins=25):
    """A function to plot data in 3D.
    The function saves 1 plots : a 3d histogram with means of z_name 
    per each bin of x_name, y_name.

    Parameters
    ----------
    df : pandas.DataFrame
        Contains data to be plotted.
    x_name : str
        The name of a column in df that will be plotted along the x-axis.
    y_name : str
        The name of a column in df that will be plotted along the y-axis.
    z_name : str
        The name of a column in df that will be plotted along the z-axis.
    path : str
        Path to save the plot as .png.
    xlab : str, optional
        label for x-axis, by default "".
    ylab : str, optional
        label for y-axis, by default "".
    zlab : str, optional
        label for z-axis, by default "".
    title : str, optional
        Title of the plot, by default "".
    nbins : int or tuple , optional
        if int, number of bins for both axes.
        if tuple, number of bin for x and y axes separately.
        By default 25.
    """
    if isinstance(nbins, int):
        x_bins = np.linspace(df[x_name].min(), df[x_name].max(), nbins)
        y_bins = np.linspace(df[y_name].min(), df[y_name].max(), nbins)
    elif isinstance(nbins, tuple):
        x_bins = np.linspace(df[x_name].min(), df[x_name].max(), nbins[0])
        y_bins = np.linspace(df[y_name].min(), df[y_name].max(), nbins[1])     

    dx = (x_bins[-1]-x_bins[0])/len(x_bins)
    dy = (y_bins[-1]-y_bins[0])/len(y_bins)

    ret = binned_statistic_2d(
        df[x_name].to_numpy(), df[y_name].to_numpy(),  df[z_name], 
        statistic=np.mean, 
        bins=[x_bins, y_bins])
    
    xpos, ypos = np.meshgrid(
        ret.x_edge[:-1]-dx/2, ret.y_edge[:-1]-dy/2, indexing="ij")
    xpos = xpos.ravel()
    ypos = ypos.ravel()
    zpos = 0

    dz = ret.statistic.ravel()
    dz = np.nan_to_num(dz)

    fig = plt.figure(figsize=(6*2,4.8))
    
    ax0 = fig.add_subplot(1, 2, 1, projection='3d')
    ax0.bar3d(xpos, ypos, zpos, dx, dy, dz, zsort='average')
    ax0.set_xlabel(xlab)
    ax0.set_ylabel(ylab)

    ax1 = fig.add_subplot(1, 2, 2)
    img = ax1.imshow(
        ret.statistic.T, 
        aspect='auto', origin='lower',
        extent=(df[x_name].min()-dx/2, 
                df[x_name].max()-dx/2, 
                df[y_name].min()-dy/2, 
                df[y_name].max()-dy/2),
        cmap="plasma")
    ax1.set_xlabel(xlab)
    ax1.set_ylabel(ylab)
    plt.colorbar(img, label="Mean "+zlab.lower(), ax=ax1)

    fig.suptitle(title)
    fig.tight_layout()
    fig.savefig(path, format="png", dpi=300)
    plt.close('all')


def plot_confusion(df, x_name, y_name, hue_name, path='none', xlab="", ylab="", zlab="", title="", nbins=None, text="none"):
    """A function to plot data in 3D.
    The function saves 1 plots : a 3d histogram with means of z_name 
    per each bin of x_name, y_name.

    Parameters
    ----------
    df : pandas.DataFrame
        Contains data to be plotted.
    x_name : str
        The name of a column in df that will be plotted along the x-axis.
    y_name : str
        The name of a column in df that will be plotted along the y-axis.
    hue_name : str
        The name of a column in df that will be plotted along the z-axis.
    path : str, optionnal
        Path to save the plot as .png. If 'none' then the plot is diplayed.
        Defaults to 'none'.
    xlab : str, optional
        label for x-axis, by default "".
    ylab : str, optional
        label for y-axis, by default "".
    zlab : str, optional
        label for z-axis, by default "".
    title : str, optional
        Title of the plot, by default "".
    nbins : int or tuple , optional
        if int, number of bins for both axes.
        if tuple, number of bin for x and y axes separately.
        By default 25.
    text : str
        If 'none' then no text is shown on plot.
        If 'MAE' the global MAE for data x VS y data is shown.
        If 'Error' the mean of y-axis is shown.
    """
    if text not in ['none', 'MAE', 'Error']:
        raise TypeError(
            f"text value '{text}' unknown. Should be one of ['none', 'MAE', 'Error'].")

    if not (isinstance(nbins, int) or isinstance(nbins, tuple)):
        dist_plot = sns.jointplot(
            data=df, hue=hue_name,
            x=x_name, y=y_name, s=5)

        dist_plot.fig.suptitle(title)
        dist_plot.fig.axes[0].set_xlabel(xlab)
        dist_plot.fig.axes[0].set_ylabel(ylab)
        if text=="MAE":
            dist_plot.fig.axes[0].text(
                min(df[x_name]), max(df[y_name]),
                f"Global MAE: {np.round(np.mean(np.abs(df[x_name]-df[y_name])), 3)}")
        elif text=="Error":
            dist_plot.fig.axes[0].text(
                min(df[x_name]), max(df[y_name]),
                f"Global error: {np.round(np.mean(df[y_name]), 3)}")
        dist_plot.fig.tight_layout()
        if path == 'none':
            dist_plot.fig.show()
        else:
            dist_plot.fig.savefig(path, format="png", dpi=300)
    
    else:
        uniques_hues = df[hue_name].unique()

        for i, hue in enumerate(uniques_hues):
            subdf = df.where(df[hue_name] == hue).dropna(how="all").copy()
            dist_plot = sns.jointplot(
                data=subdf, kind="hist",
                x=x_name, y=y_name, bins=nbins, common_bins=False,
                cbar=True, cbar_kws={"label": "number of points"})
            dist_plot.fig.suptitle(title + f" (with '{hue}').")
            dist_plot.fig.axes[0].set_xlabel(xlab)
            dist_plot.fig.axes[0].set_ylabel(ylab)
            if text=="MAE":
                dist_plot.fig.axes[0].text(
                    min(df[x_name]), max(df[y_name]),
                    f"Global MAE: {np.round(np.mean(np.abs(df[x_name]-df[y_name])), 3)}")
            elif text=="Error":
                dist_plot.fig.axes[0].text(
                    min(df[x_name]), max(df[y_name]),
                    f"Global error: {np.round(np.mean(df[y_name]), 3)}")
            dist_plot.fig.tight_layout()
            if path == 'none':
                dist_plot.fig.show()
            else:
                dist_plot.fig.savefig(path[:-4] + f'_{hue}' + ".png" , format="png", dpi=300)
    plt.close()


def plot_dataset_overview(df, path='none'):
    """
        A function to verify the distributions in a dataset.
        df must come from "create-dataset.py".

        Parameters
        ----------
        df : pandas.DataFrame
            The dataset used to make plots.
        path : str, optional
            The path to a folder where the plots will be saved.
            To show the plots instead of saving them, use path='none'.
            By default 'none'.
    """
    if path == "none":
        save = False
    else:
        save = True

    # Distances intra-couple
    if not np.all(np.isnan(df[["x_A","x_B","y_A","y_B","z_A","z_B"]])):
        distances = np.linalg.norm(
            df[["x_A", "y_A", "z_A"]].to_numpy()-df[["x_B", "y_B", "z_B"]].to_numpy(), 
            axis=1)
    else:
        distances = np.abs(df["distance_A"]-df["distance_B"]).to_numpy()
    # distances A-B
    fig, ax = plt.subplots(1, 1, sharex=True, sharey=True, figsize=(6.4, 4.8))
    ax.hist(distances, bins=25)
    ax.set_title(f"Distance between A & B (in m)")
    if save:
        fig.savefig(os.path.join(path, "AB-distances_distrib.png"), dpi=500)
    else:
        plt.show(block=False)

    # tdoas
    fig, axs = plt.subplots(1, 3, sharex=True, sharey=True, figsize=(15.36, 4.8))
    for i in range(3):
        axs[i].hist(df[f"d_0{i+1}"], bins=100)
        axs[i].set_title(f"Derivative of delay 0{i+1} between point A and B")
    if save:
        fig.savefig(os.path.join(path, "AB-derivatives_distrib.png"), dpi=500)
    else:
        plt.show(block=False)

    for letter in ["A", "B"]:
        # tdoas
        fig, axs = plt.subplots(1, 3, sharex=True, sharey=True, figsize=(15.36, 4.8))
        for i in range(3):
            axs[i].hist(df[f"t_0{i+1}_{letter}"], bins=100)
            axs[i].set_title(f"TDOA 0{i+1} for {letter} points (in sec)")
        if save:
            fig.savefig(os.path.join(path, f"{letter}-TDOA_distrib.png"), dpi=500)
        else:
            plt.show(block=False)

        # x y z
        if not np.all(np.isnan(df[["x_A","x_B","y_A","y_B","z_A","z_B"]])):
            fig, axs = plt.subplots(1, 3, figsize=(15.36, 4.8))
            for i, coord in enumerate(["x", "y", "z"]):
                axs[i].hist(df[f"{coord}_{letter}"], bins=100)
                axs[i].set_title(f"{coord}_{letter} (in m)")
            if save:
                fig.savefig(os.path.join(path, f"{letter}-coord-xyz_distrib.png"), dpi=500)
            else:
                plt.show(block=False)

        # d e a
        fig, axs = plt.subplots(1, 3, figsize=(15.36, 4.8))
        for i, coord in enumerate(["distance", "elevation", "azimuth"]):
            axs[i].hist(df[f"{coord}_{letter}"], bins=100)
            axs[i].set_title(f"{coord}_{letter} {('(in m)' if (coord=='distance') else '(in rad)')}")
        if save:
            fig.savefig(os.path.join(path, f"{letter}-coord-dea_distrib.png"), dpi=500)
        else:
            plt.show(block=True) # last plot, show all plots once this one is ready.


def barplot_huge(df, x_name, cat_name, plot_name, path_save, add2title=""):
    """
    Saves a huge figure constituted of 6 barplots :
        - N plots of "loss" in df
        - N plots of "proportion_loss" in df
    
    N plots corresponding to the number of categories in plot_name column.
    Each plots contain K sets of bars corresponding to the number of categories in x_name column
    Each sets of bars contains L bars, corresponding to the number of categories in cat_name column
    
    df must contain the columns "loss", "loss_std", "loss_proportion", "loss_proportion_std".

    /!\ Titles for axis and plots cannot be changed for now.
    """
    names_x = df[x_name].unique()
    n_x = len(names_x)
    names_cat = df[cat_name].unique()
    n_cat = len(names_cat)    
    names_plt = df[plot_name].unique()
    n_plt = len(names_plt)

    barwidth = 1/(n_cat+1)

    x = [np.arange(n_x)+i*barwidth for i in range(n_cat)]

    fig, axs = plt.subplots(
        3, 3, 
        figsize=(21, 12), 
        sharey='row',
        gridspec_kw={'height_ratios': [1, 0.5, 1]})

    max_row_1 = 125
    for i, plot in enumerate(names_plt):
        for col, cat in enumerate(names_cat):
            # get corresponding data in df
            bars = [[], []]
            errors = [[], []]

            for xelement in names_x:
                subdf = df.where(
                    (df[plot_name] == plot) &
                    (df[cat_name] == cat) &
                    (df[x_name] == xelement)
                ).dropna(how="all").copy()

                if len(subdf) > 0:
                    bars[0] += [subdf.loss.values[0]]
                    errors[0] += [subdf.loss_std.values[0]]
                    bars[1] += [subdf.loss_proportion.values[0]]
                    errors[1] += [subdf.loss_proportion_std.values[0]]
                else:
                    bars[0] += [0]
                    errors[0] += [0]
                    bars[1] += [0]
                    errors[1] += [0]       

            # do plots
            axs[0,i].bar(
                x[col], 
                bars[0],
                yerr=errors[0],
                width=barwidth,
                edgecolor='black',
                label=cat)
            axs[0,i].legend(title=cat_name, loc="upper left")
            axs[0,i].set_xticks(x[int(n_x/2)], names_x)
            if i==0:
                axs[0,i].set_ylabel("Loss (in m)")
            axs[0,i].set_xlabel("Dataset used to train model")
            axs[0,i].set_yscale("linear")
            axs[0,i].set_title(f"Prediction loss (in m) when training model {plot} " + add2title)

            # proportion_loss plots
            axs[2,i].bar(
                x[col], 
                bars[1],
                yerr=errors[1], 
                width=barwidth,
                edgecolor='black', 
                label=cat)
            axs[2,i].set_xticks(x[int(n_x/2)], names_x)
            if i==0:
                axs[2,i].set_ylabel("Loss (in %)")
            axs[2,i].set_xlabel("Dataset used to train model.")
            axs[2,i].set_ylim(0, 120)
            axs[2,i].spines['top'].set_visible(False)

            # proportion_loss plots (outside boundaries)
            axs[1,i].bar(
                x[col], 
                bars[1],
                yerr=errors[1], 
                width=barwidth,
                edgecolor='black', 
                label=cat)
            axs[1,i].set_title(f"Prediction loss (in %) when training model {plot} " + add2title)
            axs[1,i].legend(title=cat_name, loc="upper left")
            if np.max(np.array(bars[1])+np.array(errors[1])) > max_row_1:
                max_row_1 = np.max(np.array(bars[1])+np.array(errors[1]))
            axs[1,i].set_ylim(120, max_row_1)
            axs[1,i].spines['bottom'].set_visible(False)
            axs[1,i].tick_params(axis='x', labelbottom='off', bottom=False)
            axs[1,i].xaxis.tick_bottom()
            axs[1,i].set_xticks([])  
            
            # add diagonal lines
            d = .01  
            kwargs = dict(transform=axs[1,i].transAxes, color='k', clip_on=False)
            axs[1,i].plot((-d, +d), (-d, +d), **kwargs)     
            axs[1,i].plot((1 - d, 1 + d), (-d, +d), **kwargs) 
            kwargs.update(transform=axs[2,i].transAxes)  
            axs[2,i].plot((-d, +d), (1 - d, 1 + d), **kwargs)  
            axs[2,i].plot((1 - d, 1 + d), (1 - d, 1 + d), **kwargs)  


    fig.tight_layout()
    fig.savefig(path_save, format="svg")
    plt.close('all')


def verify_conversions(df, hydrophones_positions, celerity, path='none', speedup=10):
    """
        A function to verify conversions between coordinates in a dataset.
        df must come from "create-dataset.py".

        Parameters
        ----------
        df : pandas.DataFrame
            The dataset used to make plots.
        hydrophone_positions : array-like
            Positions of 4-hydrophones in 3D with shape (4, 3)
        celerity : int
            Speed of sound underwater (in m.s-1).
        path : str, optional
            The path to a folder where the plots will be saved.
            To show the plots instead of saving them, use path='none'.
            By default 'none'.
    """
    if path=='none':
        use_BColors = True
    else:
        use_BColors = False
    
    n_stations = hydrophones_positions.shape[0]
    df = df.sample(frac=1/speedup, ignore_index=True)

    lines = f"\nCenter of hydro array: {np.sum(hydrophones_positions, axis=0)}"

    if np.allclose(np.sum(hydrophones_positions, axis=0), [0,0,0]):
        lines += f"\n\t{BColors.OKGREEN if use_BColors else ''}Close to [0, 0, 0].{BColors.ENDC if use_BColors else ''}"
    else:
        lines += f"\n\t{BColors.FAIL if use_BColors else ''}Far from [0, 0, 0].{BColors.ENDC if use_BColors else ''}"
    
    # compute xyz from dist elevation azimuth
    lines += f"\n\nCoordinates conversions:"
    for letter in ["A", "B"]:
        xyz_true = df[[f"x_{letter}", f"y_{letter}", f"z_{letter}"]].copy().to_numpy()
        xyz_from_dea = spherical2cartesian(
            df[f"distance_{letter}"], 
            df[f"elevation_{letter}"], 
            df[f"azimuth_{letter}"])

        if np.allclose(xyz_true, xyz_from_dea):
            lines += f"\n\t{BColors.OKGREEN if use_BColors else ''}Valid conversion dea -> xyz ({letter}'s).{BColors.ENDC if use_BColors else ''}"
        else:
            lines += f"\n\t{BColors.FAIL if use_BColors else ''}Invalid conversion dea -> xyz ({letter}'s).{BColors.ENDC if use_BColors else ''}"
    
    # compute dist elevation azimuth from xyz
    for letter in ["A", "B"]:
        dea_true = df[[f"distance_{letter}", f"elevation_{letter}", f"azimuth_{letter}"]].copy().to_numpy()
        dea_from_xyz = cartesian2spherical(
            df[f"x_{letter}"], 
            df[f"y_{letter}"], 
            df[f"z_{letter}"])

        if np.allclose(dea_true, dea_from_xyz):
            lines += f"\n\t{BColors.OKGREEN if use_BColors else ''}Valid conversion xyz -> dea ({letter}'s).{BColors.ENDC if use_BColors else ''}"
        else:
            lines += f"\n\t{BColors.FAIL if use_BColors else ''}Invalid conversion xyz -> dea ({letter}'s).{BColors.ENDC if use_BColors else ''}"

    # compute xyz from tdoas
    lines += "\n\nTDoAs conversions:"
    for letter in ["A", "B"]:

        xyz_solo = df[[f"x_{letter}", f"y_{letter}", f"z_{letter}"]].copy().to_numpy()
        tdoa_for_xyz = df[[f"t_0{n}_{letter}" for n in range(1, n_stations)]].copy().to_numpy().astype(float)

        xyz_from_tdoa = []
        for tdoa in tqdm(tdoa_for_xyz, leave=False, position=2, desc=f"{letter}: tdoa -> xyz"):
            res = tdoa_residuals_solve(
                tdoa, 
                hydrophones_positions, 
                celerity,
                dist_max=int(path.split("-")[-1][:-1]))
            # Make a more educated guess
            new_res = tdoa_residuals_solve(
                tdoa, 
                hydrophones_positions, 
                celerity,
                init=res/np.linalg.norm(res), 
                dist_max=int(path.split("-")[-1][:-1]))
            #HACK : I should check if the new result is better than the old one
            xyz_from_tdoa = xyz_from_tdoa + [new_res]
        xyz_from_tdoa = np.array(xyz_from_tdoa)

        if np.allclose(xyz_solo, xyz_from_tdoa):
            lines += f"\n\t{BColors.OKGREEN if use_BColors else ''}Valid conversion tdoa -> xyz ({letter}'s).{BColors.ENDC if use_BColors else ''}"
        else:
            lines += f"\n\t{BColors.FAIL if use_BColors else ''}Invalid conversion tdoa -> xyz ({letter}'s).{BColors.ENDC if use_BColors else ''}"
            # try to see how many coordinates are close at least.
            hue = np.isclose(np.linalg.norm(xyz_solo, axis=1), np.linalg.norm(xyz_from_tdoa, axis=1), atol=2.5)
            hue = ["good estimation" if boolean else "bad estimation"
                    for boolean in hue]
            error = (np.linalg.norm(xyz_solo, axis=1) - np.linalg.norm(xyz_from_tdoa, axis=1))

            # HACK: adding min and max values for each hue just to have the same plot scale
            huedata = np.append(hue, ["good estimation", "good estimation", "bad estimation", "bad estimation"])
            xdata = np.linalg.norm(xyz_solo, axis=1)
            xdata = np.append(xdata, [min(xdata), max(xdata), min(xdata), max(xdata)])
            ydata = np.linalg.norm(xyz_from_tdoa, axis=1)
            ydata = np.append(ydata, [min(ydata), max(ydata), min(ydata), max(ydata)])
            errdata = np.abs(error)
            errdata = np.append(errdata, [min(errdata), max(errdata), min(errdata), max(errdata)])
            plot_data = pd.DataFrame({
                "x": xdata,
                "y": ydata,
                "errors": errdata,
                "Bins": huedata
            })
            plot_data.sort_values(by="Bins", inplace=True, ignore_index=True)

            plot1 = sns.jointplot(
                data=plot_data,
                x="x", y="y", hue="Bins", 
                kind="hist", bins=50, common_bins=False,
                linewidth=0, marginal_kws=dict(bins=50), 
                hue_order=["bad estimation", "good estimation"])
            plot1.fig.set_size_inches((10,10))
            plot1.fig.axes[0].set_xlabel("True distance (in m)")
            plot1.fig.axes[0].set_ylabel("Predicted distance (in m)")
            if path=='none':
                plot1.fig.show(block=False)
            else:
                plot1.fig.savefig(
                    os.path.join(path, f"recomputed_confusion_{letter}.png"), 
                    dpi=500)
                plt.close()

            plot2 = sns.jointplot(
                data=plot_data,
                x="x", y="errors", hue="Bins",
                kind="hist", bins=50, common_bins=False,
                linewidth=0, marginal_kws=dict(bins=50),
                hue_order=["bad estimation", "good estimation"])
            plot2.fig.set_size_inches((10,10))
            plot2.fig.axes[0].set_xlabel("True distance (in m)")
            plot2.fig.axes[0].set_ylabel("Loss (in m)")
            if path=='none':
                plot2.fig.show(block=True)
            else:
                plot2.fig.savefig(
                    os.path.join(path, f"recomputed_error_{letter}.png"), 
                    dpi=500)
                plt.close()
            
        df[f"recomputed_distance_{letter}"] = np.linalg.norm(xyz_from_tdoa, axis=1)
            
    # also if path given, save recomputation
    if path != "none":
        df[["distance_A", "distance_B", "recomputed_distance_A", "recomputed_distance_B"]
            ].to_csv(os.path.join(path, "recomputed_distances.csv"), index=False)

    # compute tdoas from xyz
    for letter in ["A", "B"]:
        tdoa_solo = df[[f"t_0{n}_{letter}" for n in range(1, n_stations)]].copy().to_numpy().astype(float)
        
        xyz_for_tdoa = df[[f"x_{letter}", f"y_{letter}", f"z_{letter}"]].copy().to_numpy()
        tdoa_from_xyz = []

        for xyz_for_tdoa_i in xyz_for_tdoa:
            times = [
                math.dist(hydrophones_positions[i], xyz_for_tdoa_i)/celerity
                for i in range(n_stations)]

            tdoa_from_xyz = tdoa_from_xyz + [[time-times[0] for time in times[1:]]]
        tdoa_from_xyz = np.array(tdoa_from_xyz)

        if np.allclose(tdoa_solo, tdoa_from_xyz):
            lines += f"\n\t{BColors.OKGREEN if use_BColors else ''}Valid conversion xyz -> tdoa ({letter}'s).{BColors.ENDC if use_BColors else ''}"
        else:
            lines += f"\n\t{BColors.FAIL if use_BColors else ''}Invalid conversion xyz -> tdoa ({letter}'s).{BColors.ENDC if use_BColors else ''}"

    if path=='none':
        print(lines)
    else:
        with open(os.path.join(path, "logs.txt"), "w") as f:
            f.writelines(lines)


def gif2frames(path2gif):
    "Saves each gif's frame to a .pdf file"

    output_folder = path2gif[:-4] + "_frames"
    os.makedirs(output_folder, exist_ok=True)

    with Image.open(path2gif) as im:
        for i in range(im.n_frames):
            im.seek(i)
            im.save(os.path.join(output_folder, f"frame-{i}.png"))


class SelectFromCollection:
    """
    From https://matplotlib.org/stable/gallery/widgets/lasso_selector_demo_sgskip.html

    Select indices from a matplotlib collection using `LassoSelector`.

    Selected indices are saved in the `ind` attribute. This tool fades out the
    points that are not part of the selection (i.e., reduces their alpha
    values). If your collection has alpha < 1, this tool will permanently
    alter the alpha values.

    Note that this tool selects collection objects based on their *origins*
    (i.e., `offsets`).

    Parameters
    ----------
    ax : `~matplotlib.axes.Axes`
        Axes to interact with.
    collection : `matplotlib.collections.Collection` subclass
        Collection you want to select from.

    Added functions
    ---------------
    Text box indicating number of groups VS points remaining for selection
    Each group is colored from a 10 color

    Added reset function ('shift' + 'r')
    Added cancel last selection function ('z')
    """
    def __init__(self, ax, collection):

        self.ax = ax
        self.canvas = self.ax.figure.canvas
        self.collection = collection
        self.default_color = np.array(colors.to_rgba('black'))
        self.list_colors = [
        np.array(colors.hex2color(col)) 
            for col in ['#1f77b4', '#ff7f0e', '#2ca02c', '#d62728', '#9467bd', '#8c564b', '#e377c2', '#7f7f7f', '#bcbd22', '#17becf']]
        self.xys = collection.get_offsets()
        self.Npts = len(self.xys)

        # Ensure that we have separate colors for each object
        self.fc = collection.get_facecolors()
        if len(self.fc) == 0:
            raise ValueError('Collection must have a facecolor')
        elif len(self.fc) == 1:
            self.fc = np.tile(self.fc, (self.Npts, 1))
        self.fc[:, :-1] = self.default_color[:-1]
        self.collection.set_facecolors(self.fc)
        self.last_fc = np.copy(self.fc)
        self.lasso = LassoSelector(self.ax, onselect=self.onselect)
        self.ind = []

        # Ensure we have labels
        self.labels = np.zeros(len(self.fc))
        self.prev_labels = np.zeros(len(self.fc))
        self.count_labels = 0
        self.canvas.mpl_connect("key_press_event", self.key_parser)
        self.ax.set_title(
            "Press 'Shift+R' to reset selection"
            "\nPress 'Z' to cancel last selection")
        self.authorize_cancel = False

        # Display useful informations
        self.textbox = self.ax.text(
            0.05, 0.95, 
            (f"{np.count_nonzero(self.labels)} points selected"
                f"\n{np.count_nonzero(self.labels==0)} points not selected"), 
            transform=self.ax.transAxes, 
            fontsize=12,
            verticalalignment='top', 
            bbox=dict(
                boxstyle='round', 
                facecolor='white', 
                alpha=0.5))
        self.canvas.draw_idle()

    def onselect(self, verts):
        self.prev_labels = np.copy(self.labels)
        self.last_fc = np.copy(self.fc)
        path = Path(verts)
        self.ind = np.nonzero(path.contains_points(self.xys))[0]
        self.count_labels += 1
        self.labels[self.ind] = self.count_labels
        self.textbox.set_text(f"{np.count_nonzero(self.labels)} points selected"
                f"\n{np.count_nonzero(self.labels==0)} points not selected")
        self.fc[self.ind, :-1] = self.list_colors[int(self.count_labels)%len(self.list_colors)]
        self.collection.set_facecolors(self.fc)
        self.canvas.draw_idle()
        self.authorize_cancel = True

    def key_parser(self, event):
        if event.key == "R":
            self.reset()
        elif event.key == "z":
            self.cancel_last()

    # new event to reset plot
    def reset(self):
        # reset all variables
        self.count_labels = 0
        self.labels = np.zeros(len(self.fc))
        self.textbox.set_text(f"{np.count_nonzero(self.labels)} points selected"
                f"\n{np.count_nonzero(self.labels==0)} points not selected")
        self.fc[:, :-1] = self.default_color[:-1]
        self.collection.set_facecolors(self.fc)
        self.canvas.draw_idle()
        self.authorize_cancel = False

    # cancel last action
    def cancel_last(self):
        if self.authorize_cancel:
            self.labels = np.copy(self.prev_labels)
            self.fc = np.copy(self.last_fc)
            self.count_labels -= 1
            self.textbox.set_text(f"{np.count_nonzero(self.labels)} points selected"
                f"\n{np.count_nonzero(self.labels==0)} points not selected")
            self.collection.set_facecolors(self.fc)
            self.canvas.draw_idle()
            self.authorize_cancel = False


class TransformData:
    """
    A very complicated object for a simple action:
        By pressing the arrows on the keyboard while plot is in focus
        data from df will be moved in time or in angle introducing a bias.

        Data plotted is azimuth in time, so if a bias is introduced on angle,
        then it will cycle in [-180°, 180°] space.

        After closing, time bias or angle bias can be accessed with attributes
        'time_bias' and 'angle_bias_deg'.
 

    Parameters
    ----------
    df : pandas.dataframe
        Data to be plotted, 
        must contain columns : ["sequence_time", "raw_azimuth"]
        "raw_azimuth" should be in radians 
        (it will be converted to degrees in plot).
    gps_data_path: string
        path to gps data to which df data will be fitted,
        must contain columns : ["time", "fwd_azimuth"]
        "fwd_azimuth" should be in radians 
        (it will be converted to degrees in plot).

    Methods
    -------
    key_pressed(event):
        When a key is pressed while plot is in focus,
        update time_bias or angle_bias_deg accordingly.
    update_plot():
        Update plot based on time_bias or angle_bias_deg.
    """
    def __init__(self, df, gps_data_path):
        print(f"{BColors.WARNING}WARNING: fwd_azimuth is reversed by default{BColors.ENDC}")

        # define variables
        self.df = df.sort_values(by="sequence_time")
        self.angle_bias_deg = 0
        self.time_bias = 0
        self.clipping_value = -180

        # import gps data
        self.df_gps = pd.read_csv(gps_data_path, parse_dates=["time"])
        self.df_gps['time'] = self.df_gps['time'].dt.tz_localize(None)
        self.df_gps["time"] += pd.to_timedelta(2, unit='h')        # UTC+2 in france

        # keep only gps corresponding to this sequence
        self.df_gps = self.df_gps.where(
            self.df_gps["time"] >= (
                df["sequence_time"].min() 
                - pd.to_timedelta(300, unit='s'))
            ).dropna()
        self.df_gps = self.df_gps.where(
            self.df_gps["time"] <= (
                df["sequence_time"].max() 
                + pd.to_timedelta(300, unit='s'))
            ).dropna()

        # compute angles
        from utils.geometry import angle_modulo
        self.azimuth_tdoa = angle_modulo(
            np.degrees(self.df["raw_azimuth"].copy()),
            mode="deg")
        self.timings_tdoa = self.df["sequence_time"].copy()
        self.azimuth_gps = -self.df_gps["fwd_azimuth"].copy()
        self.timings_gps = self.df_gps["time"].copy()
        
        # make plot
        self.fig, self.ax = plt.subplots(1,1)
        self.tdoa_plot, = self.ax.plot(
            self.timings_tdoa, self.azimuth_tdoa, 
            '.', markersize=5, label="TDOA", color="tab:blue")
        self.gps_plot, = self.ax.plot(
            self.timings_gps, self.azimuth_gps, 
            label="GPS", color="tab:orange")

        self.ax.legend(loc="lower left")
        self.ax.set_title(
            f"Panning by {self.angle_bias_deg}° and {self.time_bias}"
            f" sec\nClipping at {self.clipping_value} dB")
        self.ax.set_ylim(-180-10,180+10)
        self.ax.set_xlim(
            self.timings_tdoa.min()-pd.to_timedelta(30, unit='s'), 
            self.timings_tdoa.max()+pd.to_timedelta(30, unit='s'))
        
        # add events
        self.fig.canvas.mpl_connect("key_press_event", self.key_pressed)

        # initialise updates 
        self.update_plot()
        plt.show()

    def key_pressed(self, event):
        change = False
        # control bias by moving delay part 
        if "up" in event.key:
                change = True
                if event.key == "ctrl+up":
                    self.angle_bias_deg += 10
                elif event.key == "up":
                    self.angle_bias_deg += 1
        elif "down" in event.key:
                change = True
                if event.key == "ctrl+down":
                    self.angle_bias_deg -= 10
                elif event.key == "down":
                    self.angle_bias_deg -= 1
        elif "right" in event.key:
                change = True
                if event.key == "ctrl+right":
                    self.time_bias += 10
                elif event.key == "right":
                    self.time_bias += 1
        elif "left" in event.key:
                change = True
                if event.key == "ctrl+left":
                    self.time_bias -= 10
                elif event.key == "left":
                    self.time_bias -= 1
        # control clipping values
        if event.key == '+':
            change = True
            self.clipping_value += 10 
        elif event.key == '-':
            change = True
            self.clipping_value -= 10 
        elif "X" in event.key:
            self.angle_bias_deg = None
            self.time_bias = None
            plt.close()

        if change:
            self.update_plot()

    def update_plot(self):
        from utils.geometry import angle_modulo
        # update starting point
        subdf = self.df.where(self.df.db > self.clipping_value).dropna()
        self.azimuth_tdoa = angle_modulo(
            np.degrees(subdf["raw_azimuth"].copy()) + self.angle_bias_deg,
            mode="deg")
        self.timings_tdoa = subdf["sequence_time"].copy()

        # update plot
        self.tdoa_plot.set_data(
            self.timings_tdoa+pd.to_timedelta(self.time_bias, unit='s'), 
            self.azimuth_tdoa)
        self.ax.set_title(f"Panning by {self.angle_bias_deg}°"
            f" and {self.time_bias} sec\nClipping at {self.clipping_value} dB")
        self.fig.canvas.draw()


class Animator:
    """
        Parameters
        ----------
        time : array-like
            Time elapsed in seconds. Timestamps must be regularly spaced.
            e.g. every 1 sec, every 0.5 sec etc...
        xdata, ydata : array-like
            Coordinates of data to display on x-axis and y-axis.
            These array-like must be of shape (len(time)) or (len(time), N)
            if there are N trajectories to be plotted.
        ax : pyplot object
            Axes of the figure to plot. 
            This object only plots the trajectories, othe parametrisation must be
            done prior to giving ax.
        speed : float, optionnal
            By how much the display should be sped up.
            Should be position, default is 1.
        name_cmap : str, optionnal
            Name of a qualitative cmap of pyplot. 
            Will be used to color each trajectory.        
        decay : float, optionnal
            Time that a trajectory stays visible.
            The trajectory fades away until 'decay' and then disappears.
            Default is -1 (no decay).
        save : str, optionnal
            Path to a .gif file where the animation will be saved.
            Default is 'false' (not saving).

        Methods
        -------
        pause: event key = spacebar
            pretty self explenatory, pauses the animation.

        Returns
        -------
        None: 
            plots an animation which can be paused by pressing
            the spacebar.
    """

    def __init__(self, time, xdata, ydata, ax, speed=1, name_cmap="Accent", decay=1, save='false'):
        # set variables
        self.time = time
        self.xdata = xdata
        self.ydata = ydata
        self.ax = ax
        self.speed = speed
        self.decay = decay
        self.paused = False
        self.multiple_trajs = (len(np.array(xdata).shape) == 2) and (np.array(xdata).shape[1] > 1)
        self.interval = time[1] - time[0]
        self.time_step = 1000*self.interval/(self.speed)
        self.decay_list = np.linspace(0, 1, num=round(self.decay/self.interval))

        # extract colors from cmap
        cmap = cm.get_cmap(name_cmap)
        self.colors = []
        for i in range(cmap.N):
            self.colors = self.colors + [(cmap(i))]

        # setup plots
        if self.multiple_trajs:
            self.lines_collection = []
            self.points = []
            for idx in range(np.array(xdata).shape[1]):
                # leading point
                self.points = self.points + [
                    self.ax.scatter(
                        [], [], 
                        s=25, zorder=1,
                        color=self.colors[idx%len(self.colors)])]
                # trajectories fading out
                seg_points = np.array([self.xdata[:,idx], self.ydata[:,idx]]).T.reshape(-1, 1, 2)
                segments = np.concatenate([seg_points[:-1], seg_points[1:]], axis=1)
                lc = LineCollection(segments, colors=np.zeros((len(time)-1,4)), label=f"Line_{idx}")
                self.lines_collection = self.lines_collection + [ax.add_collection(lc)]
            self.decay_colors = [
                np.tile(self.colors[traj%len(self.colors)], (len(self.time)-1,1))
                for traj in range(self.xdata.shape[1])]

        else:
            # leading point
            self.points = self.ax.scatter(
                [], [], s=25, color=self.colors[0], zorder=1)
            # fading trajectory
            seg_points = np.array([self.xdata, self.ydata]).T.reshape(-1, 1, 2)
            segments = np.concatenate([seg_points[:-1], seg_points[1:]], axis=1)
            lc = LineCollection(segments, colors=np.zeros((len(time)-1,4)))
            self.lines_collection = ax.add_collection(lc)
            self.decay_colors = np.tile(self.colors[traj%len(self.colors)], (len(self.time)-1,1))

        # setup text
        self.time_template = 'Time = %.1fs'
        self.time_text = self.ax.text(0.05, 0.9, '', transform=self.ax.transAxes)
        self.time_text.set_text('')

        # show animation
        self.ani = animation.FuncAnimation(
            fig=self.ax.get_figure(), 
            func=self.update, 
            frames=len(self.time)-1, 
            interval=self.time_step, 
            blit=True)
        print(len(self.time)-1)
        print(self.time_step)
        if save == 'false':
            self.ax.get_figure().canvas.mpl_connect("key_press_event", self.pause)
            plt.show()
        else:
            self.ani.save(save, fps=self.speed/self.interval)

    def update(self, frame):
        if frame == 0:
            # reset decay of alphas
            self.decay_alphas = np.zeros(len(self.time)-1)

        # update alphas each frame
        if (frame+1) <= len(self.decay_list):
            self.decay_alphas[:(frame+1)] = self.decay_list[-(frame+1):]
        else:
            self.decay_alphas[(frame+1-len(self.decay_list)):(frame+1)] = self.decay_list
            self.decay_alphas[:(frame+1-len(self.decay_list))] = 0


        # update text
        self.time_text.set_text(self.time_template % (self.time[frame]))

        if self.multiple_trajs:
            for traj in range(self.xdata.shape[1]):
                # update the line plot:
                self.decay_vector = np.copy(self.decay_colors[traj])
                self.decay_vector[:,-1] = self.decay_alphas
                self.decay_vector = [tuple(color) for color in self.decay_vector]
                self.lines_collection[traj].set_colors(np.array(self.decay_vector))
                
                # update point
                self.points[traj].set_offsets(
                    np.array([self.xdata[frame+1, traj], self.ydata[frame+1, traj]]).T)
            return [*self.lines_collection, *self.points, self.time_text]
        
        else:
            # update the line plot:
            self.decay_vector = np.copy(self.decay_colors)
            self.decay_vector[:,-1] = self.decay_alphas
            self.decay_vector = [tuple(color) for color in self.decay_vector]
            self.lines_collection.set_colors(np.array(self.decay_vector))
            # update point
            self.points.set_offsets(
                np.array([self.xdata[frame+1], self.ydata[frame+1]]).T)
            return [self.lines_collection, self.points, self.time_text]

    def pause(self, event):
        if event.key==" ":
            if self.paused:
                self.ani.resume()
            else:
                self.ani.pause()
        self.paused = not self.paused


class HandleResults:
    """
        A class that enables to interact with results from the computations
        of the positions of click sources.

        Parameters
        ----------
        csv_path : string
            Path to the csv file containing data to be used.
        media_folder :
            Path to the folder containing audio recordings.

        Methods
        -------
        import_waveform(file_date):
            Imports and filters an audio file into the object.
            This is mainly to import audio data for other functions.
        plot_column(column, groups, title, xlabel, ylabel, save):
            Plots any column of the data imported VS time.
        display_delays(hyper, save):
            Plots all TDOAs (6 of them) in time.
        display_peaks(file_date, limits, save, min_nrg):
            Displays waveform with arrows to show click detections.
        display_tdoa_solo(file_date, row, window, save):
            Display a detailed view in waveform of the tdoas for a single click.
        display_animation(exclude_group, group, borders, speed, display_pts_for):
            Plots an animation of the different clusters in time.
            Each point is a cluster, the point is the last known position 
            while the lines are the previous positions of this point.
        display_trajs(self, date_from, date_to, borders , save):
            Plots all trajectories in the selected time period together.
            Each color/line is the trajectory of a selected cluster, 
            the point is the last known position.
        polar_plots(self, date_from=-1, date_to=-1, save=False):
            Plots
    """
    def __init__(
    self, 
    csv_path, 
    station_path,
    media_folder="/media/loic/DOLPHINFREE/Acoustique"):
        # Inputs
        self.data = pd.read_csv(csv_path, parse_dates=["file_date", "datetime"])
        self.media_folder = media_folder
        self.csv_path = csv_path
        self.stations_coords = pd.read_csv(station_path).values
        self.stations_coords = (self.stations_coords.T - self.stations_coords.mean(axis=1))

        # Default additional parameters
        self.colors_list = [
            '#1f77b4', '#ff7f0e', '#2ca02c', '#d62728', 
            '#9467bd', '#8c564b', '#e377c2', '#7f7f7f', '#bcbd22', '#17becf']
        self.fontsizes = 15
        self.current_file_date = None
        self.data["total_seconds"] = (self.data["datetime"] - self.data["datetime"].min()).dt.total_seconds()


        # create directory for images
        self.output_images = os.path.join(
            os.path.dirname(self.csv_path), "pictures")
        if not os.path.exists(self.output_images):
            os.makedirs(self.output_images)
            print(f"Created path '{self.output_images}'.")
      
        # determine the number of stations
        self.n_stations = 1
        while (f"t0{self.n_stations}" in self.data.columns):
            self.n_stations += 1
            if self.n_stations >= 50:
                raise ValueError(f"Cannot determine the number of stations from '{csv_path}'")
        if self.n_stations < 3:
            raise ValueError(
                f"Found {self.n_stations} stations from '{csv_path}'"
                ", minimum should be 3.")

    def import_waveform(self, file_date):
        from utils.sound import butter_pass_filter
        """
            Imports and filters an audio file into the object.
            This is mainly to import audio data for other functions.

            Parameters
            ----------
            file_date : string
                A datetime in format "%Y%m%d_%H%M%S".
                It should correspond to a wavefile datetime.
        """
        file_date = datetime.strptime(file_date, "%Y%m%d_%H%M%S") 
        filepath = os.path.join(
            self.media_folder, 
            file_date.strftime("%Y%m%d_%H%M%SUTC_V12.wav"))

        if (file_date not in 
            pd.to_datetime(self.data.file_date.unique())):
            print(f"{BColors.FAIL}Error{BColors.ENDC}: "
                "file_date given does not appear in data.")
        elif not os.path.exists(filepath):
            print(f"{BColors.FAIL}Error{BColors.ENDC}: "
                f"did not found wavefile in '{filepath}'.")
        else:
            self.original_audio, self.sr = librosa.load(
                filepath, sr=None, mono=False)
            self.current_file_date = file_date
            self.audio_data = self.original_audio.copy()
            self.audio_data = butter_pass_filter(
                self.original_audio, 
                self.sr, 
                cutoff=25000)
            print(f"{BColors.OKGREEN}Audio data imported.{BColors.ENDC}")

    def plot_column(self, column, date_from=-1, date_to=-1, groups="", title="", xlabel="", ylabel="", save=False):
        """
            Plots any column of the data imported VS time.

            Parameters
            ----------
            column : string
                Name of the column to plot (VS time).
            date_from : float
                From where the plot should start (in seconds).
            date_to : float
                Where the plot should stop (in seconds).
            groups : string
                Name of the column to use to color points in plot.
            title : string
                Title to display on the plot
            xlabel : string
                Label to display for x-axis
            ylabel : string
                Label to display for y-axis
            save : boolean
                Whether to save plot to pdf (True) 
                or to display it to the user (False)
        """

        # select dates to display
        data_  = self.data.copy()
        if date_from >= 0:
            data_ = data_.where(data_.total_seconds >= date_from).dropna().copy()
        if date_to >= 0:
            data_ = data_.where(data_.total_seconds <= date_to).dropna().copy()

        # prepare data
        data_.reset_index(drop=True, inplace=True)

        plt.figure(figsize=(12, 8))
        plt.title(title)

        if groups == "":
            plt.scatter(
                data_["total_seconds"], 
                data_[column], 
                c='black', 
                s=1)
        else:
            plt.scatter(
                data_["total_seconds"], 
                data_[column], 
                c=[self.colors_list[int(idx)%len(self.colors_list)] 
                    for idx in data_[groups]], 
                s=1)

        plt.ylabel(ylabel)
        plt.xlabel(xlabel)

        if save:
            plt.savefig(
                os.path.join(
                    self.output_images, 
                    title.replace(' ','_').lower()+'.pdf'))  
            plt.close()
        else:
            plt.show()

    def display_delays(self, hyper=True, save=False):
        """
            Plots all TDOAs in time.

            Parameters
            ----------
            hyper : boolean
                Uses tdoas from hyper resolution (True)
                or regular cross-correlation (False)
            save : boolean
                Whether to save plot to pdf (True) 
                or to display it to the user (False)
        """
        if hyper:
            couples = [f"h_t{i}{j}" for i in range(self.n_stations) for j in range(i+1,self.n_stations)]
        else:
            couples = [f"t{i}{j}" for i in range(self.n_stations) for j in range(i+1,self.n_stations)]
        
        plt.figure(figsize=(12,8))
        for couple in couples:
            plt.scatter(
            self.data["datetime"],
            self.data[couple],
            label=f"Delay {couple[-1]}->{couple[-2]}", s=5)

        plt.legend(loc='upper center', ncol=len(couples))
        plt.title("TDoA of clicks in audio recording")
        plt.xlabel("Datetime")
        plt.ylabel("TDoA (in sec)")

        if save:
            plt.savefig(
                os.path.join(
                    self.output_images, 
                    os.path.basename(self.csv_path[:-4])+"_all_TDoAs.pdf"), 
                dpi=500)  
            plt.close()
        else:
            plt.show()

    def display_peaks(self, file_date, limits=(0,-1), save=False, min_nrg=-1):
        """
            Displays waveform with arrows to show click detections.

            Parameters
            ----------
            file_date : string
                A datetime in format "%Y%m%d_%H%M%S".
                It should correspond to a wavefile datetime.
            limits : tuple of integers
                Limits to the wavefile that is plotted in seconds.
            save : boolean
                Whether to save plot to pdf (True).
                or to display it to the user (False).
            min_nrg : integer
                Minimal energy (Teager-Kaiser) to show click in plot.
        """
        from utils.sound import TeagerKaiser_operator
        if (self.current_file_date != datetime.strptime(file_date, "%Y%m%d_%H%M%S")):
            self.import_waveform(file_date)


        if limits[1] == -1:
            limits = (limits[0], self.audio_data.shape[-1]/self.sr)


        # get audio data
        audio_extract = self.audio_data[0][int(limits[0]*self.sr):int(limits[1]*self.sr)]
        tk_extract = np.abs(TeagerKaiser_operator(audio_extract))
        peaks = self.data.where(
            (self.data.file_date == self.current_file_date.strftime("%Y-%m-%d %H:%M:%S")) &
            (self.data["datetime"] > (self.current_file_date + pd.to_timedelta(limits[0]/self.sr, unit="s"))) &
            (self.data["datetime"] < (self.current_file_date + pd.to_timedelta(limits[1]*self.sr, unit="s")))
        ).dropna(how="all")["datetime"]
        peaks = ((peaks - self.current_file_date).dt.total_seconds()*self.sr).astype(int)
        
        # making a readable plot
        fig, axs = plt.subplots(2,1, sharex=True, figsize=(10,8))
        fig.suptitle("Detections of clicks vizualised on waveform (top) and TK filter (bottom).")

        axs[0].plot(audio_extract, label="Waveform", color="tab:blue")
        axs[0].scatter(
            x=peaks, y=audio_extract[peaks], s=5,
            label="Detections", color="tab:orange")
        axs[0].legend()
        axs[0].set_ylabel("Amplitude")

        axs[1].plot(tk_extract, label="Waveform", color="tab:blue")
        axs[1].scatter(
            x=peaks, y=tk_extract[peaks], s=5,
            label="Detections", color="tab:orange")
        axs[1].legend()
        axs[1].set_ylabel("Amplitude")
        axs[1].set_xlabel("Time (in sec)")

        if save:
            plt.savefig(
                os.path.join(
                    self.output_images, 
                    os.path.basename(self.csv_path[:-4])+"_detection.pdf"),
                dpi=500)  
            print(f"{BColors.OKBLUE}Figure saved in",
                os.path.join(
                    self.output_images,
                    os.path.basename(self.csv_path[:-4])+'_detection.pdf'),
                BColors.ENDC)
            plt.close()
        else:
            plt.show()
        plt.close('all')

    def display_tdoa_solo(self, file_date, row=None, window=0.0005, save=False):
        """
            Display a detailed view in waveform of the tdoas for a single click.

            Parameters
            ----------
            file_date : string
                A datetime in format "%Y%m%d_%H%M%S".
                It should correspond to a wavefile datetime.
            row : integer
                The index of a click detected in the audio_recording.
                This parameter controls the click displayed.
            window : float
                The size of the window around each click to use for the plot.
            save : boolean
                Whether to save plot to pdf (True).
                or to display it to the user (False).
        """
        if (self.current_file_date != datetime.strptime(file_date, "%Y%m%d_%H%M%S")):
            self.import_waveform(file_date)

        subdata = self.data.where(
            self.data.file_date == self.current_file_date).copy().dropna()
        subdata.reset_index(drop=True, inplace=True)

        if not isinstance(row, int):
            print(f"{BColors.FAIL}TypeError{BColors.ENDC}: "
                "display_tdoa_solo() missing 1 required positional argument: 'row'"
                f"\n\t{BColors.OKCYAN}Tip{BColors.ENDC}: "
                f"they are {len(subdata)} clicks in the indicated file.")
            return None

        window_sr = int(window*self.sr)

        fig, axs = plt.subplots(4,1, sharex=True, figsize=(9,6))

        for i in range(self.audio_data.shape[0]):
            extract = self.audio_data[i][
                (int((subdata.loc[row,'datetime']-subdata.loc[row,'file_date'])*self.sr)-window_sr):
                (int((subdata.loc[row,'datetime']-subdata.loc[row,'file_date'])*self.sr)+window_sr)]
            norm_extract = ((extract-min(extract))/(max(extract)-min(extract))-0.5)*2

            wave_legend = axs[i].plot(norm_extract, label='Waveform', lw=1)
            axs[i].text(
                x=(self.sr*0.78)/1000,
                y=-1, 
                s=f"Hydrophone {i}", 
                weight="bold", 
                fontsize=self.fontsizes)

            if i>=1:
                delay = subdata.loc[row,f'h_t0{i}']
                click0_legend = axs[i].axvline(
                    x=window_sr, 
                    color='tab:red', 
                    label='Position of click in channel 0',
                    ls='--',
                    lw=1.5)
                click1_legend = axs[i].axvline(
                    x=window_sr+delay*self.sr, 
                    color='tab:orange', 
                    label='Position of click in current channel',
                    ls='--',
                    lw=1.5)

                axs[i].annotate(
                    "", 
                    xy=(window_sr+delay*self.sr, 0.5), 
                    xytext=(window_sr, 0.5),
                    arrowprops=dict(arrowstyle="<->", color='black'))
                axs[i].text(
                    x=np.mean([window_sr+delay*self.sr, window_sr]), 
                    y=0.75, 
                    s=f'{delay:1.2e} s', 
                    color='black', 
                    ha='center', 
                    fontsize=self.fontsizes)

            axs[i].set_xlim(0, window_sr*2)
            axs[i].set_ylim(-1.1, 1.1)
            axs[i].set_yticks([-1, 0, 1])

            # Keep 1 y-axis and 1 x-axis (that is centered)
            axs[i].spines['top'].set_position('center') # spine for xaxis 
            axs[i].spines['bottom'].set_visible(False)
            axs[i].spines['right'].set_visible(False)
            axs[i].tick_params(
                axis='x',          # changes apply to the x-axis
                which='both',      # both major and minor ticks are affected
                bottom=False,      # ticks along the bottom edge are off
                top=False,         # ticks along the top edge are off
                labelbottom=False) # labels along the bottom edge are off

        # finish setup
        axs[-1].set_xlabel(f'\n\nTime in samples (SR={self.sr//1000} kHz)', fontsize=self.fontsizes+3)
        fig.legend(
            [wave_legend[0], click0_legend, click1_legend], 
            [wave_legend[0].get_label(), click0_legend.get_label(), click1_legend.get_label()],
            loc='center', fontsize=self.fontsizes-3, ncols=3, bbox_to_anchor=(.525,.1125), handleheight=.3)
        axs[-1].spines['bottom'].set_visible(True)
        axs[-1].tick_params(
            axis='x',          # changes apply to the x-axis
            which='both',      # both major and minor ticks are affected
            bottom=True,      # ticks along the bottom edge are off
            top=False,         # ticks along the top edge are off
            labelbottom=True) # labels along the bottom edge are off
        fig.supylabel('Amplitude', fontsize=self.fontsizes+3)
        
        fig.tight_layout()

        if save:
            plt.savefig(
                os.path.join(
                    self.output_images, 
                    os.path.basename(self.csv_path[:-4])+"_TDOA.pdf"), 
                dpi=500)
            print(f"{BColors.OKBLUE}Figure saved in",
                os.path.join(
                    self.output_images, 
                    os.path.basename(self.csv_path[:-4])+'_TDOA.pdf'),
                BColors.ENDC)
            plt.close()
        else:
            plt.show()
    
    def display_animation(self, date_from=-1, date_to=np.inf, borders=200, speedup=1, display_pts_for=5, precision=0.1, save=False):
        """
            Plots an animation of the different clusters in time.
            Each point is a cluster, the point is the last known position 
            while the lines are the previous positions of this point.

            Parameters
            ----------
            date_from : float, optionnal
                From where the animation should start (in seconds).
                Defaults is -1 (all values).
            date_to : float, optionnal
                Where the animation should stop (in seconds).
                Default is np.inf (all values).
            borders: int, optionnal
                Limit of the plot's axis (in meters).
                Default is 200 m.
            speedup : float, optionnal
                Whether the animation should be showed at real time (1) or not.
                Ex : if speed = 10, then 10 seconds of recording are shown in 1 second.
                Default is 1.
            display_pts_for : float, optionnal
                Decay time (in seconds) of points. 
                Tracks stays with transparency of 75%.
                Default is 5.
            precision : float, optionnal
                Updates the positions of points every 'precision' seconds in real time.
                Default is 0.1 s.
            save : bool, optionnal
                If True, will save a file "animation.gif" in /pictures.
                Default is False (not saving).
        """
        if "x" not in self.data.columns:
            raise NotImplementedError(
                "Loaded dataset contains only angles, no distances. "
                "Animation of points in 2D cannot be displayed.")

        # compute positions to be shown (interpolation)
        t = np.arange(
            self.data.total_seconds.min(), 
            self.data.total_seconds.max()+precision,
            precision)
        t = t[t >= date_from]
        t = t[t <= date_to]

        x = []
        y = []
        for cluster in self.data.clusters.unique():
            subdf = self.data.where(self.data.clusters == cluster).copy()
            subdf.sort_values(by="total_seconds", inplace=True)
            cluster_time = np.array(subdf.total_seconds)
            cluster_x = np.array(subdf.x)
            cluster_y = np.array(subdf.y)

            x_interp = np.interp(t, cluster_time, cluster_x)
            x_interp[np.where(t < min(cluster_time))] = np.nan
            x_interp[np.where(t > max(cluster_time))] = np.nan
            y_interp = np.interp(t, cluster_time, cluster_y)
            y_interp[np.where(t < min(cluster_time))] = np.nan
            y_interp[np.where(t > max(cluster_time))] = np.nan

            # if date_from and date_to then we could have empty lists.
            if np.sum(np.isnan(x_interp)) != len(x_interp):
                x = x + [x_interp]
                y = y + [y_interp]

        fig, ax = plt.subplots()
        fig.suptitle("Top-down view on trajectoriess")
        ax.set(
            xlim=[-borders, borders], 
            ylim=[-borders, borders],
            xlabel='X-axis (in m)', 
            ylabel='Y-axis (in m)')
        ax.set_aspect('equal')
        ax.grid()
        origin = ax.plot(0, 0, label="origin", 
            marker="*", mec="black", mfc="red", ms=10)

        future_gif = Animator(
            t, np.array(x).T , np.array(y).T, 
            ax=ax, 
            name_cmap="tab10",
            speed=speedup, 
            decay=display_pts_for, 
            save=(os.path.join(self.output_images, 'animation.gif') if save else 'false')) 

    def display_trajs(self, date_from=-1, date_to=-1, borders=200, save=False, raw=False, show_pinger=False):
        """
            Plots all trajectories in the selected time period together.
            Each color/line is the trajectory of a selected cluster, 
            the point is the last known position.

            Parameters
            ----------
            date_from : float
                From where the animation should start (in seconds).
            date_to : float
                Where the animation should stop (in seconds).
            borders: int
                Limit of the plot's axis (in meters)
        """
        if "x" not in self.data.columns:
            raise NotImplementedError(
                "Loaded dataset contains only angles, no distances. "
                "Plot of points in 2D cannot be displayed.")

        # select dates to display
        data_  = self.data.copy()
        if date_from > 0:
            data_ = data_.where(data_.total_seconds > date_from).dropna().copy()
        if date_to > 0:
            data_ = data_.where(data_.total_seconds < date_to).dropna().copy()

        # prepare data
        data_.reset_index(drop=True, inplace=True)
        colors_list = np.array([[31, 119, 180, 1], [255, 127, 14, 1], [44, 160, 44, 1],
                       [214, 39, 40, 1], [148, 103, 189, 1], [140, 86, 75, 1],
                       [227, 119, 194, 1], [127, 127, 127, 1], [188, 189, 34, 1],
                       [23, 190, 207, 1]])/255
        pinger_ratio = 0.9
        x_name, y_name = 'x', 'y'

        # make plot
        fig, ax = plt.subplots(figsize=(9,9))
        if raw:
            ax.set(
                xlim=[-2.5, 2.5], 
                ylim=[-2.5, 2.5], 
                xlabel='Normed distances', 
                ylabel='Normed distances')
        else:
            ax.set(
                xlim=[-borders,borders], 
                ylim=[-borders,borders], 
                xlabel='Metres', 
                ylabel='Metres')

        origin = ax.scatter(
            0, 0, 
            color='tab:red', edgecolors='black', s=100, marker="*", label="origin")

        # clean pinger column with ratio
        if show_pinger:
            for cluster in pd.unique(data_.clusters):
                idx_cluster = np.where(data_.clusters == cluster)[0]
                ratio = sum(data_.loc[idx_cluster, "pinger"])/len(idx_cluster)
                if ratio > pinger_ratio:
                    data_.loc[idx_cluster, "pinger"] = True
                else:
                    data_.loc[idx_cluster, "pinger"] = False
        else:
            data_.pinger=False

        # add lines and points
        for idx, cluster in enumerate(data_.clusters.unique()):
            subdf = data_.where(data_.clusters == cluster).dropna().copy()
            if show_pinger:
                pinger = True if subdf.pinger.sum()==len(subdf) else False
            else:
                pinger = False

            if raw:
                coord1 = subdf.raw_distance*np.sin(subdf.raw_azimuth)
                coord2 = subdf.raw_distance*np.cos(subdf.raw_azimuth)
            else:
                coord1 = subdf[y_name]
                coord2 = subdf[x_name]

            ax.plot(
                coord1, coord2, 
                color=colors_list[int(cluster)%len(colors_list)][:3],
                lw=2, alpha=0.2, zorder=-1)
            ax.scatter(
                coord1.iloc[-1], coord2.iloc[-1], 
                facecolor=colors_list[int(cluster)%len(colors_list)][:3],
                edgecolor=([0,0,0,1] if pinger else colors_list[int(cluster)%len(colors_list)]),
                s=50, alpha=1, zorder=1)
                
        if save:
            plt.savefig(
                os.path.join(
                    self.output_images, 
                    os.path.basename(self.csv_path[:-4])+"_all_trajs.pdf"), 
                dpi=500)  
            print(f"{BColors.OKBLUE}Figure saved in",
                os.path.join(
                    self.output_images, 
                    os.path.basename(self.csv_path[:-4])+'_all_trajs.pdf'),
                BColors.ENDC)
            plt.close()
        else:
            plt.show(block=True)

    def polar_plots(self, date_from=-1, date_to=-1, N_angles=32, save=False, _animation=False):
        """
            Plots a "barplot" on 2 circles : one for the azimuths and one 
            for the elevation angles.
            
            Parameters
            ----------
            date_from : float, optionnal
                From where the animation should start (in seconds).
                Defaults is -1 (all values).
            date_to : float, optionnal
                Where the animation should stop (in seconds).
                Default is np.inf (all values).
            N_angles : int, optionnal
                The number of bars to plot, 
                must be a multiple of 4 (4 quadrans in the circle)
                Default is 32.
            save : bool, optionnal
                If True, will save a file "_polar_plots.pdf" in /pictures.
                Default is False (not saving).
            _animation : bool, optionnal
                Whether this function is used to draw an animation, in that case,
                returns that data needed for it to work.
                Default is False
        """
        # select dates to display
        data_  = self.data.copy()
        if date_from >= 0:
            data_ = data_.where(data_.total_seconds >= date_from).dropna().copy()
        if date_to >= 0:
            data_ = data_.where(data_.total_seconds <= date_to).dropna().copy()

        # prepare data
        data_.reset_index(drop=True, inplace=True)

        if not (isinstance(N_angles, int) and ((N_angles%4)==0)):
            raise TypeError(f"N_angles {N_angles} is not an integer that can be divided by 4.")

        # Compute pie slices
        angles = np.linspace(-np.pi, np.pi, N_angles, endpoint=False)
        bin_count_azimuth = np.zeros(N_angles)
        bin_count_elevation = np.zeros(N_angles)
        for i, angle in enumerate(angles):
            min_angle = max(-np.pi, angle-(np.pi/N_angles))
            max_angle = min(np.pi, angle+(np.pi/N_angles))
            bin_count_azimuth[i] = len(data_.azimuth.where(
                (data_.azimuth > min_angle) &
                (data_.azimuth <= max_angle)
                ).dropna())
            bin_count_elevation[i] = len(data_.elevation.where(
                (data_.elevation > min_angle) &
                (data_.elevation <= max_angle)
                ).dropna())

        if len(data_)==0:
            # making up data to have a good empty plot
            bin_count_azimuth = np.ones(N_angles)*0.5
            bin_count_elevation = np.ones(N_angles)*0.5
            edgecolor = "white"
            color = "white"
        else:
            edgecolor = "black"
            color = "gray"

        ### Make plot (2 pies : azimuths and elevations)
        fig, axs = plt.subplots(1, 2, subplot_kw={"projection":"polar"}, figsize=(16,9))

        ## azimuth side
        azimuth_bars = axs[0].bar(
            x=angles,  height=bin_count_azimuth, width=2*np.pi/N_angles, 
            edgecolor=edgecolor, color=color)

        # add stations
        stations_azimuths = []
        for station in range(self.n_stations):
            station_azimuth = cartesian2spherical(
                self.stations_coords[station,0],
                self.stations_coords[station,1],
                self.stations_coords[station,2])[-1]
            station_distance_xy = np.linalg.norm(self.stations_coords[station,:2])
            relative_distance_xy = max(bin_count_azimuth-(np.max(bin_count_azimuth)/25))*station_distance_xy/max(np.linalg.norm(self.stations_coords[:,:2], axis=1))
            stations_azimuths += [axs[0].scatter(
                x=station_azimuth, y=relative_distance_xy, 
                s=50, label=f"H{station}")]

        # add etiquettes
        axs[0].text(
            0*np.pi/2, 
            np.max(bin_count_azimuth)+np.max(bin_count_azimuth)/25, 
            "Right", 
            fontsize=14, 
            horizontalalignment="right", verticalalignment="center",
            bbox=dict(boxstyle='round', facecolor='wheat', alpha=1))
        axs[0].text(
            1*np.pi/2, 
            np.max(bin_count_azimuth)+np.max(bin_count_azimuth)/25, 
            "Front", 
            fontsize=14, 
            horizontalalignment="center", verticalalignment="top",
            bbox=dict(boxstyle='round', facecolor='wheat', alpha=1))
        axs[0].text(
            2*np.pi/2, 
            np.max(bin_count_azimuth)+np.max(bin_count_azimuth)/25, 
            "Left", 
            fontsize=14, 
            horizontalalignment="left", verticalalignment="center",
            bbox=dict(boxstyle='round', facecolor='wheat', alpha=1))
        axs[0].text(
            3*np.pi/2, 
            np.max(bin_count_azimuth)+np.max(bin_count_azimuth)/25, 
            "Back", 
            fontsize=14, 
            horizontalalignment="center", verticalalignment="bottom",
            bbox=dict(boxstyle='round', facecolor='wheat', alpha=1))

        axs[0].set_title("Distribution of estimated azimuth angles", fontsize=18)
        axs[0].legend(
            title="Relative location of hydrophones",
            loc="upper right", labelspacing=0.1, columnspacing=0.1, handletextpad=0.1,
            fontsize=14, title_fontsize=14, ncol=4)
        axs[0].tick_params(axis='both', which='major', labelsize=14)

        ## elevation side
        elevation_bars = axs[1].bar(
            x=angles, height=bin_count_elevation, width=2*np.pi/N_angles, 
            edgecolor=edgecolor, color=color)

        # add stations;
        stations_elevations = []
        for station in range(self.n_stations):
            station_elevation = cartesian2spherical(
                self.stations_coords[station,0],
                self.stations_coords[station,1],
                self.stations_coords[station,2])[1]
            station_sign = np.sign(cartesian2spherical(
                self.stations_coords[station,0],
                self.stations_coords[station,1],
                self.stations_coords[station,2])[2])
            if station_sign==0:
                station_sign = 1
            stations_elevations += [axs[1].scatter(
                x=station_elevation*station_sign, 
                y=np.max(bin_count_elevation-(np.max(bin_count_elevation)/20)), 
                s=50, label=f"H{station}")]

        # add etiquettes
        axs[1].text(
            0, 
            np.max(bin_count_elevation)+np.max(bin_count_elevation)/25, 
            "Top", 
            fontsize=14, 
            horizontalalignment="center", verticalalignment="top",
            bbox=dict(boxstyle='round', facecolor='wheat', alpha=1))
        axs[1].text(
            np.pi, 
            np.max(bin_count_elevation)+np.max(bin_count_elevation)/25, 
            "Bottom", 
            fontsize=14, 
            horizontalalignment="center", verticalalignment="bottom",
            bbox=dict(boxstyle='round', facecolor='wheat', alpha=1))
        axs[1].set_title("Distribution of estimated elevation angles", fontsize=18)
        axs[1].legend(
            title="Relative location of hydrophones", 
            loc="upper right", labelspacing=0.1, columnspacing=0.1, handletextpad=0.1,
            fontsize=14, title_fontsize=14, ncol=4)
        axs[1].set_theta_offset(np.pi/2)
        axs[1].set_theta_direction(-1)
        axs[1].set_thetamin(-5)
        axs[1].set_thetamax(185)
        axs[1].tick_params(axis='both', which='major', labelsize=14)

        fig.tight_layout()

        if _animation:
            plt.close("all")
            return (
                fig, axs, 
                azimuth_bars, elevation_bars, 
                stations_azimuths, stations_elevations
                )
        else:
            if save:
                plt.savefig(
                    os.path.join(
                        self.output_images, 
                        os.path.basename(self.csv_path[:-4])+"_polar_plots.pdf"), 
                    dpi=500)  
                print(f"{BColors.OKBLUE}Figure saved in",
                    os.path.join(
                        self.output_images, 
                        os.path.basename(self.csv_path[:-4])+'_polar_plots.pdf'),
                    BColors.ENDC)
                plt.close()
            else:
                return fig, axs
    
    def polar_plot_animation(self, date_from=-1, date_to=-1, N_angles=32, save=False, speedup=1, step=1):
        # prepare data
        if date_from < 0:
            date_from = 0
        if date_to == -1:
            date_to = self.data.total_seconds.max()
        
        data_  = self.data.copy()
        data_.reset_index(drop=True, inplace=True)

        if not (isinstance(N_angles, int) and ((N_angles%4)==0)):
            raise TypeError(f"N_angles {N_angles} is not an integer that can be divided by 4.")

        # create arrays for displays
        time_steps = np.arange(date_from, date_to+step, step)
        angles = np.linspace(-np.pi, np.pi, N_angles, endpoint=False)

        # Compute pie slices
        bin_counts_azimuths = []
        bin_counts_elevations = []
        for min_time, max_time in zip(time_steps[:-1], time_steps[1:]):
            sub_data_ = data_.copy()
            sub_data_ = sub_data_.where(
                (sub_data_.total_seconds >= min_time) &
                (sub_data_.total_seconds <= max_time)
            ).dropna().copy()
            sub_data_.reset_index(drop=True, inplace=True)

            bin_count_azimuth = np.zeros(N_angles)
            bin_count_elevation = np.zeros(N_angles)
            for i, angle in enumerate(angles):
                min_angle = max(-np.pi, angle-(np.pi/N_angles))
                max_angle = min(np.pi, angle+(np.pi/N_angles))
                bin_count_azimuth[i] = len(sub_data_.azimuth.where(
                    (sub_data_.azimuth > min_angle) &
                    (sub_data_.azimuth <= max_angle)
                    ).dropna())
                bin_count_elevation[i] = len(sub_data_.elevation.where(
                    (sub_data_.elevation > min_angle) &
                    (sub_data_.elevation <= max_angle)
                    ).dropna())
            bin_counts_azimuths += [bin_count_azimuth/np.max(bin_count_azimuth)]
            bin_counts_elevations += [bin_count_elevation/np.max(bin_count_elevation)]
        
        # Compute station positions
        station_dea_coords = [
            cartesian2spherical(
                self.stations_coords[station,0],
                self.stations_coords[station,1],
                self.stations_coords[station,2])
            for station in range(self.n_stations)]

        ### Make plot (2 pies : azimuths and elevations)
        fig, axs = plt.subplots(1,2, figsize=(16,9), subplot_kw={"projection":"polar"})

        fig.suptitle("Initialisation")

        azimuth_bars = axs[0].bar(
            x=angles, height=np.zeros(N_angles), width=2*np.pi/N_angles,
            edgecolor="black", color="gray")
        azimuth_scatters = [axs[0].scatter(
                x=station_dea_coords[station][-1], 
                y=np.linalg.norm(self.stations_coords[station,:2])/max(np.linalg.norm(self.stations_coords[:,:2], axis=1)), 
                s=50, label=f"Station {station}") 
            for station in range(self.n_stations)]
        for quarter, side in enumerate(["Right", "Front", "Left", "Back"]):
            axs[0].text(
                quarter*np.pi/2, 
                1, 
                side, 
                fontsize=9, 
                horizontalalignment="center", verticalalignment="center",
                bbox=dict(boxstyle='round', facecolor='wheat', alpha=1))
        axs[0].set_title("Distribution of estimated azimuth angles")
        axs[0].legend(
            title="Station relative locations", 
            fontsize=7, title_fontsize=7, ncols=4)


        elevation_bars = axs[1].bar(
            x=angles, height=np.zeros(N_angles), width=2*np.pi/N_angles,
            edgecolor="black", color="gray")
        elevation_scatters = [axs[1].scatter(
                x=station_dea_coords[station][1]*np.sign(station_dea_coords[station][-1]), 
                y=1, 
                s=50, label=f"Station {station}") 
            for station in range(self.n_stations)]
        for quarter, side in enumerate(["Top", "Bottom"]):
            axs[1].text(
                quarter*np.pi, 
                1, 
                side, 
                fontsize=9, 
                horizontalalignment="center", verticalalignment="center",
                bbox=dict(boxstyle='round', facecolor='wheat', alpha=1))
        axs[1].set_title("Distribution of estimated elevation angles")
        axs[1].legend(
            title="Station relative locations", 
            fontsize=7, title_fontsize=7, ncols=4)
        axs[1].set_theta_offset(np.pi/2)
        axs[1].set_theta_direction(-1)
        axs[1].set_thetamin(-5)
        axs[1].set_thetamax(185)

        # fig.tight_layout()

        def update(frame):
            for idx, b in enumerate(azimuth_bars):
                b.set_height(bin_counts_azimuths[frame][idx])

            for idx, b in enumerate(elevation_bars):
                b.set_height(bin_counts_elevations[frame][idx])

            fig.suptitle(f"Relative number of detections between\n{time_steps[:-1][frame]} and {time_steps[1:][frame]} seconds.")

        self.ani = animation.FuncAnimation(
            fig,
            update,
            repeat=True,
            blit=False,
            frames=np.arange(len(time_steps)-1),
            interval=(step*1000)/speedup)

        self.paused = False
        def pause(event):
            if event.key==" ":
                if self.paused:
                    self.ani.resume()
                else:
                    self.ani.pause()
            self.paused = not self.paused

        if save:
            self.ani.save(
                os.path.join(self.output_images, f"radar_from_{date_from}_to_{date_to}_sec.gif"),
                fps=speedup/step)
        else:
            fig.canvas.mpl_connect("key_press_event", pause)
            plt.show()
        plt.close("all")