#%%## Importations #####
import os
import csv
import torch
import argparse
import numpy as np
import pandas as pd
from datetime import datetime


#%%## Definitions #####
class BColors:
    """
    A class containing strings that code colors.
    They can be used in print().

    WARNING : Always end a string by BColors.ENDC
        if a color is added at the beginning of the string
    """
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

def find_device():
    """
    A function to fetch the type of device on which
    PyTorch will run operations.

    Returns
    -------
    str :
        "cuda', "mps" or "cpu"
    """
    if torch.cuda.is_available():
        return "cuda"
    elif torch.backends.mps.is_available():
        return "mps"
    else:
        return "cpu"

def import_csv(csv_name, folder, separator, useless=True):
    """
    Parameters
    ----------
    csv_name : STRING
        Name of the csv file that needs to be imported
    folder : STRING, optional
        Path to the folder containing csv file. 
        The default is data_folder (see Parameters section).
    separator : STRING
        Separator for folders.
    useless : BOOLEAN, optional
        Does the file contains useless infos after the table ? 
        If True, remove those infos

    Returns
    -------
    data : LIST
        List containing all rows of the csv file. 
        Exclusion of last lines if they don't end with .wav.
    """
    data = []
    
    # read data csv
    with open(folder + separator + csv_name, newline="") as csv_file:
        lines = csv.reader(csv_file, delimiter=',')
        for row in lines:
            data = data + [row]
    csv_file.close()
    
    # clean csv (last lines contain useless info)
    n = 0
    while useless:
        # in case wrong use of the function
        if n == len(data):
            useless = False
            n = 0
            raise Exception("First column contains no audio file name")
        # exit the loop when audio is found
        if data[-n][0].endswith(".wav"):
            useless = False 
        # search audio in next line
        else:
            n += 1
    
    data = data[0:-n]
    return data

def get_csv(csv_folder, slash="\\"):
    """
    Parameters
    ----------
    csv_folder : STRING
        Path to a folder containing ONLY csv file.
    slash : STRING
        Separator for folders.

    Returns
    -------
    data_20_21 : DATAFRAME
        Contains the inforamtion inside the .CSVs.
    sorted_names : LIST
        Names corresponding to audio files in the first column of the .CSVs.
    """
    csv_names = [a for a in os.listdir(csv_folder) if a.endswith('.csv')]

    # import data
    data = import_csv(csv_names[0], csv_folder, separator=slash)
    for i in range(1,len(csv_names)):
        data = data + import_csv(csv_names[i], csv_folder, separator=slash)[1:]
    data_frame = pd.DataFrame(data=data[:][1:], columns=data[:][0])

    # change dtype for selected columns
    for i in range(3,19):
        data_frame.iloc[:,i] = data_frame.iloc[:,i].astype(int) 

    # fetch audio files names
    audio_names = np.copy([])
    for filename in data_frame["Fichier Audio"]:
        if filename.endswith(".wav"):
            audio_names = np.append(audio_names, filename.replace('/', slash))

    # sort audio files names
    # We want to sort it by year
    years = np.unique([path[4:8] for path in audio_names])
    sorted_names = np.array([], dtype='<U49')
    # and inside each year, sort it by date.
    for year in years:
        idx_to_sort = np.where(np.array([path[4:8] for path in audio_names]) == year)[0]
        temp_path = np.copy(audio_names[idx_to_sort])
        temp_path.sort()
        sorted_names = np.append(temp_path, sorted_names)
            
    return data_frame, sorted_names

def find_sequences(directory, minutes=5):
    """
    Group wavefiles in sequences if name is "%Y%m%d_%H%M%SUTC_V12.wav"

    Parameters
    ----------
    directory : string or list of strings
        path to a parent folder containing wavefiles
    minutes : int
        Tolerance between 2 datetimes so they are still considered 
        to be part of the same sequence.

    Returns
    -------
    sequences : list
        List of sequences. Each sequence contains a list of pd.Timestamp.
        Each sequence contains datetimes of the same day, 
        within 5 minutes of each previous or following datetime.
    """

    # loop on subdirectories
    date_wavefile = []
    if isinstance(directory, str):
        directory = [directory]

    for d in directory:
        date_wavefile += [
            datetime.strptime(file, "%Y%m%d_%H%M%SUTC_V12.wav")
            for file in os.listdir(d) if file.lower().endswith(".wav")
            ]

    # split files in sequences
    datetimes_ = pd.to_datetime(date_wavefile).sort_values()
    
    sequences = [[datetimes_[0]]]
    sequence_idx = 0
    for _, (prev_date, date) in enumerate(zip(datetimes_[:-1], datetimes_[1:])):
        # if different day than the previous one, new sequence
        if prev_date.strftime("%Y%m%d") != date.strftime("%Y%m%d"):
            sequence_idx += 1
            sequences.append([date])
        # if more than 5 minutes between 2 recordings, new sequence
        elif prev_date + pd.to_timedelta(minutes, unit='m') < date:
            sequence_idx += 1
            sequences.append([date])
        # else, append current sequence
        else:
            sequences[sequence_idx].append(date)

    # put results in dict variable
    dict_sequences = {}
    for sequence in sequences:
        str_sequence = [date.strftime("%Y%m%d_%H%M%S") for date in sequence]
        dict_sequences[pd.Series(sequence).min().strftime("%Y-%m-%d_%Hh%M")] = str_sequence

    return dict_sequences

def execute_code_delays_quota(audio, outfile, pos_file, channels=[], script_folder="gsrp-tdoa", verbose=False):
    """
        A function that executes a script from a external library :
        'gsrp-tdoa'. It is necessary for TDOAs computation.

        Parameters
        ----------
        audio : string
            Path to an audio recording
        outfile : string
            Path to the place were the output file will be saved
            (folder + outut filename)
        pos_file : string
            Path to a file containing the positions of detected clicks
            in 'audio'
        channels : list of ints
            List of int corresponding to the channels which will be kept
            to computed tdoas. (Allows to exclude gps pulse tracks for example...)
            Default is [] (selects all channels)

        Returns
        -------
        None, the script saves an .csv with all the outputs.
    """

    instruction = "python3 -B "
    instruction += os.path.join(script_folder, 'gsrp_tdoa_hyperres.py') + " "
    instruction += audio + " "
    instruction += outfile + " "
    instruction += f"--frame-size 0.01 --pos {pos_file} --low 20000 --quota 1GB --wide --erase "

    if verbose:
        instruction += "--verbose "
    if len(channels) > 0:
        instruction += f"--channels {','.join([str(channel) for channel in channels])} "

    exit_code = os.system(instruction)

    if verbose:
        if exit_code != 0:
            print(f"\t{BColors.FAIL}Failed at fetching delays.{BColors.ENDC}")
        else:
            print(f"{BColors.OKGREEN}\tSuccess at fetching delays!{BColors.ENDC}")

def scale_data(mean, multiply, data):
    """
    Small function that needs no explaining.
    """
    return (data-mean)*multiply

def concatenate_delays_csv(folder_path, ending="_gsrp_outputs.csv"):
    """
    Searches 'folder_path' for files ending with 'ending'.
    Imports each corresponding file and concatenates them into one.

    Parameters
    ----------
    folder_path : string
        Path to a folder condaining outputs from 'gsrp-tdoa'.
    ending : string
        Ending of the names of the files in 'folder_path'.

    Returns
    -------
    A pandas dataframe of these files concatenated into one table.
    """
    all_csv = []
    for f in os.listdir(folder_path):
        if f.endswith(ending):
            temp = pd.read_csv(
                os.path.join(folder_path, f))
            temp = temp.rename(columns={'# pos':'time'})
            temp['file_date'] = datetime.strptime(f[:-len(ending)], "%Y%m%d_%H%M%S")
            all_csv.append(temp)

    return pd.concat(all_csv)

def float_range(mini,maxi):
    """ From : https://stackoverflow.com/a/64259328
    Return function handle of an argument type function for 
       ArgumentParser checking a float range: mini <= arg <= maxi
         mini - minimum acceptable argument
         maxi - maximum acceptable argument"""

    # Define the function with default arguments
    def float_range_checker(arg):
        """New Type function for argparse - a float within predefined range."""

        try:
            f = float(arg)
        except ValueError:    
            raise argparse.ArgumentTypeError("must be a floating point number")
        if f < mini or f > maxi:
            raise argparse.ArgumentTypeError("must be in range [" + str(mini) + " .. " + str(maxi)+"]")
        return f

    # Return function handle to checking function
    return float_range_checker

def int_range(mini,maxi):
    """ Adapted from : https://stackoverflow.com/a/64259328
    Return function handle of an argument type function for 
       ArgumentParser checking a int range: mini <= arg <= maxi
         mini - minimum acceptable argument
         maxi - maximum acceptable argument"""

    # Define the function with default arguments
    def int_range_checker(arg):
        """New Type function for argparse - an int within predefined range."""

        try:
            f = int(arg)
        except ValueError:    
            raise argparse.ArgumentTypeError("must be a integer number")
        if f < mini or f > maxi:
            raise argparse.ArgumentTypeError("must be in range [" + str(mini) + " .. " + str(maxi)+"]")
        return f

    # Return function handle to checking function
    return int_range_checker

def path_exists(mode="folder"):
    """Return function handle of an argument type function for 
       ArgumentParser checking if arg is an existing folder or file.
        mode :
            - folder if path has to be a folder
            - file if path has to be a file"""

    # Define the function with default arguments
    def path_checker(arg):
        """New Type function for argparse - a path that exists and matches mode."""

        try:
            path = str(arg)
        except ValueError:    
            raise argparse.ArgumentTypeError("Cannot convert to string")
        
        if mode == "file":
            if not os.path.isfile(path):
                raise argparse.ArgumentTypeError("File does not exist")
        elif mode == "folder":
            if not os.path.isdir(path):
                raise argparse.ArgumentTypeError("Folder does not exist")
        else:
            raise argparse.ArgumentTypeError("Invalid mode selected in type function")
        
        return path

    # Return function handle to checking function
    return path_checker
