#%%## Importations #####
import os
import json
import math
import pickle
import joblib
import argparse
import numpy as np
import pandas as pd
from tqdm import tqdm
from datetime import datetime
from scipy.optimize import minimize, differential_evolution

import matplotlib.pyplot as plt

import torch
from torch import nn, optim
from torchinfo import summary
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split

from utils.utils import BColors

import warnings
warnings.filterwarnings("ignore", category=RuntimeWarning)


#%%## Definitions #####
def angle_modulo(angles, mode="deg"):
    """
    A function to make sure that angles remain in the interval [-180°, 180°].

    Parameters
    ----------
    angles : numpy array
        Angles in degrees or radians
    mode : string
        "deg" : when angles are in degrees
        "rad" : when angles are in radians

    Returns
    -------
    angles : numpy array
        angles with bias added, with value circling in [-180°, 180°] interval.
    """
    if mode == "deg":
        angles = np.remainder(angles, 360)
        angles[angles > 180] -= 360 
    elif mode == "rad":
        angles = np.remainder(angles, 2*np.pi)
        angles[angles > np.pi] -= 2*np.pi 
    else:
        raise ValueError("mode should be one of ['deg', 'rad']")
    return angles


def spherical2cartesian(distance, elevation, azimuth):
    """
    A function to transform spherical/polar coordinates
    to cartesian coordinates.

    Parameters
    ----------
    distance : scalar or array-like
        Distance to origin
    elevation : scalar or array-like
        Angle of elevation (in radians).
        0° correspond to z-axis pointing up.
    azimuth : scalar or array-like
        Angle of azimuth (in radians).
    
    Returns
    -------
    xyz_coords : numpy array 
        Coordinates in cartesian system.
    """
    x = distance * np.sin(elevation) * np.cos(azimuth)
    y = distance * np.sin(elevation) * np.sin(azimuth)
    z = distance * np.cos(elevation)

    xyz_coords = np.array([x, y, z]).T

    return xyz_coords


def cartesian2spherical(x, y, z):
    """
    A function to convert cartesian coordinates 
    to spherical/polar coordinates.

    Parameters
    ----------
    x : scalar or array-like
        X-axis coordinates.
    y : scalar or array-like
        Y-axis coordinates.
    z : scalar or array-like
        Z-axis coordinates.

    Returns
    -------
    sphe_coords : array-like
        coordinates in spherical/polar system.
        Contains [distance, elevation, azimuth] data.
    """
    distance = np.linalg.norm([x, y, z], axis=0)
    azimuth = np.arctan2(y, x)
    elevation = np.arctan2(
        (np.linalg.norm([x, y], axis=0)), 
        z)

    sph_coords = np.array([distance, elevation, azimuth]).T

    return sph_coords


def random_point_in_sphere(radius, bounds_theta=(), bounds_r=()):
    """
    A Function that generates random spherical coordinates 
    for one point inside of a sphere. 
    Distribution is uniform accross distances.

    Parameters
    ----------
    radius : int
        Radius of the sphere.
    bounds_theta : tuple, optional
        Bounds for theta angle (azimuth) in radians. (min angle, max angle).
    bounds_r : tuple, optional
        Bounds for distance in radius. (min distance, max distance)

    Returns
    -------
    r : float
        distance from origin
    theta : float
        azimuth angle (from x-axis), in radians.
    phi : float
        elevation angle (from y-axis), in radians.
    """

    # azimuth
    if len(bounds_theta)>0:
        theta = float(np.random.random()*(np.diff(bounds_theta))+bounds_theta[0])
    else:
        theta = np.random.random()*2*np.pi
    
    # elevation
    costphi = (np.random.random()-0.5)*2    
    phi = np.arccos(costphi)

    # distance
    u = np.random.random() 
    r = radius * np.cbrt(u) # cbrt to have uniform distrib of x, y
                            # sqrt to have uniform distrib of distances 
    if len(bounds_r)>0 and (bounds_r[0] != bounds_r[1]):
        while (r < bounds_r[0]) or (r > bounds_r[1]):
            u = np.random.random()
            r = radius * np.cbrt(u)
    elif len(bounds_r)>0 and (bounds_r[0] == bounds_r[1]):
        r = bounds_r[0]
    else:
        pass

    while theta > np.pi:
        theta = theta-2*np.pi
    while phi > np.pi:
        phi = phi-2*np.pi
    while theta < -np.pi:
        theta = theta+2*np.pi
    while phi < -np.pi:
        phi = phi+2*np.pi

    return r, theta, phi


def tdoa_residuals_solve_couple(couple_dataframe, stations_coordinates, celerity, dist_max=125, results="xyz"):
    """
        A function that tries to find the mean distance of two points to match given data
        (using differential_evolution to avoid local maxima).

        Parameters
        ----------
        couple_dataframe : numpy array
            Data information necessary to predict the distance of 2 point :
            [delay_01, delay_02, delay_03, azimuth, elevation]
            Shape should thus be (5,2)
        stations_coordinates : numpy array
            Coordinates of stations in 3D space.
            Shape should be (4, 3)
        dist_max : int
            Borders for x, y and z-axes.
        results : str
            If "xyz" results are given in cartesian coordinates
            If "dea" results are given in spherical coordinates [distance, elevation, azimuth]

        Returns
        -------
        numpy array
            Best distance that matches the delays given
    """
    n_stations = stations_coordinates.shape[0]

    delays_to_stations_A = couple_dataframe[[f"t_0{station}_A" for station in range(1,n_stations)]].to_numpy()
    delays_to_stations_B = couple_dataframe[[f"t_0{station}_B" for station in range(1,n_stations)]].to_numpy()
    angles_A = couple_dataframe[["azimuth_A", "elevation_A"]].to_numpy()
    angles_B = couple_dataframe[["azimuth_B", "elevation_B"]].to_numpy()

    def error_function(distances):
        # convert to x, y, z
        xyz_A = spherical2cartesian(distances[0], angles_A[1], angles_A[0])
        xyz_B = spherical2cartesian(distances[1], angles_B[1], angles_B[0])

        # compute distance to each station
        time_distances_A = np.linalg.norm(stations_coordinates - xyz_A, axis=1)/celerity
        time_distances_B = np.linalg.norm(stations_coordinates - xyz_B, axis=1)/celerity

        # compare tdoas
        t_A = time_distances_A[1:] - time_distances_A[0]
        t_B = time_distances_B[1:] - time_distances_B[0]

        value_A = np.linalg.norm(t_A-delays_to_stations_A)
        value_B = np.linalg.norm(t_B-delays_to_stations_B)

        return np.mean([value_A, value_B])

    output = differential_evolution(
            error_function, 
            bounds=[(0, dist_max), (0, dist_max)], 
            strategy="best1bin", 
            init="latinhypercube").x

    if results=="xyz":
        return output
    
    elif results=="dea":
        return cartesian2spherical(output[0], output[1], output[2])
    
    else:
        raise ValueError(f"Unknown results {results}. Should be one of ['xyz', 'dea']")


def tdoa_matrix_solve(stations, tdoas, celerity, letter="", columns=None, results="xyz"):
    """
        A function to determine the direction of arrival of a sound.

        Parameters
        ----------
        stations : array-like
            Coordinates of hydrophones (in m) that record the arrival of a sound.
            Shape should be (X hydrophones, 3 dimensions).
        tdoas : pandas DataFrame
            Recorded tdoa to each hydrophones in seconds.
            Shape should be (N sounds, X-1 (because tdoas : [0->1, 0->2, 0->3, ....])).
        celerity : float or int
            Celerity of sound underwater in m.s-1
        letter : str
            The letter to add to, example: "t_01" -> "t_01_A.
            By default "" (no letter is added).
        columns : list, optionnal
            Overrides "letter" parameters with absolute names of columns.
        results : str
            If "xyz" results are given in cartesian coordinates
            If "dea" results are given in spherical coordinates [distance, elevation, azimuth]

        Returns
        -------
        Coordinates using the provided coordinates system.
    """
    n_stations = stations.shape[0]
    
    tdoas_columns = [f't_0{station}' for station in range(1, n_stations)]
    if len(letter)!=0:
        tdoas_columns = [tdoas_column + f"_{letter}" for tdoas_column in tdoas_columns]
    if isinstance(columns, list):
        tdoas_columns = columns

    norm_xyz = np.linalg.lstsq(
        stations[0] - stations[1:], 
        tdoas[tdoas_columns].T*celerity,
        rcond=None)[0]
    
    if results == "xyz":
        return norm_xyz
    
    elif results=="dea":
        dea_norm = cartesian2spherical(
            norm_xyz[0], 
            norm_xyz[1], 
            norm_xyz[2])
        
        return dea_norm.T

    else:
        raise ValueError(f"Unknown results {results}. Should be one of ['xyz', 'dea']")


def tdoa_residuals_solve(delays_to_stations, stations_coordinates, celerity, init=[0,0,-10], dist_max=125, results="xyz", func="minimize"):
    """
    A function that tries to find the best coordinates to match
    the tdoas given to each station.

    Parameters
    ----------
    delays_to_stations : numpy array
        Time Delay of Arrival (TDoA) of sound to each station.
        Length should be the same as the number of hydrophones minus 1 (N-1).
    stations_coordinates : numpy array
        Coordinates of stations in 3D space.
        Shape should be (N, 3)
    init : array-like
        Initial coordinates for solving (can have impact if strong local minima).
    dist_max : int
        Borders for x, y and z-axes.
    results : str
        If "xyz" results are given in cartesian coordinates
        If "dea" results are given in spherical coordinates [distance, elevation, azimuth]

    Returns
    -------
    numpy array
        Best coordinates that matches the delays given
    """
    n_stations = stations_coordinates.shape[0]
    def error(x):
        time_distances = np.linalg.norm(stations_coordinates - x, axis=1)/celerity
        delays = []
        for n_station in range(1, n_stations):
            delays += [time_distances[n_station] - time_distances[0]]

        value = np.linalg.norm(np.array(delays)-delays_to_stations)
        return value
    x0 = np.array(init)

    if func=="minimize":
        coords = minimize(error, x0, tol=1e-18, method="L-BFGS-B",
            bounds=[(-dist_max, dist_max), (-dist_max, dist_max), (-dist_max, dist_max)]).x

    elif func=="differential_evolution":
        coords = differential_evolution(
            error, 
            bounds=[(-dist_max, dist_max), (-dist_max, dist_max), (-dist_max, dist_max)], 
            strategy="best2bin", 
            init="sobol").x
    
    else:
        raise ValueError(f"Unknown func: {func}. Should be one of ['minimize', 'differential_evolution']")


    if results=="xyz":
        return coords
    
    elif results=="dea":
        return cartesian2spherical(coords[0], coords[1], coords[2])

    else:
        raise ValueError(f"Unknown results {results}. Should be one of ['xyz', 'dea']")


def cluster_it_yourself(data, x_name, y_name):
    """
        Shows an interactive plot where points can be selected.
        Each selection attributes the point contained inside to a
        specific label, which is returned.

        Parameters
        ----------
        data : pandas dataframe
            Dataframe that must contain x_name and y_name columns
        x_name : string
            Column in data that will be plotted on the x-axis
        y_name : string or list
            Column(s) in data that will be plotted on the y-axis
            If string : figure will show one plot
            If list : figure will show as many plots as elements in y_name, but the selection
            can only be performed on the first plot. 

        Returns
        -------
        selector.labels:
            Labels of the points. 
    """
    from utils.plots import SelectFromCollection
    colors_list = ['#1f77b4', '#ff7f0e', '#2ca02c', '#d62728', '#9467bd', 
        '#8c564b', '#e377c2', '#7f7f7f', '#bcbd22', '#17becf']

    if isinstance(y_name, list):
        fig, axs = plt.subplots(len(y_name),1, sharex=True)

        pts = axs[0].scatter(
            x=data[x_name],
            y=data[y_name[0]],
            s=5)
        selector = SelectFromCollection(axs[0], pts)

        for y_idx, y_element in enumerate(y_name[1:]):
            axs[y_idx+1].scatter(
                x=data[x_name],
                y=data[y_element],
                s=5)

    else:
        fig, ax = plt.subplots(1,1)
        pts = ax.scatter(
            x=data[x_name],
            y=data[y_name],
            s=5)
        selector = SelectFromCollection(ax, pts)

    plt.show()
    return selector.labels


class PositionsFromTDOAs:
    """
        A class to compute 3D positions from (pre-computed) TDoAs of clicks.

        
        Parameters
        ----------
        sequence : list of strings
            List of strings with the following date_format 'YYYYMMDD_hhmmss' 
            and belonging to the same sequence of recordings.

        output_f : string
            Folder where a folder containg all outputs 
            for this sequence will be created

        velocity : float or int
            Speed of sound underwater in m.s-1


        Methods
        -------
        cluster_TDOAS():
            Displays a matplotlib interface to select points.
            This allows to group click_train together and 
            to exclude weird values (unselected points not considered further).
        add_gps_data():
            For sequences when a pinger was underwater from the experiment boat,
            it is possible to compare computed 3D positions with gps measurements.
            This function does this comparison (with user interactions) and 
            updates data in main dataframe.
        fit_raw_distances(method, calib_path=None):
            Computes 3D positions with different method to fine-tune
            the prediction of the distances.
        calib_dist(calib_type):
            Calibrates parameters to compute positions (and mainly distances).
            Works only of gps data is available.

        Tips
        ----
        use the methods in the following order:
            1. cluster_TDOAS()
            2. add_gps_data()
            3. calib_dist(calib_type)
            4. fit_raw_distances(method)
    """
    def __init__(
            self,
            sequence,
            output_f,
            velocity=1500,
            verbose=False):
        self.file_dates = [datetime.strptime(date, "%Y%m%d_%H%M%S") 
            for date in sequence]
        self.output_dir = os.path.join(
            output_f, 
            min(self.file_dates).strftime("Seq_%Y-%m-%d_%Hh%M")
            )
        self.velocity = velocity
        self.output_f = output_f
        self.verbose = verbose

        # read csv files
        if os.path.exists(os.path.join(self.output_dir, "concat_delays.csv")):
            self.df_tdoas = pd.read_csv(
                os.path.join(self.output_dir, "concat_delays.csv"),
                parse_dates = ['file_date', 'datetime'])
        else:
            raise FileNotFoundError("Cannot find file: 'concat_delays.csv'"
                f" in {self.output_dir}")

        # determine the number of stations
        self.n_stations = 1
        while (f"t0{self.n_stations}" in self.df_tdoas.columns):
            self.n_stations += 1

            if self.n_stations >= 50:
                raise ValueError("Cannot determine the number of stations from 'concat_delays.csv'")

        if self.n_stations < 3:
            raise ValueError(
                f"Found {self.n_stations} stations from 'concat_delays.csv'"
                ", minimum should be 3.")

    def compute_angles(self, path_coordinates, mode="precision"):
        """
            A function that determines the angles of arrival (AoAs) from tdoas and
            stations positions. 

            Parameters
            ----------
            path_coordinates : string
                Path to a file containing the coordinates of each stations (receiver/hydrophone).
                The file should be an output of TETRACoordsFinder. (Coordinates in m).
            mode : string
                Should be one of ["precision", "fast"].
                If "precision" : angles will be determined using a method of residual optimization.
                It is precise but very slow.
                If "fast" : angles will be determined using a linear solver. It is much faster but
                less precise.

            Returns
            -------
            None : adds a ["azimuth", "elevation"] columns to df_tdoas
        """
        if os.path.exists(path_coordinates):
            self.stations_coords = pd.read_csv(path_coordinates).to_numpy()
        else:
            raise FileNotFoundError(f"{path_coordinates} not found.")

        # center coordinates
        self.stations_coords = self.stations_coords.T - self.stations_coords.mean(axis=1)

        if mode=="fast":
            if self.verbose:
                print(f"Computing angles...")
            _, self.df_tdoas["elevation"], self.df_tdoas["azimuth"] = np.array(
                tdoa_matrix_solve(
                    stations=self.stations_coords, 
                    tdoas=self.df_tdoas, 
                    celerity=self.velocity,
                    columns=[f"h_t0{i}" for i in range(1, self.n_stations)]
                    )
                )
    
        elif mode=="precision":
            if self.verbose:
                print(f"Computing angles...")
            elevations, azimuths = [], []
            for _, row in tqdm(self.df_tdoas.iterrows(), total=len(self.df_tdoas), leave=True, desc=f"Computing angles", disable=(not self.verbose)):
                temp = tdoa_residuals_solve(
                    delays_to_stations=row[[f"h_t0{i}" for i in range(1, self.n_stations)]].to_numpy(), 
                    stations_coordinates=self.stations_coords,
                    celerity=self.velocity,
                    results="dea")
                elevations += [temp[1]]
                azimuths += [temp[2]]
            self.df_tdoas["elevation"], self.df_tdoas["azimuth"] = elevations, azimuths
            del elevations, azimuths

        else:
            raise ValueError(f"Unknwon mode {mode}. Should be one of ['fast', 'precision'].")

        if self.verbose:
            print("Saving results...")
        to_save = self.df_tdoas.copy() # just a test.
        to_save.to_csv(
            os.path.join(self.output_dir, "concat_delays_with_angles.csv"), 
            index=False)
        if self.verbose:
            print(f"{BColors.OKGREEN}\tDone!\n{BColors.ENDC}")

    def cluster_tracks(self, mode="auto", exclude_noise=True, cluster_with=["azimuth"], with_umap=True,
        min_cluster_size=25, min_samples=25, cluster_selection_epsilon=0, cluster_selection_method="eom"):
        """
            A function to cluster acoustic tracks together based on
            angles in time, two methods are available : manual or auto.

            Parameters
            ----------
            mode : string
                If 'auto' : performs clustering of tracks based on umap embedding and hdbscan clustering
                If 'manual' : displays a matplotlib interface that allows mouse-selection of clusters.
                Default is 'auto'.
            exclude_noise : bool
                When True, if (in a previous step) a "noise" column was already generated, 
                uses it to reduce the number of rows on which the clustering will be executed
            cluster_with : string or list
                Name(s) of columns in df_tdoas that will be used as y-axis for clustering.
            **others : arguments for hdbscan
        """
        import umap
        import hdbscan
        
        if ("elevation" not in self.df_tdoas.columns):
            raise KeyError("Cannot cluster tracks without angles. Run compute_angles() first.")

        if (exclude_noise) and ("noise" not in self.df_tdoas.columns):
            raise UserWarning("No column 'noise' given, clustering will include all rows.")
            exclude_noise = False

        self.df_tdoas_with_clusters = self.df_tdoas.copy()
        if exclude_noise:
            self.df_tdoas_with_clusters = self.df_tdoas_with_clusters.where(
                self.df_tdoas_with_clusters.noise==False).dropna(how="all").copy()
        
        # datetime is a timestamp, not suited for clustering, replacing it with "total_seconds"
        self.df_tdoas_with_clusters["total_seconds"] = (
            self.df_tdoas_with_clusters["datetime"] -
            self.df_tdoas_with_clusters["datetime"].min()
            ).dt.total_seconds()

        if mode=="manual":
            if self.verbose:
                print("Manual clustering...")
            clusters = cluster_it_yourself(
                self.df_tdoas_with_clusters, "total_seconds", cluster_with)

            # Only if cluster were selected, save a new file.
            if len(pd.unique(clusters)) > 1:
                self.df_tdoas_with_clusters["clusters"] = clusters
                self.df_tdoas_with_clusters.to_csv(os.path.join(
                    self.output_dir, "concat_delays_with_angles_and_clusters.csv"), index=False)
                if self.verbose:
                    print(f"{BColors.OKGREEN}\tClusters saved!\n{BColors.ENDC}")
            else:
                if self.verbose:
                    print(f"{BColors.WARNING}\tNo selection made.\n{BColors.ENDC}")

        elif mode=="auto":
            if self.verbose:
                print("Automatic clustering...")

            columns_for_embedding = ["total_seconds"] + ([cluster_with] if isinstance(cluster_with, str) else cluster_with)

            reducer = umap.UMAP()
            if self.verbose:
                print("\tUMAP...")
            scaled_df = StandardScaler().fit_transform(
                self.df_tdoas_with_clusters[columns_for_embedding].values)
            if with_umap:
                embedding = reducer.fit_transform(scaled_df)
            else:
                embedding = np.copy(scaled_df)

            if self.verbose:
                print("\tHDBSCAN...")
            clusters = hdbscan.HDBSCAN(
                min_cluster_size=min_cluster_size,
                min_samples=min_samples,
                cluster_selection_epsilon=cluster_selection_epsilon,
                cluster_selection_method=cluster_selection_method
                ).fit_predict(embedding)
            
            self.df_tdoas_with_clusters["clusters"] = clusters
            del self.df_tdoas_with_clusters["total_seconds"]
            self.df_tdoas_with_clusters.to_csv(os.path.join(
                self.output_dir, "concat_delays_with_angles_and_clusters.csv"), index=False)
            if self.verbose:
                print(f"{BColors.OKGREEN}\tClusters saved!{BColors.ENDC}")

        else:
            raise ValueError(f"Unknown mode {mode}, should be one of ['manual', 'auto']")


class TETRACoordsFinder:
    """
    A class that computes and saves the coordinates of 4 hydrophones
    arranged in a tetrahedron. Using only the distances between each
    hydrophone and eventually an offset for the distance to the water surface.

    Parameters
    ----------
    distances : list of floats
        Distances between each hydrophone of the tetrahedron
        (In the following order: [1-2, 1-3, 1-4, 2-3, 2-4, 3-4]).
    d1_S : FLOAT
        Distance (in metres) between the top of the tetrahedron 
        and the surface of the water

    Methods
    -------
    tetrahedron_coords()
        Method that computes the coordinates of the 4 hydrophones of the tetrahedron. 
        With hydrophone 1 on top, d1_S m under the surface of the water.
        It uses distances and d1_S.
    tetrahedron_coords_from_zero()
        Method that computes the coordinates of the 4 hydrophones of the tetrahedron. 
        With hydrophone 1 on top.
        It uses distances.
    tetrahedron_coords_default():
        Method that computes the coordinates of the 4 hydrophones of the tetrahedron. 
        With hydrophone 1 on top and distance values that were measured in 2022.
        It does not need any input.  
    save_coords(output_dir, add_info):
        Method that saves coordinates previously computed into a csv file.
    """
    def __init__(
        self,
        distances=None,
        d1_S=0):

        # fetching arguments
        self.distances = np.array(distances)
        self.d1_S = d1_S

        # creating output
        self.points = pd.DataFrame(
            data = {        
            'Hydro1': None,
            'Hydro2': None,
            'Hydro3': None,
            'Hydro4': None
                },
            index = [0,1,2]
            )

    def tetrahedron_coords(self):
        try:
            dist = self.distances
            offset = self.d1_S

            # compute position of hydros (Tetrahon)
            # first hydro will be at the top, the base is a plane.
            point2 = [0,0,0]
            point3 = [dist[3],0,0]
            point4 = [((dist[3]**2+dist[4]**2-dist[5]**2) / (2*dist[3])),
                     np.sqrt((dist[4]**2) - ((dist[3]**2+dist[4]**2-dist[5]**2)**2 / (2*dist[3])**2)),
                     0]

            annex = (point3[0]*(point4[1]**2)) - ((point3[0]**2)*point4[0]) + \
                (point3[0]*(point4[0]**2)) + (point3[0]*(dist[0]**2)) - (point3[0]*(dist[2]**2))-\
                (point4[0] * (dist[0]**2)) + (point4[0] * (dist[2]**2))

            point1 = [((point4[1]**2+point4[0]**2+dist[0]**2-dist[2]**2-(annex/dist[3]))/(2*point4[0])),
                        annex/(2*dist[3]*point4[1]),
                        0]
            point1[2] = np.sqrt(-point1[0]**2-point1[1]**2+dist[0]**2)


            # we substract the high of the first hydro to the surface of the water
            # and we have all our coordinates
            H = point1[2] + offset
            point1[2] = point1[2] - H
            point2[2] = point2[2] - H
            point3[2] = point3[2] - H
            point4[2] = point4[2] - H

            self.points['Hydro1'] = point1
            self.points['Hydro2'] = point2
            self.points['Hydro3'] = point3 
            self.points['Hydro4'] = point4

        except:
            print(
                f"{BColors.FAIL}FAILED :{BColors.ENDC}", 
                "'distances' or 'd1_S' variable was not instantiated correctly.")

    def tetrahedron_coords_from_zero(self):
        try:
            dists = self.distances
            # transcription
            vertex1 = [0,0,0]
            vertex2 = [dists[0],0,0]
            vertex3 = [((dists[0]**2+dists[1]**2-dists[3]**2) / (2*dists[0])),
                     np.sqrt((dists[1]**2) - ((dists[0]**2+dists[1]**2-dists[3]**2)**2 / (2*dists[0])**2)),
                     0]
            annex = (vertex2[0]*(vertex3[1]**2)) - ((vertex2[0]**2)*vertex3[0]) + \
                (vertex2[0]*(vertex3[0]**2)) + (vertex2[0]*(dists[2]**2)) - (vertex2[0]*(dists[4]**2))-\
                (vertex3[0] * (dists[2]**2)) + (vertex3[0] * (dists[4]**2))
            vertex4 = [((vertex3[1]**2+vertex3[0]**2+dists[2]**2-dists[4]**2-(annex/dists[0]))/(2*vertex3[0])),
                        annex/(2*dists[0]*vertex3[1]),
                        0]
            vertex4[2] = -np.sqrt(-vertex4[0]**2-vertex4[1]**2+dists[2]**2)

            self.points['Hydro1'] = vertex1
            self.points['Hydro2'] = vertex2
            self.points['Hydro3'] = vertex3 
            self.points['Hydro4'] = vertex4     

        except:
            print(
                f"{BColors.FAIL}FAILED :{BColors.ENDC}", 
                "'distances' variable was not instantiated correctly.") 

    def tetrahedron_coords_default(self):
        # get coordinates from linear solving
        DIST = np.array([   [0,     87.6,   87.4,   87.1], 
                            [87.6,  0,      87.6,   85.5],
                            [87.4,  87.6,   0,      87.5],
                            [87.1,  85.5,   87.5,   0 ] ])
        DIST = DIST*0.01
        out = minimize( fun = self._ErrorTetrahedron, 
                        x0 = [-100,-50,80,-50,-80,100], 
                        args = DIST)

        hydro1, hydro2, hydro3, hydro4 = [0,0,0], [0,0,0], [0,0,0], [0,0,0]
        hydro2[0], hydro3[0], hydro3[1], hydro4[0], hydro4[1], hydro4[2] = out['x']

        self.points['Hydro1'] = hydro1
        self.points['Hydro2'] = hydro2
        self.points['Hydro3'] = hydro3 
        self.points['Hydro4'] = hydro4

    def show_coords(self):
        try:
            fig = plt.figure()
            fig.add_subplot(projection='3d')
            for i in range(1,4,1):
                for j in range(i+1,5,1):
                    plt.plot(
                        [self.points.loc[0,f"Hydro{i}"], self.points.loc[0,f"Hydro{j}"]],
                        [self.points.loc[1,f"Hydro{i}"], self.points.loc[1,f"Hydro{j}"]],
                        [self.points.loc[2,f"Hydro{i}"], self.points.loc[2,f"Hydro{j}"]])
            plt.show(block=False)
        except:
            print(
                f"{BColors.FAIL}FAILED :{BColors.ENDC}", 
                "Did you generate coordinates first?") 

    def save_coords(self, output_dir, add_info=""):
        """
        output_dir : string
            Path the the directory were the .csv will be saved.
        add_info : string
            Additional info to add to csv filename.
            will be 'coords-tetra{add_info}.csv'
        """
        self.points.to_csv(
            os.path.join(
            output_dir, 
            (f"coords-tetra{add_info}.csv")),
            index=False)

    def _ErrorTetrahedron(self, vertex_pos, distances):
        """
        We consider a tetrahedron with 4 vertexes such as:
        vertex_n = (x_n, y_n, z_n)

        Parameters
        ----------
        vertex_pos : LIST of FLOATS
            List of 6 elements that are the coordinates of the 4 vertexes
            of the tetrahedron such as: [[x2, x3, y3, x4, y4, z4]]

        distances : MATRIX of FLOATS
            Matrix of 6*6 elements that contains the distances to each vertex.
            of the tetrahedron such as:
            Distances = [   [[1-1], [1-2],  [1-3],  [1-4]], 
                            [[1-2], [2-2],  [2-3],  [2-4]],
                            [[1-3], [2-3],  [3-3],  [3-4]],
                            [[1-4], [2-4],  [3-4],  [4-4]] ]

        Returns
        -------
        dev : FLOAT
            Deviation between the distance between vertexes and measured distances.
        """
        x2, x3, y3, x4, y4, z4 = vertex_pos
        P = np.array([[0, 0, 0],
                      [x2, 0, 0],
                      [x3, y3, 0],
                      [x4, y4, z4]])
        dev = np.square(np.linalg.norm(P[:,None] - P, axis=2) - distances).sum()
        return dev


#%% Code legacy
# def cosine_similarity(v1, v2):
#     """
#     Computes cosine similarity between two vectors

#     Parameters
#     ----------
#     v1, v2 : torch tensors
#     """
#     norm1 = torch.linalg.norm(v1, axis=-1)
#     norm2 = torch.linalg.norm(v2, axis=-1)

#     return (v1*v2).sum(axis=-1)/(norm1*norm2)

# From PositionsFromTDOAs
    # # for nn
    # self.use_columns = ['h_t01','h_t02', 'h_t03', 'raw_distance']
    # self.predict_columns = ["distance"]

    # def add_gps_data(self):
    #     from utils.gps import add_GPS_info
    #     from utils.plots import TransformData
    #     # Match gps time to acoustic data
    #     self._load_clusters()

    #     self.df_tdoas_gps = add_GPS_info(
    #         self.df_tdoas,
    #         os.path.join(self.output_f, "all_relative.csv"))
    #     self.df_tdoas_gps = self.df_tdoas_gps.where(
    #         self.df_tdoas_gps.clusters != 0).copy().dropna()

    #     # determine raw positions from acoustic data
    #     dist, elev, azim = doa_matrix_solve(
    #         stations=self.self.pos_hydros,
    #         tdoas=self.df_tdoas_gps,
    #         celerity=self.velocity,
    #         with_distance = True
    #     )
    #     self.df_tdoas_gps["raw_distance"] = dist
    #     self.df_tdoas_gps["raw_elevation"] = elev
    #     self.df_tdoas_gps["raw_azimuth"] = azim

    #     # determine time and angle shift from gps data
    #     t_data = TransformData(
    #         self.df_tdoas_gps, 
    #         os.path.join(self.output_f, "all_relative.csv"))
    #     # /!\ angle bias in TansformData is in degrees.
    #     self.angle_shift = np.radians(t_data.angle_bias_deg)
    #     self.time_shift = t_data.time_bias
    #     self.df_tdoas_gps = add_GPS_info(
    #         self.df_tdoas_gps,
    #         os.path.join(self.output_f, "all_relative.csv"),
    #         self.time_shift,
    #         self.angle_shift)

    #     # save modified data
    #     self.df_tdoas_gps.to_csv(
    #         os.path.join(self.output_dir, "delays_vs_gps.csv"), 
    #         index=False)

    #     # save file with shift informations
    #     shift_infos_path = os.path.join(self.output_dir, "shift_infos.json")
    #     shift_infos = {
    #             "time_shift": self.time_shift,
    #             "angle_shift-in_rad": self.angle_shift
    #             }
    #     with open(shift_infos_path, "w") as f:
    #         json.dump(shift_infos, f, indent=4)

    # def OLD_fit_raw_distances(self, method, calib_path=None, parameters_path=None):
    #     """
    #         OLD : keeping it for traacking
    #         Computes 3D positions with different method to fine-tune
    #         the prediction of the distances.

    #         Parameters
    #         ----------
    #         method : string
    #             the method to use in order to fine-tune distances.
    #             'paul': a method based on the grouping of clicks by trains.
    #             'scale': using a scaling function (with premade parameters).
    #             'NN': using a pretrained neural-network.
    #             'raw': saving relative distances without fitting.
    #         calib_path : string
    #             path to a folder containing 'scale' parameters or 'NN' model.
    #             Default is None.
    #         parameters_path : string
    #             path to a folder containing parameters of model training.
    #             Default is None.

    #         Returns
    #         -------
    #         None, saves data into "fitted_{method}.csv" file.
    #     """
    #     from utils.sound import compute_raw_doa
    #     # check given paths
    #     if not isinstance(calib_path, str):
    #         calib_path = self.output_dir
    #     if not isinstance(parameters_path, str):
    #         parameters_path = "parameters.json"
    #         if not os.path.exists(parameters_path):
    #             raise ValueError("Cannot find parameters.json in main folder, please provide parameter_path.")
        
    #     # load data
    #     with open(parameters_path, "r") as f:
    #         self.parameters = json.load(f)
    #     self._load_clusters()
    #     # exclude outliers
    #     self.df_tdoas = self.df_tdoas.where(
    #         self.df_tdoas.clusters != 0).dropna().copy()
    #     self.df_tdoas = compute_raw_doa(
    #         self.df_tdoas,
    #         self.hydro1_dist,
    #         self.velocity)

    #     # compute x, y, z coordinates using 'method'
    #     if method in ["NN", "raw"]:
    #         if method == "NN":
    #             self._predict_nn(calib_path)
    #         elif method == "raw":
    #             self.df_tdoas["distance_fit"] = self.df_tdoas["raw_distance"]
    #         else:
    #             raise ValueError(f"Given method '{method}' is not in ['NN', 'raw']")

    #         self.df_results = self.df_AB[[
    #             "distance_fit", "elevation_A", "azimuth_A",
    #             "sequence_time", "clusters", "file_date",
    #         ]].copy()

    #         self.df_results.rename(
    #             columns={
    #                'distance_fit': 'distance',
    #                'elevation_A': 'elevation',
    #                'azimuth_A': 'azimuth'
    #             },
    #             inplace=True
    #         )

    #         results_coords = spherical2cartesian(
    #             self.df_results["distance"],
    #             self.df_results["elevation"],
    #             self.df_results["azimuth"]
    #         )
    #         self.df_results["x"] = results_coords[:,0]
    #         self.df_results["y"] = results_coords[:,1]
    #         self.df_results["z"] = results_coords[:,2]

    #         # save it with results
    #         self.df_results.to_csv(
    #             os.path.join(self.output_dir, f"fit_results.csv"),
    #             index=False)     
               
    #     else:
    #         print(f"{BColors.FAIL}Unknown 'method'{BColors.ENDC}: "
    #             "should be 'raw', 'paul', 'NN' or 'scale'.")

    # def _load_clusters(self):
    #     """
    #         A function to load "delays_clustered.csv"
    #     """
    #     path = os.path.join(self.output_dir, "delays_clustered.csv")
    #     if os.path.exists(path):
    #         self.df_tdoas = pd.read_csv(
    #             path, 
    #             parse_dates=["file_date", "sequence_time"])
    #     else:
    #         print(f"{BColors.FAIL}Did not find file 'delays_clustered.csv' "
    #             f"in {self.output_dir}{BColors.ENDC}")

    # def _predict_nn(self, folder):
    #     from utils.models import split_to_tensor_XY_scale, DResNet
    #     if (os.path.exists(os.path.join(folder, "model_state_dict.pt")) and 
    #         os.path.exists(os.path.join(folder, "scaler.pkl"))):

    #         with open(os.path.join(folder, 'scaler.pkl'), "rb") as f:
    #             self.scaler = pickle.load(f)

    #         # pre-process data
    #         if os.path.exists(os.path.join(self.output_dir, f"coupled_points.csv")):
    #             self.df_AB = pd.read_csv(os.path.join(self.output_dir, f"coupled_points.csv"))
    #         else:
    #             self._preprocess_audio_data()

    #         # Create X, y tensors
    #         self.X, _, _ = split_to_tensor_XY_scale(
    #             data=self.df_AB,
    #             feats=self.parameters["features"],
    #             preds=["azimuth_A", "azimuth_B"], # existing columns bc y will be discarded anw
    #             model_type="2pts+++",
    #             scaler=self.scaler
    #         )

    #         # create and use model
    #         self.nn_model = DResNet(13, 1)
    #         self.nn_model.load_state_dict(torch.load(
    #             os.path.join(folder, "model_state_dict.pt")))
    #         self.nn_model.eval()
    #         self.df_AB["distance_fit"] = self.nn_model(self.X).detach().numpy()

    #         # save it with results
    #         self.df_AB.to_csv(
    #             os.path.join(self.output_dir, f"coupled_points.csv"),
    #             index=False)

    #     else:
    #         raise ValueError(
    #             "Did not find file 'model_state_dict.pt' or 'scaler.pkl' in "
    #             f"folder: {folder}. Did you provide calib_path ?")
    
    # def _preprocess_audio_data(self):
    #     self.processed_data = self.df_tdoas.copy()
    #     self.processed_data.reset_index(drop=True, inplace=True)

    #     # group by approx ~deltaT
    #     self.processed_data["coupled_with"] = None
    #     values_not_found = 0
    #     for index, row in tqdm(self.processed_data.iterrows(), total=self.processed_data.shape[0], desc="Group by"):
    #         closest_up = np.argmin(np.abs(
    #             self.processed_data.sequence_time - 
    #             (row.sequence_time + pd.to_timedelta(self.parameters["time_delta"], unit='s'))
    #             ))
    #         val_up = np.abs(
    #             self.processed_data.loc[closest_up, "sequence_time"] - 
    #             row.sequence_time - 
    #             pd.to_timedelta(self.parameters["time_delta"], unit='s'))
    #         closest_down = np.argmin(np.abs(
    #             self.processed_data.sequence_time - 
    #             (row.sequence_time - pd.to_timedelta(self.parameters["time_delta"], unit='s'))
    #             ))
    #         val_down = np.abs(
    #             row.sequence_time - 
    #             self.processed_data.loc[closest_down, "sequence_time"] - 
    #             pd.to_timedelta(self.parameters["time_delta"], unit='s'))
            
    #         # keep closest
    #         closest = (closest_up if (val_up < val_down) else closest_down)

    #         if ((closest != index) and 
    #             (row.sequence_time - self.processed_data.loc[closest, "sequence_time"]) < 
    #                 pd.to_timedelta(self.parameters["time_delta"], unit='s')):
    #             self.processed_data.loc[index, "coupled_with"] = closest
    #         else:
    #             values_not_found += 1
    #             self.processed_data.loc[index, "coupled_with"] = closest
    #     if values_not_found > 0:
    #         print(f"{BColors.WARNING}Warning:{BColors.ENDC} "
    #             f"{values_not_found} clicks did not find a couple...")
            
    #     # create a dataframe that matches synthetic data
    #     self.df_AB = pd.DataFrame()
    #     self.df_AB[self.parameters["columns"]] = None
    #     self.df_AB[["clusters", "file_date", 
    #                 "sampling_rate", "sequence_time"]] = None
    #     couples_added = []
    #     for index, row in tqdm(self.processed_data.iterrows(), total=self.processed_data.shape[0], desc="Compute tdoas"):
    #         rowA = row.copy()
    #         rowB = self.processed_data.loc[rowA.coupled_with].copy()

    #         d_01 = (rowA.h_t01-rowB.h_t01)/np.abs(rowB.sequence_time - rowA.sequence_time).total_seconds()
    #         d_02 = (rowA.h_t02-rowB.h_t02)/np.abs(rowB.sequence_time - rowA.sequence_time).total_seconds()
    #         d_03 = (rowA.h_t03-rowB.h_t03)/np.abs(rowB.sequence_time - rowA.sequence_time).total_seconds()
    #         if np.isnan(d_01) or np.isnan(d_02) or np.isnan(d_03):
    #             d_01, d_02, d_03 = 0, 0, 0

    #         new_row = [None, None, None, None, None, None,
    #                 None, None, None, None, None, None,
    #                 rowA.h_t01, rowA.h_t02, rowA.h_t03, rowB.h_t01, rowB.h_t02, rowB.h_t03, 
    #                 d_01, d_02, d_03, rowA.clusters,
    #                 rowA.file_date, rowA.sampling_rate, rowA.sequence_time]
            
    #         # add row 
    #         if [rowA.name, rowB.name] not in couples_added: #detect potential duplicates
    #             couples_added += [[rowA.name, rowB.name], [rowB.name, rowA.name]]
    #             self.df_AB.loc[len(self.df_AB)] = new_row

    #     # compute azimuth and elevation from h_positions
    #     for letter in ["A", "B"]:
    #         self.df_AB[f"elevation_{letter}"], self.df_AB[f"azimuth_{letter}"] = np.array(
    #             doa_matrix_solve(
    #                 stations=self.parameters["hydrophones_coordinates"], 
    #                 tdoas=self.df_AB[[f"t_01_{letter}", f"t_02_{letter}", f"t_03_{letter}"]], 
    #                 celerity=self.parameters["sound_celerity"],
    #                 letter=letter))

    #     # save it (angles in radians in [-np.pi, np.pi])
    #     self.df_AB.to_csv(
    #         os.path.join(self.output_dir, f"coupled_points.csv"),
    #         index=False)

    # def __calib_dist(self, calib_type):
    #     """
    #     Calibrates parameters to compute positions (and mainly distances).
    #     Works only of gps data is available.

    #     Parameters
    #     ----------
    #     calib_type : string
    #         Type of calibration to use :
    #         'NN' : creates a model that tries to fit computed
    #             distances to distances measured by gps.
    #         'scale' : uses parameters to try to rescale computed
    #             distances to distances measured by gps.

    #     Returns
    #     -------
    #     None, saves parameters or model to self.output_dir.
    #     """
    #     calib_file = os.path.join(
    #         self.output_dir,
    #         "calib_params.json")

    #     if os.path.exists(calib_file):
    #         with open(calib_file, "r") as f: 
    #             self.calib = json.load(f)
    #     else:
    #         self.calib = {}

    #     # choosing method
    #     if calib_type == "scale":
    #         scaling_cst, scaling_factor = self._calib_scl()
    #         self.calib["scaling_cst"] = scaling_cst
    #         self.calib["scaling_factor"] = scaling_factor
        
    #     elif calib_type == "NN":
    #         self.device = (
    #             "cuda"
    #             if torch.cuda.is_available()
    #             else "mps"
    #             if torch.backends.mps.is_available()
    #             else "cpu"
    #             )
    #         print(f"{BColors.OKBLUE}Pytorch{BColors.ENDC}:"
    #             f" using {self.device} device")
    #         self.device = torch.device(self.device)
    #         self.nn_model, self.std_scaler = self._calib_nn()
    #         torch.save(
    #             self.nn_model, 
    #             os.path.join(self.output_dir, "nn_model.pt"))
    #         joblib.dump(
    #             self.std_scaler, 
    #             os.path.join(self.output_dir, 'nn_scaler.gz'), 
    #             compress=True)
        
    #     # saving parameters 
    #     with open(
    #             os.path.join(self.output_dir, "calib_params.json"), 
    #             "w") as f:
    #         json.dump(self.calib, f, indent=4)

    # def __load_gps_fit(self):
    #     """
    #     A function to load "delays_vs_gps.csv"
    #     """
    #     path = os.path.join(self.output_dir, "delays_vs_gps.csv")
    #     if os.path.exists(path):
    #         self.df_tdoas_gps = pd.read_csv(path, parse_dates=["file_date", "sequence_time"])
    #     else:
    #         print(f"{BColors.FAIL}Did not find file 'delays_vs_gps.csv' "
    #             f"in {self.output_dir}{BColors.ENDC}")

    # def __calib_scl(self):
    #     """
    #     Calibrates computed distances to gps measures by rescaling.
    #     """
    #     from utils.utils import scale_data
    #     self._load_gps_fit()

    #     max_iter = 10000
    #     learning_rate = 1e0

    #     print(f"{BColors.HEADER}Looking for best fit "
    #         f"of distances using scaling.{BColors.ENDC}\n"
    #         f"Optimisation with lr={learning_rate}, max_iter={max_iter}.")

    #     # rescaling using distance = (value-minus)*factor
    #     minus = torch.tensor(-1, dtype=torch.float32, requires_grad=True)
    #     factor = torch.tensor(1000, dtype=torch.float32, requires_grad=True)
    #     criterion = nn.L1Loss(reduction='sum')  
    #     optimizer = optim.Adam([minus, factor], lr=learning_rate) 

    #     # fit parameters using optimisation loop.
    #     loss_train = []
    #     for epoch in range(max_iter):
    #         prediction = scale_data(
    #             minus, 
    #             factor, 
    #             torch.tensor(self.df_tdoas_gps.raw_distance.copy().to_numpy(), 
    #                 dtype=torch.float32)
    #             )
    #         loss = criterion(
    #             prediction, 
    #             torch.tensor(self.df_tdoas_gps.distance.copy(), 
    #                 dtype=torch.float32)
    #             )
    #         loss.backward()
    #         optimizer.step()
    #         optimizer.zero_grad() 
    #         loss_train.append(loss.item())

    #         if epoch % 100 == 0:
    #             print(f"[STEP]: {epoch}\t[LOSS]: {loss_train[-1]:.3f}")

    #         # if np.sum(np.abs(np.diff(loss_train[:-51])) < 100) == 50:
    #         #     print(f"[STEP]: {epoch}, no evolution for last 50 steps.")
    #         #     break

    #     # show results of optimisation loop.
    #     print(f"\n[RESULTS]: value-{minus.item():.2f}*{factor.item():.2f}")
    #     self._plot_distances_VS_gps(
    #         scale_data(
    #             minus.item(), 
    #             factor.item(), 
    #             self.df_tdoas_gps.raw_distance))

    #     return (minus.item(), factor.item())

    # def __calib_nn(self):
    #     """
    #     Calibrates computed distances to gps measures using a DNN.
    #     """
    #     self._load_gps_fit()
    #     batch_size = 64
    #     learning_rate = 0.01

    #     # create training, test and validation datasets
    #     X_train, X_test, y_train, y_test = train_test_split(
    #         self.df_tdoas_gps[self.use_columns].copy().to_numpy(), 
    #         self.df_tdoas_gps[self.predict_columns].copy().to_numpy(),
    #         train_size=0.75,
    #         shuffle=True
    #         )
    #     scaler = StandardScaler()
    #     scaler.fit(X_train)
    #     X_train = torch.tensor(scaler.transform(X_train), dtype=torch.float32)
    #     X_test = torch.tensor(scaler.transform(X_test), dtype=torch.float32)
    #     y_train = torch.tensor(y_train, dtype=torch.float32)
    #     y_test = torch.tensor(y_test, dtype=torch.float32)

    #     # create model
    #     DNN = LeModel(X_train.shape[1], y_train.shape[1])
    #     DNN.to(self.device)
    #     print(f"\n{BColors.HEADER}Summary:{BColors.ENDC}")
    #     print(summary(DNN, input_size=(batch_size, X_train.shape[1]), 
    #         col_names=["input_size", "output_size", "num_params"], col_width=15),
    #         end="\n\n")

    #     # training step
    #     training_step = train_model(
    #         DNN, 
    #         X_train, y_train,
    #         learning_rate=learning_rate,
    #         end_lr_factor=0.1,
    #         early_stopping=-1,
    #         print_every=100)
    #     DNN_train1 = training_step.best_model
        
    #     # show results of training.
    #     test_step = test_model(DNN_train1, X_test, y_test)
    #     to_predict = scaler.transform(
    #         self.df_tdoas_gps[self.use_columns].copy().to_numpy())
    #     self._plot_distances_VS_gps(DNN_train1(
    #         torch.tensor(to_predict, dtype=torch.float32)).detach().numpy())

    #     return (DNN_train1, scaler)

    # def __predict_paul(self, space=1):
    #     from utils.sound import group_clicks
    #     # group clicks in clusters
    #     self.df_tdoas = group_clicks(
    #         self.df_tdoas,
    #         space=space)
        
    #     max_iter = 2**6
    #     max_dist = 1500

    #     for click_group, subdf in tqdm(
    #         self.df_tdoas.groupby('click_group'), 
    #         total=self.df_tdoas.click_group.nunique(), 
    #         leave=False, 
    #         desc="Fitting 3D positions"):

    #         ### fixed values used for computation
    #         timings = torch.tensor(subdf.time.to_numpy(), dtype=torch.float64)

    #         if len(timings)==1:
    #             tqdm.write(f"{BColors.WARNING}Skipped click_group "
    #                 f"{int(click_group)}{BColors.ENDC}: cannot evaluate speed")
            
    #         else:
    #             measured_tdoa = -subdf[['h_t01', 'h_t02', 'h_t03']].to_numpy()
    #             measured_tdoa = torch.tensor(measured_tdoa).float()

    #             doa = np.linalg.lstsq(
    #                 self.hydro1_dist, 
    #                 measured_tdoa.T*self.velocity, 
    #                 rcond=None)[0].T
    #             norm_doa = np.linalg.norm(doa, axis=-1, keepdims=True)
    #             doa = torch.tensor(doa/norm_doa).reshape(-1, 1, 3)

    #             #### variables that we're trying to fit
    #             celerity = torch.tensor(
    #                 self.velocity, 
    #                 requires_grad=True, 
    #                 dtype=torch.float64)
    #             positions = (doa*1).contiguous().clone().requires_grad_(True)
    #             ref_distances = torch.tensor(
    #                 np.repeat(1, len(doa))[...,np.newaxis], 
    #                 dtype=torch.float64)

    #             # variable controling while loop
    #             run = True
    #             while run:
    #                 run = False
    #                 ### Setting up the optimizer 
    #                 optim = torch.optim.LBFGS(
    #                     [positions, celerity], 
    #                     lr=1e1, 
    #                     line_search_fn='strong_wolfe', 
    #                     tolerance_grad=1e-35, 
    #                     tolerance_change=1e-35)
    #                 last = torch.rand_like(positions).double()

    #                 ##### Find minimum loss #####
    #                 # loop and evaluate loss at each step
    #                 for i in range(max_iter):
    #                     def loss_fn(arg1=positions, arg2=celerity, h_pos=self.pos_hydros.T, t_axis=timings, ref_delays=measured_tdoa, ref_doa=doa):
    #                         optim.zero_grad() 

    #                         # compute 4 different component of loss
    #                         toa = (1/arg2) * torch.linalg.norm(arg1 - h_pos, axis=-1)
    #                         speed = (torch.linalg.norm(torch.diff(arg1, axis=0), axis=-1)/
    #                             torch.diff(t_axis))
    #                         terr = 1e6*(ref_delays - (toa[:,1:] - toa[:,:1]))
    #                         cosloss = -torch.log(cosine_similarity(arg1, ref_doa)+1).sum()

    #                         # compute customized loss
    #                         loss = (
    #                             (terr**2).mean() + 
    #                             torch.relu(speed.mean()/10-1) + 
    #                             1e5*cosloss + 
    #                             10*torch.exp(((arg2-self.velocity)/100)**2))
    #                         loss.backward()

    #                         return loss

    #                     # perform step i
    #                     optim.step(loss_fn)

    #                     # Optionnal : Early Stopping if no progress
    #                     # if torch.allclose(last, positions, atol=1e-20):
    #                     #     break
    #                     last = positions.clone()
            

    #                 ##### Check if there was an evolution #####
    #                 if torch.allclose(
    #                     torch.linalg.norm(last, axis=-1), 
    #                     ref_distances, 
    #                     atol=1) or np.isnan(celerity.detach().numpy()): 
    #                     # new intial variables.
    #                     ref_distances = ref_distances*2
    #                     # conditions to continue while loop
    #                     if ref_distances.detach().numpy().mean() < max_dist:
    #                         run = True
    #                         positions = (doa*ref_distances.detach().numpy().mean()).contiguous().clone().requires_grad_(True)
    #                     # conditions to end while loop
    #                     else:
    #                         run = False
    #                         celerity = torch.tensor(np.nan, dtype=torch.float64)
    #                         positions = positions*np.nan
    #                 # Just to be sure that it is set as False
    #                 else:
    #                     run = False

    #             # print summary
    #             if np.isnan(celerity.item()):
    #                 tqdm.write(f"{BColors.FAIL}Error click_group{BColors.ENDC} "
    #                     f"{int(click_group)}: optimizer did not converge")
    #             else:
    #                 tqdm.write(f"{BColors.OKGREEN}Success click_group {int(click_group)}{BColors.ENDC}: "
    #                     f"soundspeed {celerity.item():.2f}"
    #                     f"\tmeandist {np.mean(torch.linalg.norm(positions, axis=-1).detach().cpu().squeeze().numpy()):.2f}")

    #             # store results
    #             self.df_tdoas.loc[subdf.index, 'distance_fit'] = (torch.linalg.norm(
    #                 positions, axis=-1).detach().cpu().squeeze().numpy()/celerity.item())*self.velocity
    #             # self.df_tdoas.loc[subdf.index, 'velocity'] = celerity.item()
    #             # self.df_tdoas.loc[subdf.index, ['x']] = positions.detach().cpu().squeeze().numpy()[:,0]
    #             # self.df_tdoas.loc[subdf.index, ['y']] = positions.detach().cpu().squeeze().numpy()[:,1]
    #             # self.df_tdoas.loc[subdf.index, ['z']] = positions.detach().cpu().squeeze().numpy()[:,2]
    
    # def __predict_scl(self, folder):
    #     calib_file = os.path.join(
    #         folder,
    #         "calib_params.json")

    #     if os.path.exists(calib_file):
    #         with open(calib_file, "r") as f: 
    #             self.calib = json.load(f)

    #         self.df_tdoas["distance_fit"] = scale_data(
    #             self.calib["scaling_cst"], 
    #             self.calib["scaling_factor"], 
    #             torch.tensor(self.df_tdoas.raw_distance.copy().to_numpy(), 
    #                 dtype=torch.float32))

    #     else:
    #         print(f"{BColors.FAIL}Did not find file 'calib_params.json'.{BColors.ENDC}"
    #             "\n\tHave you fitted parameters for another sequence ?"
    #             f"\n\tIf yes, copy generated file to {folder}"
    #             "\n\tIf no, try runing PositionsFromTDOAs.calib_dist('scale').")

    # def __plot_distances_VS_gps(self, new_dist):
    #     plt.figure()
    #     plt.scatter(
    #         self.df_tdoas_gps.sequence_time,
    #         new_dist,
    #         label="predictions", s=5)
    #     plt.scatter(
    #         self.df_tdoas_gps.sequence_time,
    #         self.df_tdoas_gps.distance,
    #         label="gps", s=5)
    #     plt.legend(loc='best')
    #     plt.show()

