# DATA AVAILABILITY

Drone footage are available upon request.
Audio recordings that do no contain the bio-inspired signal (confidential) are available upon request.

Contact [Loïc Lehnhoff](mailto:loic.lehnhoff@gmail.com) or [Bastien Mérigot](mailto:bastien.merigot@umontpellier.fr).